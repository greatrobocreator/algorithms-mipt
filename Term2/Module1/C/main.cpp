#include <iostream>
#include <vector>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, m;
    std::cin >> n >> m;

    std::vector< long long > a(n);
    for(long long i = 0; i < n; ++i) std::cin >> a[i];

    std::vector< std::vector< std::pair< long long, long long > > > dp(n + 1, std::vector< std::pair< long long, long long > >(m + 1, {0, 0}));

    std::vector< long long > pref(n, a[0]);
    for(long long i = 1; i < n; ++i) pref[i] = pref[i - 1] + a[i];

    for(long long i = 1; i <= n; ++i) {
        for(long long j = 1; j <= m && j <= i; ++j) {
            if(j == 1) {
                dp[i][j] = std::make_pair(i * a[i - 1] - pref[i - 1], 0);
            } else {
                dp[i][j] = std::make_pair(std::numeric_limits<long long>::max(), 0);
                for (long long k = std::max(1ll, j - 1); k < i; ++k) {
                    long long t = 0;
                    for (long long x = k; x <= i; ++x) t += std::min(a[x - 1] - a[k - 1], a[i - 1] - a[x - 1]);
                    dp[i][j] = std::min(dp[i][j], std::make_pair(dp[k][j - 1].first + t, k));
                }
            }
        }
    }

    std::pair< long long, long long > min = std::make_pair(dp[n][m].first, n);
    for(long long i = 1; i < n; ++i) min = std::min(min, std::make_pair(dp[i][m].first + (pref[n - 1] - pref[i - 1]) - (n - i) * a[i - 1], i));

    std::cout << min.first << '\n';
    std::vector< long long > ans;
    for(long long i = min.second, j = 0; i > 0; i = dp[i][m - j].second, ++j) ans.push_back(a[i - 1]);
    for(auto it = ans.rbegin(); it != ans.rend(); ++it) std::cout << *it << " ";
    std::cout << '\n';

    return 0;
}
