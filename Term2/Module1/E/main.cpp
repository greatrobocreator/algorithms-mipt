#include <iostream>
#include <vector>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    // dp[i][j] - count of peaceful sets with power i and max element j
    std::vector< std::vector< long long > > dp(n + 1, std::vector< long long > (n + 1));

    dp[1][1] = 1;
    for(long long i = 2; i <= n; ++i) {
        dp[i][i] = 1;
        for(long long j = 2; j <= i; ++j) {
            for(long long k = 1; k <= j / 2; ++k) dp[i][j] += dp[i - j][k];
        }
    }

    long long ans = 0;
    for(long long i = 1; i <= n; ++i) ans += dp[n][i];

    std::cout << ans << std::endl;

    return 0;
}
