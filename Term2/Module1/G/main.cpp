#include <iostream>
#include <vector>
#include <algorithm>

int main() {

    std::string a, b;
    std::cin >> a >> b;

    /*std::vector< bool > aChars(26, false), bChars(26, false);
    for(char c : a) aChars[c - 'a'] = true;
    for(char c : b) bChars[c - 'a'] = true;
    for(long long i = 0; i < 26; ++i) {
        if(aChars[i] && !bChars[i]) a.erase(std::remove(a.begin(), a.end(), 'a' + i), a.end());
        else if(!aChars[i] && bChars[i]) b.erase(std::remove(b.begin(), b.end(), 'a' + i), b.end());
    }*/

    if(a.size() < b.size()) std::swap(a, b);

    std::vector< std::vector< short > > dp(2, std::vector< short >(b.size() + 1, 0));
    std::vector< std::vector< bool > > p(a.size() + 1, std::vector< bool >(b.size() + 1, 0));

    for(long long i = 0; i < a.size(); ++i) {
        for(long long j = 0; j < b.size(); ++j) {
            if(a[i] == b[j]) dp[1][j + 1] = dp[0][j] + 1;
            else if(dp[1][j] > dp[0][j + 1]) {
                dp[1][j + 1] = dp[1][j];
                p[i + 1][j + 1] = true;
            } else {
                dp[1][j + 1] = dp[0][j + 1];
                p[i + 1][j + 1] = false;
            }
        }
        std::swap(dp[0], dp[1]);
    }

    //std::cout << dp[a.size()][b.size()] << std::endl;
    std::string answer = "";
    long long i = a.size() - 1, j = b.size() - 1;
    while(i >= 0 && j >= 0) {
        if(a[i] == b[j]) {
            answer = a[i] + answer;
            j--;
            i--;
        } else if(p[i + 1][j + 1]) j--;
        else i--;
    }

    std::cout << answer << std::endl;

    return 0;
}
