#include <iostream>
#include <vector>

long long LCS(const std::vector< long long >& a, const std::vector< long long >& b) {
    long long n = a.size(), m = b.size();
    std::vector< std::vector< long long > > dp(2, std::vector< long long >(n + 1, 0));
    for(long long i = 0; i < m; ++i) {
        for(long long j = 0; j < n; ++j) {
            dp[1][j + 1] = ((a[j] == b[i]) ? dp[0][j] + 1 : std::max(dp[0][j + 1], dp[1][j]));
        }
        std::swap(dp[0], dp[1]);
    }
    return dp[0][n];
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, m;

    std::cin >> n;
    std::vector< long long > a(n);
    for(long long i = 0; i < n; ++i) std::cin >> a[i];

    std::cin >> m;
    std::vector< long long > b(m);
    for(long long i = 0; i < m; ++i) std::cin >> b[i];

    std::cout << LCS(a, b) << std::endl;

    return 0;
}
