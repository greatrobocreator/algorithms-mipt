#include <iostream>
#include <vector>
#include <algorithm>

std::vector< long long > getPossibleWeights(std::vector< long long >::iterator first, std::vector< long long >::iterator last, long long c) {
    std::vector< long long > result;
    for(long long i = 0; i < 1 << (last - first); ++i) {
        long long w = 0;
        for (long long j = 0; j < last - first; ++j) {
            if ((i >> j) & 1) w += *(first + j);
        }
        if(w <= c) result.push_back(w);
    }
    std::sort(result.begin(), result.end());
    return result;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, c;
    std::cin >> n;
    std::vector<long long> temp(n, 0);
    std::vector<long long> weights;

    for (long long i = 0; i < n; ++i) std::cin >> temp[i];
    std::cin >> c;
    for (long long i = 0; i < n; ++i) {
        if (temp[i] <= c) {
            weights.push_back(temp[i]);
        }
    }
    n = weights.size();

    std::vector< long long > weights1 = getPossibleWeights(weights.begin(), weights.begin() + weights.size() / 2, c);
    std::vector< long long > weights2 = getPossibleWeights(weights.begin() + weights.size() / 2, weights.end(), c);

    long long answer = 0;
    for(long long i = 0, j = weights2.size() - 1; j >= 0 && i < weights1.size(); ++i) {
        for(; j >= 0 && weights1[i] + weights2[j] > c; --j);
        answer += j + 1;
    }

    std::cout << answer << std::endl;

    return 0;
}
