#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

class TFenwikTree {
private:

    long long size;
    std::vector< long long >& a;
    std::vector< std::pair< long long, long long > > t;

    void update(unsigned long long i, long long delta);
    std::pair< long long, long long > query(long long pos);

public:

    TFenwikTree(std::vector< long long >& a);
    void Update(unsigned long long pos, long long delta);
    std::pair< long long, long long > Query(long long r);
};

TFenwikTree::TFenwikTree(std::vector< long long > &a) : size(a.size()), a(a) {
    t.resize(size, {0, -1});
    for(size_t i = 0; i < size; ++i) update(i, a[i]);
}

void TFenwikTree::update(unsigned long long pos, long long delta) {
    for(long long i = pos; i < size; i |= (i + 1)) t[i] = std::max(t[i], {delta, pos });
}

std::pair< long long, long long > TFenwikTree::query(long long pos) {
    std::pair< long long, long long > ans = {0, -1};
    for(; pos >= 0; pos = (pos & (pos + 1)) - 1) ans = std::max(ans, t[pos]);
    return ans;
}

void TFenwikTree::Update(unsigned long long int pos, long long int delta) {
    update(pos, delta);
}

std::pair< long long, long long > TFenwikTree::Query(long long r) {
    return query(r);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    std::vector< std::pair< long long, long long > > a(n);

    long long x;
    for(long long i = 0; i < n; ++i) {
        std::cin >> x;
        a[i] = {x, i};
    }

    std::sort(a.begin(), a.end(), [](const auto& l, const auto& r) {

        return (l.first == r.first ? l.second < r.second : l.first > r.first );

    });

    std::vector< long long > p(n, -1);
    TFenwikTree dp(p);

    for(long long i = 0; i < n; ++i) {
        const auto& number = a[i];
        const auto& maximum = dp.Query(number.second);
        p[number.second] = maximum.second;
        dp.Update(number.second, maximum.first + 1);
    }

    std::pair< long long, long long > ans = dp.Query(n - 1);
    std::cout << ans.first << std::endl;
    std::vector< long long > sequence;
    for(long long i = ans.second; i >= 0; i = p[i]) sequence.push_back(i);
    for(auto it = sequence.rbegin(); it != sequence.rend(); ++it) std::cout << *it + 1 << " ";
    std::cout << std::endl;

    return 0;
}
