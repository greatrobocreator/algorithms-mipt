#include <iostream>
#include <vector>
#include <cmath>
//#pragma GCC optimize("Ofast")

long long solve(long long n, long long k) {
    if(k <= 0) return n > 1 ? -1 : 0;
    if(k >= std::log(static_cast<long double>(n)) / std::log(2.0l))
        return std::ceil(std::log(static_cast<long double>(n)) / std::log(2.0l));

    //std::vector< std::vector< std::pair< long long, long long > > > dp(n + 1, std::vector< std::pair< long long, long long > >(k + 1, {std::numeric_limits<long long>::max(), 1}));
    std::vector< std::vector< std::pair< long long, long long > > > dp(2, std::vector< std::pair< long long, long long > >(n + 1, {std::numeric_limits<long long>::max(), 1}));
    //for(long long i = 1; i <= k; ++i) dp[1][i] = {0, 1};
    //for(long long i = 1; i <= n; ++i) dp[i][1] = {i - 1, 1};
    for(long long i = 1; i <= n; ++i) dp[0][i] = {i - 1, 1};
    dp[0][1] = dp[1][1] = {0, 1};

    auto comp = [](const std::pair< long long, long long>& l, const std::pair< long long, long long>& r) {
        return (l.first == r.first ? l.second > r.second : l.first < r.first);
    };

    long long a = 1;
    for(long long j = 2; j <= k; ++j) {
        for(long long i = 2; i <= n; ++i) {
        //a = 1;
            for(a = std::max(dp[0][i].second, dp[1][i - 1].second); a <= i - 1; ++a) {
                dp[1][i] = std::min(dp[1][i], std::max(dp[1][i - a], dp[0][a], comp), comp);
                if(comp(dp[1][i - a], dp[0][a])) break;
            }
            dp[1][i].first++;
            dp[1][i].second = --a;
            /*long long l = dp[i][j - 1].second, r = i - 1;
            while(r - l > 1) {
                long long m = (r + l) / 2;
                if(comp(dp[i - m][j], dp[m][j - 1])) {
                    r = m;
                } else {
                    l = m;
                }
            }
            dp[i][j] = {1 + std::min(std::max(dp[i - l][j], dp[l][j - 1], comp), std::max(dp[i - r][j], dp[r][j - 1], comp), comp).first, l};*/
        }
        std::swap(dp[0], dp[1]);
        //if(i >= 5) dp[i - 5].clear();
    }

    return dp[0][n].first;
}

/*long long solve2(long long n, long long k) {
    std::vector< std::vector< long long > > dp(2, std::vector< long long >(k + 1, 0));

}*/

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, k;
    std::cin >> n >> k;

    std::cout << solve(n, k) << std::endl;

    return 0;
}
