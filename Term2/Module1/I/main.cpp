#include <iostream>
#include <unordered_map>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long base = 1'000'000'007;
    long long n;
    std::cin >> n;

    std::unordered_map< long long, long long > dp;
    long long sum = 1; // empty subsequence

    long long x;
    for(long long i = 0; i < n; ++i) {
        std::cin >> x;
        long long delta = (sum - dp[x] + base) % base;
        dp[x] = sum;
        sum = (sum + delta) % base;
    }

    std::cout << sum - 1 << std::endl;

    return 0;
}
