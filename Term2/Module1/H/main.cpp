#include <iostream>
#include <vector>
#include <algorithm>

long long MaxCommonIncreasing(const std::vector< long long > &a, const std::vector< long long > &b) {
    long long n = a.size(), m = b.size();
    std::vector< std::vector< long long > > dp(n, std::vector< long long >(m, 0));

    for(long long i = 0; i < n; ++i) {
        long long max = 0;
        for(long long j = 0; j < m; ++j) {
            if(i > 0) dp[i][j] = dp[i - 1][j];
            if(a[i] == b[j]) dp[i][j] = std::max(dp[i][j], max + 1);
            else if(a[i] > b[j]) max = std::max(max, dp[i][j]);
        }
    }

    return *std::max_element(dp[n - 1].begin(), dp[n - 1].end());
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, m;
    std::cin >> n >> m;

    std::vector< long long > a(n), b(m);
    for(long long i = 0; i < n; ++i) std::cin >> a[i];
    for(long long i = 0; i < m; ++i) std::cin >> b[i];

    std::cout << MaxCommonIncreasing(a, b) << std::endl;

    return 0;
}
