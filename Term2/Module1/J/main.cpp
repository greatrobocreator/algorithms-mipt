#include <iostream>
#include <vector>
#include <algorithm>

long long MaxCost(const std::vector< long long >& w, long long s) {
    long long n = w.size();

    std::vector< std::vector< long long > > dp(2, std::vector< long long > (s + 1, 0));

    for(long long i = 1; i <= n; ++i) {
        for(long long j = 1; j <= s; ++j) {
            if(j >= w[i - 1])
                dp[1][j] = std::max(dp[0][j], dp[0][j - w[i - 1]] + w[i - 1]);
            else
                dp[1][j] = dp[0][j];
        }
        std::swap(dp[0], dp[1]);
    }

    return  dp[0][s];
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long s, n;
    std::cin >> s >> n;

    std::vector< long long > w;
    long long x;
    for(long long i = 0; i < n; ++i) {
        std::cin >> x;
        if(x <= s) w.push_back(x);
    }

    std::cout << MaxCost(w, s) << std::endl;

    return 0;
}
