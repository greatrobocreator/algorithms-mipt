#include <iostream>
#include <unordered_map>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    std::unordered_map< long long, long long > map;

    long long a, b;
    for(long long i = 0; i < n; ++i) {
        std::cin >> a >> b;
        if(map.find(a) == map.end()) map[a] = a;
        if(map.find(b) == map.end()) map[b] = b;
        std::swap(map[a], map[b]);
        std::cout << std::abs(map[a] - map[b]) << '\n';
    }

    return 0;
}