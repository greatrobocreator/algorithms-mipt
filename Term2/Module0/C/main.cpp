#include <iostream>
#include <functional>
#include <vector>

bool solve(long long w, long long h, long long n) {
    if(5 * n < w * h) return false;
    std::vector< std::vector< bool > > canvas(w, std::vector< bool >(h, false));
    long long count = 0;

    long long x, y;
    for(long long i = 0; i < n; ++i) {
        std::cin >> x >> y;
        if(x < 0 || y < 0 || x > w + 1 || y > h + 1) continue;
        std::vector< std::pair< long long, long long > > points = {{x, y}, {x - 1, y}, {x + 1, y}, {x, y - 1}, {x, y + 1}};
        for(auto p : points) {
            if(p.first >= 1 && p.first <= w && p.second >= 1 && p.second <= h) {
                if(!canvas[p.first - 1][p.second - 1]) {
                    canvas[p.first - 1][p.second - 1] = true;
                    count++;
                }
            }
        }
    }
    return count == w * h;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long w, h, n;
    std::cin >> w >> h >> n;

    std::cout << (solve(w, h, n) ? "Yes" : "No") << std::endl;

    return 0;
}