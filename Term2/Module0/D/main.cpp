#include <iostream>
#include <vector>
#include <iomanip>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    size_t n, m;
    std::cin >> n >> m;

    std::vector< std::vector < bool > > matrix(n, std::vector< bool >(n, false));

    long long x, y;
    for(size_t i = 0; i < m; ++i) {
        std::cin >> x >> y;
        matrix[x - 1][y - 1] = true;
        matrix[y - 1][x - 1] = true;
    }

    size_t cyclesCount = 0;
    for(size_t i = 0; i < n; ++i)
        for(size_t j = i + 1; j < n; ++j)
            if(matrix[i][j])
                for(size_t k = j + 1; k < n; ++k)
                    if(matrix[j][k] && matrix[i][k])
                        cyclesCount++;

    std::cout << std::fixed << std::setprecision(6) << (static_cast<long double>(cyclesCount) / 4.0) << std::endl;

    return 0;
}
