#include <iostream>
#include <unordered_map>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::unordered_map<std::string, long long> map;
    std::string s;
    long long delta;
    while(std::cin >> s) {
        std::cin >> delta;
        map[s] += delta;
        std::cout << map[s] << std::endl;
    }

    return 0;
}