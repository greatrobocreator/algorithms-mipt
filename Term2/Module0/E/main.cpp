#include <iostream>
#include <functional>
#include <list>
#include <vector>
#include <utility>
#include <stdexcept>
#include <string>

template< typename Key, typename Value, typename Hash = std::hash< Key > >
class THashMap {

private:
    long long n, m;
    std::vector< std::list< std::pair< Key, Value > > > buckets;

    void Rehash();

public:
    THashMap(long long m = 2048*4) : n(0), m(m), buckets(m) {};

    void Insert(Key key, Value value);
    void Erase(Key key);
    Value At(Key key);

};

template<typename Key, typename Value, typename Hash>
void THashMap<Key, Value, Hash>::Insert(Key key, Value value) {
    size_t hash = Hash{}(key) % m;
    if(std::find_if(buckets[hash].begin(), buckets[hash].end(), [&key](auto x){return x.first == key;}) == buckets[hash].end()) {
        buckets[hash].push_front({key, value});
        n++;
        if(100 * n >= 95 * m) Rehash();
    }
}

template<typename Key, typename Value, typename Hash>
void THashMap<Key, Value, Hash>::Erase(Key key) {
    size_t hash = Hash{}(key) % m;
    auto it = std::find_if(buckets[hash].begin(), buckets[hash].end(), [&key](auto x){return x.first == key;});
    if(it != buckets[hash].end()) {
        buckets[hash].erase(it);
        n--;
    }
}

template<typename Key, typename Value, typename Hash>
Value THashMap<Key, Value, Hash>::At(Key key) {
    size_t hash = Hash{}(key) % m;
    auto it = std::find_if(buckets[hash].begin(), buckets[hash].end(), [&key](auto x){return x.first == key;});
    if(it == buckets[hash].end()) throw std::out_of_range("No element with key " + std::string(key));
    return it->second;
}

template<typename Key, typename Value, typename Hash>
void THashMap<Key, Value, Hash>::Rehash() {
    m *= 2;
    auto oldBuckets(buckets);
    buckets.clear();
    buckets.resize(m);
    for(auto& bucket : oldBuckets)
        for(auto& pair : bucket)
            Insert(pair.first, pair.second);
}


int main() {

    freopen("map.in", "r", stdin);
    freopen("map.out", "w", stdout);

    THashMap< std::string, std::string > map;

    std::string cmd, x, y;
    while(std::cin >> cmd) {
        if(cmd == "put") {
            std::cin >> x >> y;
            map.Erase(x);
            map.Insert(x, y);
        } else if(cmd == "delete") {
            std::cin >> x;
            map.Erase(x);
        } else if(cmd == "get") {
            std::cin >> x;
            try {
                std::cout << map.At(x) << '\n';
            } catch (const std::out_of_range &e) {
                std::cout << "none\n";
            }
        } else {
            std::cout << "Something went wrong!" << std::endl;
        }
    }

    return 0;
}
