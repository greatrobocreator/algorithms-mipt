#include <iostream>
#include <vector>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, m;
    std::cin >> n >> m;
    std::vector< std::vector< bool > > ans(n, std::vector< bool >(m, false));

    long long u, v;
    for(long long i = 0; i < m; ++i) {
        std::cin >> u >> v;
        ans[u - 1][i] = true;
        ans[v - 1][i] = true;
    }
    
    for(long long i = 0; i < n; ++i) {
        for(long long j = 0; j < m; ++j) {
            std::cout << ans[i][j] << " ";
        }
        std::cout << '\n';
    }

    return 0;
}
