#include <iostream>
#include <vector>
#include <unordered_set>
#include <algorithm>


namespace std {
    template<>
    struct hash<std::pair< long long, long long > > {
        size_t operator()(const std::pair<long long, long long>& x) const noexcept {
            return std::hash<long long>()(x.first) ^ (std::hash<long long>()(x.second) << 1);
        }
    };
}

void dfs(long long v,
         long long p,
         long long timer,
         const std::vector< std::vector< long long > >& g,
         std::vector<bool> &used,
         std::vector<long long> &tin,
         std::vector<long long>& ret,
         std::unordered_set< std::pair<long long int, long long> >& bridges) {
    tin[v] = timer++;
    used[v] = true;
    ret[v] = tin[v];
    for(auto& to : g[v]) {
        if(to == p) continue;
        if(!used[to]) {
            dfs(to, v, timer, g, used, tin, ret, bridges);
            ret[v] = std::min(ret[v], ret[to]);
            if(ret[to] >= tin[to]) {
                bridges.insert({v, to});
                bridges.insert({to, v});
            }
        } else {
            ret[v] = std::min(ret[v], tin[to]);
        }
    }
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, m;
    std::cin >> n >> m;

    std::vector< std::vector< long long > > g(n, std::vector<long long>());
    std::vector< std::pair< long long, long long > > edges;
    std::unordered_set< std::pair< long long, long long > > bridges;

    long long u, v;
    for(long long i = 0; i < m; ++i) {
        std::cin >> u >> v;
        --u, --v;
        g[u].push_back(v);
        g[v].push_back(u);
        edges.push_back({u, v});
    }

    std::vector< bool > used(n, false);
    std::vector< long long > ret(n, std::numeric_limits<long long>::max());
    std::vector< long long > tin(n, std::numeric_limits<long long>::max());
    long long timer = 0;
    for(long long i = 0; i < n; ++i) {
        if(!used[i]) {
            dfs(i, -1, timer, g,  used, tin, ret, bridges);
        }
    }

    std::vector< long long > ans;
    for(long long i = 0; i < m; ++i) {
        if(bridges.find(edges[i]) != bridges.end()) ans.push_back(i + 1);
    }
    std::sort(ans.begin(), ans.end());

    std::cout << ans.size() << std::endl;
    for(auto& i: ans) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    return 0;
}