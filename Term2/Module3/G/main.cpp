#include <iostream>
#include <vector>
#include <functional>

void dfs(long long v, long long p,
         const std::vector< std::vector< long long > >& g,
         std::vector< bool >& used,
         std::function<void(long long)> set) {
    used[v] = true;
    for(long long to : g[v]) {
        if(to == p) continue;
        if(!used[to]) {
            dfs(to, v, g, used, set);
        }
    }
    set(v);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, m;
    std::cin >> n >> m;

    std::vector< std::vector< long long > > g(n, std::vector< long long >());
    long long u, v;
    for(long long i = 0; i < m; ++i) {
        std::cin >> u >> v;
        --u, --v;
        g[u].push_back(v);
        g[v].push_back(u);
    }

    std::vector< bool > used(n, false);

    long long componentNumber = 1;
    std::vector< long long > components(n, 0);
    for(long long i = 0; i < n; ++i) {
        if(!used[i]) {
            dfs(i, -1, g, used, [&componentNumber, &components](long long v){ components[v] = componentNumber; });
            ++componentNumber;
        }
    }

    std::cout << (componentNumber - 1) << std::endl;
    for(auto& i: components) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    return 0;
}
