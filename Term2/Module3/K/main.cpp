#include <iostream>
#include <vector>

void dfs(long long v,
         long long p,
         const std::vector< std::vector< long long > >& g,
         std::vector< bool >& used,
         std::vector< long long >& component) {
    used[v] = true;
    for(auto& to: g[v]) {
        if(to == p) continue;
        if(!used[to]) {
            component[to] = component[v];
            dfs(to, v, g, used, component);
        }
    }
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, m;
    std::cin >> n >> m;
    std::vector< bool > states(n, false);
    std::vector< std::vector< long long > > lamps(n, std::vector< long long >());

    long long x;
    for(long long i = 0; i < n; ++i) {
        std::cin >> x;
        states[i] = x;
    }

    long long y;
    for(long long i = 0; i < m; ++i) {
        std::cin >> x;
        for(long long j = 0; j < x; ++j) {
            std::cin >> y;
            lamps[--y].push_back(i);
        }
    }

    std::vector< std::vector< long long > > g(2 * m, std::vector< long long >());
    for(long long i = 0; i < n; ++i) {
        x = lamps[i][0];
        y = lamps[i][1];
        if(states[i]) {
            g[2 * x].push_back(2 * y);
            g[2 * y].push_back(2 * x);
            g[2 * x + 1].push_back(2 * y + 1);
            g[2 * y + 1].push_back(2 * x + 1);
        } else {
            g[2 * x].push_back(2 * y + 1);
            g[2 * y + 1].push_back(2 * x);
            g[2 * x + 1].push_back(2 * y);
            g[2 * y].push_back(2 * x + 1);
        }
    }

    std::vector< bool > used(2 * m, false);
    std::vector< long long > component(2 * m, -1);
    long long count = 0;
    for(long long i = 0; i < 2 * m; ++i) {
        if(used[i]) continue;
        component[i] = ++count;
        dfs(i, -1, g, used, component);
    }

    bool ans = true;
    for(long long i = 0; i < m && ans; ++i) {
        ans &= (component[2  * i] != component[2 * i + 1]);
    }

    std::cout << (ans ? "YES" : "NO") << std::endl;

    return 0;
}
