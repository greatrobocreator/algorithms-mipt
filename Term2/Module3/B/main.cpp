#include <iostream>
#include <vector>

class Graph {
private:
    std::vector< std::vector< long long > > g;

public:

    Graph(const std::vector< std::vector< long long > >& g) : g(g) {};

    const std::vector< long long >& get(long long v) const {
        return g[v];
    }

    size_t size() const {
        return g.size();
    }

};

void dfs(long long v,
         long long p,
         const Graph& graph,
         std::vector< bool >& used,
         long long& timer,
         std::vector< long long >& tin,
         std::vector< long long >& tout) {
    used[v] = true;
    tin[v] = ++timer;
    for(auto& to: graph.get(v)) {
        if(to == p) continue;
        if(!used[to]) {
            dfs(to, v, graph, used, timer, tin, tout);
        }
    }
    tout[v] = ++timer;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    std::vector< std::vector< long long > > g(n, std::vector< long long >());

    long long root = 0;
    long long x;
    for(long long i = 0; i < n; ++i) {
        std::cin >> x;
        if(x == 0) root = i;
        else g[--x].push_back(i);
    }

    Graph graph(g);

    std::vector< bool > used(n, false);
    std::vector< long long > tin(n, -1);
    std::vector< long long > tout(n, -1);
    long long timer = 0;

    dfs(root, -1, graph, used, timer, tin, tout);

    long long m;
    std::cin >> m;
    long long a, b;
    for(long long i = 0; i < m; ++i) {
        std::cin >> a >> b;
        if(tin[--a] < tin[--b] && tout[b] < tout[a]) {
            std::cout << "1\n";
        }
        else std::cout << "0\n";
    }

    return 0;
}