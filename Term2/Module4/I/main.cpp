#include <iostream>
#include <vector>
#include <unordered_map>
#include <functional>
#include <queue>

long long carrollDistance(long long s,
                          long long t,
                          const std::vector<std::vector<long long> > &g) {
    if(s == t) return 0;
    long long n = g.size();

    std::vector< std::queue< long long > > qs(2);
    std::vector< std::unordered_map< long long, long long > > dists(2);

    qs[0].push(s);
    qs[1].push(t);

    dists[0][s] = 0;
    dists[1][t] = 0;

    while(!qs[0].empty() && !qs[1].empty()) {
        for(long long k = 0; k < 2; ++k) {
            auto& q = qs[k];
            auto& dist = dists[k];
            long long size = q.size();
            for(long long l = 0; l < size; ++l) {
                long long v = q.front();
                q.pop();
                for(auto& to: g[v]) {
                    if(dist.find(to) != dist.end()) continue;
                    dist[to] = dist[v] + 1;
                    q.push(to);
                    if(dists[1 - k].find(to) != dists[1 - k].end()) {
                        return dists[1 - k][to] + dist[to];
                    }
                }
            }
        }
    }
    return -1;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    std::vector<std::string> alphabet;
    std::unordered_map<std::string, long long> indexes;
    std::vector<std::vector<long long> > g(n);

    char maxChar = 'a';

    std::string s;
    for (long long i = 0; i < n; ++i) {
        std::cin >> s;
        if(indexes.find(s) != indexes.end()) continue;
        indexes[s] = alphabet.size();
        alphabet.push_back(s);
        for(char c: s) maxChar = std::max(maxChar, c);
    }
    n = alphabet.size();

    for(long long i = 0; i < alphabet.size(); ++i) {
        auto& str = alphabet[i];
        for(long long j = 0; j < str.size(); ++j) {
            for(char c = 'a'; c <= maxChar; ++c) {
                if(str[j] == c) continue;
                char oldC = str[j];
                str[j] = c;
                auto it = indexes.find(str);
                if(it != indexes.end()) {
                    g[i].push_back(it->second);
                }
                str[j] = oldC;
            }
        }
    }

    long long m;
    std::cin >> m;

    std::string sStr, tStr;
    for (long long i = 0; i < m; ++i) {
        std::cin >> sStr >> tStr;
        auto itS = indexes.find(sStr);
        auto itT = indexes.find(tStr);
        if (itS == indexes.end() || itT == indexes.end()) {
            std::cout << "-1" << std::endl;
            continue;
        }
        std::cout << carrollDistance(itS->second, itT->second, g) << std::endl;
    }

    return 0;
}