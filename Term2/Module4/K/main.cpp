#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>
#include <algorithm>

unsigned long long castToNumber(const std::vector< short >& state) {
    unsigned long long result = 0;
    for(short x: state) {
        result *= 16;
        result += x;
    }
    return result;
}

unsigned long long h(unsigned long long s) {
    std::vector< std::pair< short, short > > t = {
            {3, 3},
            {0, 0}, {1, 0}, {2, 0}, {3, 0},
            {0, 1}, {1, 1}, {2, 1}, {3, 1},
            {0, 2}, {1, 2}, {2, 2}, {3, 2},
            {0, 3}, {1, 3}, {2, 3}
    };
    long long manhattan = 0;
    std::vector< short > nums(16);
    std::vector< short > positions(16, 0);
    std::vector< bool > rowConflict(16, false);
    std::vector< bool > columnConflict(16, false);
    for(long long i = 15; i >= 0; --i) {
        auto num = static_cast<short>(s % 16);
        nums[i] = num;
        positions[num] = i;
        s /= 16;
        if(num == 0) continue;
        auto p = t[num];
        manhattan += std::abs(i % 4 - p.first) + std::abs(i / 4 - p.second);
    }
    long long linearConflicts = 0;
    for(long long i = 0; i < 4; ++i) {
        std::vector< short > c(4, 0);
        for(long long j = 0; j < 4; ++j) {
            short t_j = nums[4 * i + j];
            if(t_j == 0) continue;
            for(long long k = j + 1; k < 4; ++k) {
                short t_k = nums[4 * i + k];
                if(t_k == 0) continue;
                if((t_j  - 1) / 4 == i && (t_k  - 1) / 4 == i && t_j > t_k) {
                    ++c[j], ++c[k];
                    rowConflict[4 * i + j] = true;
                    rowConflict[4 * i + k] = true;
                }
            }
        }
        while(c[0] || c[1] || c[2] || c[3]) {
            long long maxInd = 0;
            for (long long j = 1; j < 4; ++j) {
                if (c[j] > c[maxInd]) maxInd = j;
            }
            c[maxInd] = 0;
            ++linearConflicts;
            for(long long j = 0; j < 4; ++j) {
                if((nums[4 * i + j] - nums[4 * i + maxInd]) * (j - maxInd) < 0 && c[j]) --c[j];
            }
        }
    }
    for(long long i = 0; i < 4; ++i) {
        std::vector< short > c(4, 0);
        for(long long j = 0; j < 4; ++j) {
            short t_j = nums[4 * j + i];
            if(t_j == 0) continue;
            for(long long k = j + 1; k < 4; ++k) {
                short t_k = nums[4 * k + i];
                if(t_k == 0) continue;
                if((t_j  - 1) % 4 == i && (t_k  - 1) % 4 == i && t_j > t_k) {
                    ++c[j], ++c[k];
                    columnConflict[4 * j + i] = true;
                    columnConflict[4 * k + i] = true;
                }
            }
        }
        while(c[0] || c[1] || c[2] || c[3]) {
            long long maxInd = 0;
            for (long long j = 1; j < 4; ++j) {
                if (c[j] > c[maxInd]) maxInd = j;
            }
            c[maxInd] = 0;
            ++linearConflicts;
            for(long long j = 0; j < 4; ++j) {
                if((nums[4 * j + i] - nums[4 * maxInd + i]) * (j - maxInd) < 0 && c[j]) --c[j];
            }
        }
    }
    short delta = 2;
    if(nums[3] != 4 && nums[3] != 0) {
        if(nums[2] == 3 && !rowConflict[3]) manhattan += delta;
        if(nums[7] == 8 && !columnConflict[8]) manhattan += delta;
    }
    if(nums[0] != 1 && nums[0] != 0) {
        if(nums[1] == 2 && !rowConflict[2]) manhattan += delta;
        if(nums[4] == 5 && !columnConflict[5]) manhattan += delta;
    }
    if(nums[12] != 13 && nums[12] != 0) {
        if(nums[13] == 14 && !rowConflict[14]) manhattan += delta;
        if(nums[8] == 9 && !columnConflict[9]) manhattan += delta;
    }
    return manhattan + linearConflicts * 2;
}

unsigned long long makeMove(unsigned long long v, const std::pair< short, short >& direction) {
    std::vector< short > nums(16);
    short zero = -1;
    for(short i = 15; i >= 0; --i) {
        nums[i] = v % 16;
        v /= 16;
        if(nums[i] == 0) zero = i;
    }
    auto x = direction.first + zero % 4, y = direction.second + zero / 4;
    if(x >= 4 || x < 0 || y >= 4 || y < 0) return -1;
    std::swap(nums[zero], nums[y * 4 + x]);
    return castToNumber(nums);
}

long long inversionsCount(unsigned long long v) {
    std::vector< short > nums(16);
    short zero = -1;
    for(short i = 15; i >= 0; --i) {
        nums[i] = v % 16;
        v /= 16;
        if(nums[i] == 0) zero = i / 4;
    }
    long long inversions = 0;
    for(long long i = 0; i < 16; ++i) {
        for(long long j = i + 1; j < 16; ++j) {
            if(nums[i] != 0 && nums[j] != 0 && nums[i] > nums[j]) ++inversions;
        }
    }
    return inversions + zero;
}

std::string solve15Puzzle(unsigned long long s) {

    if(inversionsCount(s) % 2 == 0) return "-1";

    unsigned long long t = 1311768467463790320; // 0x123456789abcdef0;

    std::set<std::pair<long long, unsigned long long> > q;
    std::unordered_map< unsigned long long,
            std::set<std::pair<long long, unsigned long long> >::iterator> iterators;

    std::unordered_map< unsigned long long, long long > dist;
    dist[s] = h(s);

    std::unordered_map< unsigned long long, std::pair< unsigned long long, short > > p;
    p[s] = {0, 0};

    iterators[s] = q.emplace(dist[s], s).first;

    while (!q.empty()) {
        auto v = *q.begin();
        if(v.first == std::numeric_limits<long long>::max()) break;
        dist[v.second] = v.first - h(v.second);
        if(v.second == t) break;
        q.erase(q.begin());

        static const std::vector< std::pair< short, short > > directions = {
                {{1, 0}, {0, 1},  {-1, 0}, {0,-1}}
        };
        for(long long i = 0; i < 4; ++i) {
            auto& direction = directions[i];
            auto to = makeMove(v.second, direction);
            if(to == -1) continue;
            if (dist.find(to) != dist.end()) continue;
            if(iterators.find(to) == iterators.end()) {
                iterators[to] = q.emplace(std::numeric_limits<long long>::max(), to).first;
            }
            auto h_to = h(to);
            if(iterators[to]->first > dist[v.second] + 1 + h_to) {
                q.erase(iterators[to]);
                iterators[to] = q.emplace(dist[v.second] + 1 + h_to, to).first;
                p[to] = {v.second, i};
            }
        }
    }

    if(dist.find(t) == dist.end()) return "-1";
    std::string result;
    std::vector< char > directionsNames = {'L', 'U', 'R', 'D'};
    for(unsigned long long v = t; v != s; result += directionsNames[p[v].second], v = p[v].first);
    std::reverse(result.begin(), result.end());
    return result;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::vector< short > s(16);
    short x;
    for(long long i = 0; i < 16; ++i) {
        std::cin >> x;
        s[i] = x;
    }

    std::string ans = solve15Puzzle(castToNumber(s));
    std::cout << ans.size() << '\n';
    std::cout << ans << std::endl;

    return 0;
}