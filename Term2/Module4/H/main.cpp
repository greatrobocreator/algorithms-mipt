#include <iostream>
#include <queue>
#include <unordered_map>

long long reverseSegment(long long n, long long i, long long j, long long len) {
    std::vector< long long > nums(len);
    long long result = 0;

    for(long long k = 0; k < len; ++k) {
        nums[k] = n % 10;
        n /= 10;
    }

    long long k;
    for(k = len - 1; k > j; --k) {
        result *= 10;
        result += nums[k];
    }

    for(; k >= i; --k) {
        result *= 10;
        result += nums[j - (k - i)];
    }

    for(; k >= 0; --k) {
        result *= 10;
        result += nums[k];
    }

    return result;
}

long long findDistance(const std::string& _s, const std::string& _t) {
    if(_s == _t) return 0;
    long long n = _s.size();

    long long s = stoll(_s);
    long long t = stoll(_t);

    std::vector< std::queue< long long > > qs(2);
    std::vector< std::unordered_map< long long, long long > > dists(2);

    qs[0].push(s);
    qs[1].push(t);

    dists[0][s] = 0;
    dists[1][t] = 0;

    while(!qs[0].empty() && !qs[1].empty()) {
        for(long long k = 0; k < 2; ++k) {
            auto& q = qs[k];
            auto& dist = dists[k];
            long long size = q.size();
            for(long long l = 0; l < size; ++l) {
                long long v = q.front();
                q.pop();
                for (long long i = 0; i < n; ++i) {
                    for (long long j = i + 1; j < n; ++j) {
                        long long to = reverseSegment(v, i, j, n);
                        if (dist.find(to) != dist.end()) continue;
                        dist[to] = dist[v] + 1;
                        q.push(to);
                        if (dists[1 - k].find(to) != dists[1 - k].end()) {
                            return dist[to] + dists[1 - k][to];
                        }
                    }
                }
            }
        }
    }
    return -1;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    std::string s, t;
    long long x;
    for(long long i = 0; i < n; ++i) {
        std::cin >> x;
        s += std::to_string(x - 1);
    }
    for(long long i = 0; i < n; ++i) {
        std::cin >> x;
        t += std::to_string(x - 1);
    }

    std::cout << findDistance(s, t) << std::endl;

    return 0;
}