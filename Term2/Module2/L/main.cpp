#include <iostream>
#include <bitset>
#include <vector>
#include <numeric>
#include <string>

class BigInteger {

private:
    static const long long base = 1'000'000'000;

    std::vector<long long> digits;
    bool negative = false;

    size_t Size() const;

    static std::pair<BigInteger, BigInteger> divide(const BigInteger &dividend, const BigInteger &divisor);

public:

    BigInteger(long long n = 0);

    BigInteger(std::string s);

    void Swap(BigInteger &r);

    BigInteger operator-() const;

    friend bool operator<(const BigInteger &l, const BigInteger &r);

    BigInteger &operator+=(BigInteger r);

    BigInteger &operator-=(const BigInteger &r);

    BigInteger &operator*=(long long r);

    BigInteger &operator*=(const BigInteger &r);

    BigInteger &operator/=(const BigInteger &r);

    friend BigInteger operator/(const BigInteger &l, const BigInteger &r);

    BigInteger &operator%=(const BigInteger &r);

    friend BigInteger operator%(const BigInteger &l, const BigInteger &r);

    static BigInteger gcd(BigInteger l, BigInteger r);

    BigInteger &operator++();

    BigInteger &operator--();

    BigInteger operator++(int);

    BigInteger operator--(int);

    explicit operator bool() const;

    std::string toString() const;

    friend std::istream &operator>>(std::istream &in, BigInteger &r);

    bool isNegative() const;

    bool IsEven() const;
};

BigInteger::BigInteger(long long n) {
    negative = (n < 0);
    if (negative) n *= -1;
    if (n == 0) digits.push_back(0);
    for (; n > 0; n /= base) digits.push_back(n % base);
}

bool operator<(const BigInteger &l, const BigInteger &r) {
    if (l.negative != r.negative) return l.negative;
    if (l.Size() != r.Size()) return l.negative ^ (l.Size() < r.Size());
    for (long long i = static_cast<long long>(l.Size()) - 1; i >= 0; --i) {
        if (l.digits[i] != r.digits[i]) return l.negative ^ (l.digits[i] < r.digits[i]);
    }
    return false;
}

bool operator>(const BigInteger &l, const BigInteger &r) {
    return (r < l);
}

bool operator<=(const BigInteger &l, const BigInteger &r) {
    return !(l > r);
}

bool operator>=(const BigInteger &l, const BigInteger &r) {
    return !(l < r);
}

bool operator==(const BigInteger &l, const BigInteger &r) {
    return (l >= r) && (l <= r);
}

bool operator!=(const BigInteger &l, const BigInteger &r) {
    return !(l == r);
}

BigInteger &BigInteger::operator+=(BigInteger r) {
    bool subtract = false;
    if (r.negative ^ negative) {
        if (negative) { // -a + b
            negative = false;
            if (r > (*this)) { // b - a
                Swap(r);
            } else {
                negative = true;
            }
        } else { // a + (-b)
            r.negative = false;
            if ((*this) < r) { // -(b - a)
                Swap(r);
                negative = true;
            }
        }
        subtract = true;
    }
    long long carry = 0;
    size_t i = 0;
    for (i = 0; i < r.Size() && i < Size(); ++i) {
        long long newValue = digits[i] + (r.digits[i] * (subtract ? -1 : 1)) + carry;
        digits[i] = newValue % base;
        carry = newValue / base;
        if (digits[i] < 0) {
            digits[i] += base;
            carry -= 1;
        }
    }
    for (; i < Size() && carry != 0; ++i) {
        long long newValue = digits[i] + carry;
        digits[i] = newValue % base;
        carry = newValue / base;
        if (newValue < 0) {
            digits[i] += base;
            carry -= 1;
        }
    }
    for (; i < r.Size(); ++i) {
        long long newValue = r.digits[i] + carry;
        carry = newValue / base;
        digits.push_back(newValue % base);
    }
    if (carry != 0) digits.push_back(carry);
    while (Size() > 1 && digits.back() == 0) digits.pop_back();
    if (Size() == 1 && digits.back() == 0) negative = false;

    return *this;
}

BigInteger operator+(const BigInteger &l, const BigInteger &r) {
    BigInteger copy(l);
    copy += r;
    return copy;
}

BigInteger &BigInteger::operator-=(const BigInteger &r) {
    return (*this) += (-r);
}

BigInteger operator-(const BigInteger &l, const BigInteger &r) {
    BigInteger copy(l);
    copy -= r;
    return copy;
}

size_t BigInteger::Size() const {
    return digits.size();
}

BigInteger BigInteger::operator-() const {
    BigInteger result = *this;
    if (result != 0) result.negative ^= true;
    return result;
}

void BigInteger::Swap(BigInteger &r) {
    digits.swap(r.digits);
    std::swap(negative, r.negative);
}

BigInteger &BigInteger::operator*=(long long r) {
    if ((*this) == 0) return (*this);
    if (r == 0) {
        negative = false;
        digits.clear();
        digits.push_back(0);
        return (*this);
    }
    if (r < 0) {
        negative ^= true;
        r *= -1;
    }
    long long carry = 0;
    for (size_t i = 0; i < Size(); ++i) {
        long long newValue = digits[i] * r + carry;
        digits[i] = newValue % base;
        carry = newValue / base;
    }
    while (carry != 0) {
        digits.push_back(carry % base);
        carry /= base;
    }
    return (*this);
}

BigInteger operator*(const BigInteger &l, long long r) {
    BigInteger copy(l);
    copy *= r;
    return copy;
}

BigInteger &BigInteger::operator*=(const BigInteger &r) {
    if (r.Size() == 0 || r == 0 || (*this) == 0) {
        return *this *= 0;
    }
    negative ^= r.negative;
    BigInteger copy(*this);

    (*this) *= r.digits[0];
    for (size_t i = 1; i < r.Size(); ++i) {
        copy.digits.insert(copy.digits.begin(), 0); // *= base
        (*this) += (copy * r.digits[i]);
    }
    return (*this);
}

BigInteger operator*(const BigInteger &l, const BigInteger &r) {
    BigInteger copy(l);
    copy *= r;
    return copy;
}

std::pair<BigInteger, BigInteger> BigInteger::divide(const BigInteger &dividend, const BigInteger &divisor) {
    BigInteger q, r;
    q.digits.resize(dividend.Size());

    for (long long i = static_cast<long long>(dividend.Size()) - 1; i >= 0; --i) {
        r.digits.insert(r.digits.begin(), dividend.digits[i]);
        long long s1 = r.Size() <= divisor.Size() ? 0 : r.digits[divisor.Size()];
        long long s2 = r.Size() < divisor.Size() ? 0 : r.digits[divisor.Size() - 1];
        long long d = (base * s1 + s2) / divisor.digits.back();
        r -= divisor * (divisor.negative ? -d : d);
        while (r < 0) {
            if (!divisor.negative)
                r += divisor;
            else r -= divisor;
            --d;
        }
        q.digits[i] = d;
    }

    q.negative = dividend.negative ^ divisor.negative;
    r.negative = dividend.negative;

    while (q.Size() > 1 && q.digits.back() == 0) q.digits.pop_back();
    while (r.Size() > 1 && r.digits.back() == 0) r.digits.pop_back();
    if (q.Size() == 1 && q.digits[0] == 0) q.negative = false;
    if (r.Size() == 1 && r.digits[0] == 0) r.negative = false;
    return {q, r};
}

BigInteger &BigInteger::operator/=(const BigInteger &r) {
    return (*this = divide(*this, r).first);
}

BigInteger operator/(const BigInteger &l, const BigInteger &r) {
    BigInteger result = BigInteger::divide(l, r).first;
    return result;
}

BigInteger &BigInteger::operator%=(const BigInteger &r) {
    return (*this = divide(*this, r).second);
}

BigInteger operator%(const BigInteger &l, const BigInteger &r) {
    BigInteger result = BigInteger::divide(l, r).second;
    return result;
}

BigInteger &BigInteger::operator++() {
    return (*this += 1);
}

BigInteger &BigInteger::operator--() {
    return (*this -= 1);
}

BigInteger BigInteger::operator++(int) {
    BigInteger copy = (*this);
    ++(*this);
    return copy;
}

BigInteger BigInteger::operator--(int) {
    BigInteger copy = (*this);
    --(*this);
    return copy;
}

BigInteger::operator bool() const {
    return (*this != 0);
}

std::string BigInteger::toString() const {
    std::string result = (negative ? "-" : "");
    size_t baseLength = std::to_string(base).size() - 1;

    result += std::to_string(digits.back());
    for (long long i = static_cast<long long>(Size()) - 2; i >= 0; --i) {
        std::string digit = std::to_string(digits[i]);
        result += std::string(baseLength - digit.size(), '0') + digit;
    }

    return result;
}

std::istream &operator>>(std::istream &in, BigInteger &r) {
    std::string input;
    in >> input;
    r = BigInteger(input);
    return in;
}

BigInteger::BigInteger(std::string s) {
    if (s.empty()) {
        (*this) = BigInteger();
        return;
    }
    negative = (s[0] == '-');
    if (s[0] == '-' || s[0] == '+') s.erase(0, 1);

    size_t baseLength = std::to_string(base).size() - 1;
    while (s.size() > baseLength) {
        digits.push_back(stoll(s.substr(s.size() - baseLength)));
        s.erase(s.size() - baseLength, s.size() - 1);
    }
    digits.push_back(stoll(s));
}

BigInteger BigInteger::gcd(BigInteger a, BigInteger b) {
    //if(a == 0 && b == 0) return BigInteger(1);
    a.negative = false;
    b.negative = false;
    if (a < b) std::swap(a, b);
    while (b) {
        a %= b;
        a.Swap(b);
    }
    return a;
}

bool BigInteger::isNegative() const {
    return negative;
}

bool BigInteger::IsEven() const {
    return digits.front() % 2 == 0;
}

std::ostream &operator<<(std::ostream &out, const BigInteger &r) {
    return (out << r.toString());
}


std::vector<std::vector<long long> > multiplyMatrices(
        const std::vector<std::vector<long long> > &a,
        const std::vector<std::vector<long long> > &b,
        unsigned long long mod) {
    if (a[0].size() != b.size()) throw std::invalid_argument("Wrong matrices sizes");
    if (!std::accumulate(a.begin(), a.end(), true,
                         [&a](bool result, const std::vector<long long> &v) {
        return result & (v.size() == a[0].size());
    })) {
        throw std::invalid_argument("Wrong first matrix inner sizes");
    }
    if (!std::accumulate(b.begin(), b.end(), true,
                         [&b](bool result, const std::vector<long long> &v) {
        return result & (v.size() == b[0].size());
    })) {
        throw std::invalid_argument("Wrong second matrix inner sizes");
    }

    long long n = a.size(), m = b.size(), k = b[0].size();
    std::vector<std::vector<long long> > result(n, std::vector<long long>(k, 0));

    for (long long i = 0; i < n; ++i) {
        for (long long j = 0; j < k; ++j) {
            for (long long s = 0; s < a[i].size(); ++s) {
                result[i][j] = (result[i][j] + (a[i][s] * b[s][j]) % mod) % mod;
            }
        }
    }

    return result;
}

std::vector<std::vector<long long> > pow(std::vector<std::vector<long long> > a, BigInteger n, unsigned long long mod) {
    std::vector<std::vector<long long> > ans =
            std::vector<std::vector<long long> >(a.size(),std::vector<long long>(a.size(),0));
    for (long long i = 0; i < ans.size(); ++i) ans[i][i] = 1;

    while (n) {
        if (!n.IsEven()) ans = multiplyMatrices(ans, a, mod);
        a = multiplyMatrices(a, a, mod);
        n /= 2;
    }
    return ans;
}

bool isCorrect(unsigned long long l, unsigned long long r, unsigned long long size) {
    unsigned long long lRepetitions = ~(l ^ (l >> 1u)) & ((1u << (size - 1)) - 1);
    unsigned long long rRepetitions = ~(r ^ (r >> 1u)) & ((1u << (size - 1)) - 1);
    return !(lRepetitions & rRepetitions & ~(l ^ r));
}

long long countPatterns(const BigInteger &n, unsigned long long m, unsigned long long mod) {
    std::vector<std::vector<long long> > matrix(1 << m, std::vector<long long>(1 << m, 0));
    for (long long i = 0; i < matrix.size(); ++i) {
        for (long long j = 0; j < matrix.size(); ++j) {
            matrix[i][j] = isCorrect(i, j, m);
        }
    }

    std::vector<std::vector<long long> > ans(1 << m, {1});
    ans = multiplyMatrices(pow(matrix, n - 1, mod), ans, mod);
    return std::accumulate(ans.begin(), ans.end(), 0,
                           [&mod](const long long l, const std::vector<long long> &r) {
                                return (l + r[0]) % mod;
                            });
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    unsigned long long m, mod;
    BigInteger n;
    std::cin >> n >> m >> mod;

    std::cout << countPatterns(n, m, mod) << std::endl;
    return 0;
}
