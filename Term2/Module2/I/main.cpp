#include <iostream>
#include <vector>

long long countOnes(long long n) {
    long long count = 0;
    for(; n; count += (n & 1), n >>= 1);
    return count;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    std::vector< std::vector< bool > > g(n, std::vector< bool >(n, false));

    char c;
    for(long long i = 0; i < n; ++i) {
        for(long long j = 0; j < n; ++j) {
            std::cin >> c;
            g[i][j] = (c == 'Y');
        }
    }

    std::vector< bool > dp(1 << n, false);
    dp[0] = true;

    long long ans = 0;

    long long oldest = -1;
    for(long long i = 1; i < dp.size(); ++i) {
        if(!(i & (i - 1))) oldest++;
        for(long long j = 0; j < n && !dp[i]; ++j) {
            if(((i >> j) & 1) && g[oldest][j] && dp[(i ^ (1 << j)) ^ (1 << oldest)]) {
                dp[i] = true;
                ans = std::max(ans, countOnes(i));
            }
        }
    }

    std::cout << ans << std::endl;

    return 0;
}
