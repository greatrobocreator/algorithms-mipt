#include <iostream>
#include <vector>


std::vector< std::vector< long long > > multiplyMatrices(const std::vector< std::vector< long long > >& a, const std::vector< std::vector< long long > >& b, unsigned long long mod) {
    long long n = a.size(), m = b.size(), k = b[0].size();
    std::vector< std::vector< long long > > result(n, std::vector< long long >(k, 0));

    for(long long i = 0; i < n; ++i) {
        for(long long j = 0; j < k; ++j) {
            for(long long s = 0; s < a[i].size(); ++s) result[i][j] = (result[i][j] + (a[i][s] * b[s][j]) % mod) % mod;
        }
    }

    return result;
}

std::vector< std::vector< long long > > pow(std::vector< std::vector< long long > > a, long long n, unsigned long long mod) {
    std::vector< std::vector< long long > > ans = std::vector< std::vector< long long > >(a.size(), std::vector< long long >(a.size(), 0));
    for(long long i = 0; i < ans.size(); ++i) ans[i][i] = 1;

    while(n) {
        if(n & 1) ans = multiplyMatrices(ans, a, mod);
        a = multiplyMatrices(a , a, mod);
        n >>= 1;
    }

    return ans;
}

std::vector< std::vector< long long > > subMatrix(const std::vector< std::vector< long long > >& fullMatrix, long long x, long long y) {
    std::vector< std::vector< long long > > result(x, std::vector< long long >(y, 0));
    for(long long i = 0; i < x; ++i) {
        //std::copy(fullMatrix[i].begin(), fullMatrix[i].end(), &result[i][0]);
        for(long long j = 0; j < y; ++j) {
            result[i][j] = fullMatrix[i][j];
        }
    }
    return result;
}

void expandMatrix(std::vector< std::vector< long long > >& matrix, long long x, long long y) {
    for(auto & i : matrix) i.resize(y, 0);
    matrix.resize(x, std::vector< long long >(y, 0));
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long mod = 1'000'000'007;
    long long n, k;
    std::cin >> n >> k;
    long long a, b, c;
    std::vector< std::pair< long long, long long > > segments;
    segments.emplace_back(0, 0);

    long long y = 0;
    for(long long i = 0; i < n; ++i) {
        std::cin >> a >> b >> c;
        y = std::max(y, c);
        segments.emplace_back(b, c);
    }

    std::vector< std::vector< long long > > fullMatrix(y + 1, std::vector< long long >(y + 1, 0));
    for(long long i = 0; i <= y; ++i) {
        for(long long j = 0; j <= y; ++j) {
            fullMatrix[i][j] = (std::abs(i - j) <= 1);
        }
    }

    std::vector< std::vector< long long > > resultMatrix(y + 1, std::vector< long long >(y + 1, 0));
    for(long long i = 0; i <= y; ++i) resultMatrix[i][i] = 1;
    for(long long i = 1; i <= n; ++i) {
        std::vector< std::vector< long long > > currentMatrix = subMatrix(fullMatrix, segments[i].second + 1, segments[i].second + 1);
        currentMatrix = pow(currentMatrix, std::min(segments[i].first, k) - segments[i - 1].first, mod);
        expandMatrix(currentMatrix, y + 1, y + 1);
        resultMatrix = multiplyMatrices(resultMatrix, currentMatrix, mod);
    }

    std::cout << resultMatrix[0][0] << std::endl;

    return 0;
}
