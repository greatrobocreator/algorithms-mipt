#include <iostream>
#include <bitset>
#include <vector>
#include <numeric>

std::vector<std::vector<long long> > multiplyMatrices(
        const std::vector<std::vector<long long> > &a,
        const std::vector<std::vector<long long> > &b) {
    if (a[0].size() != b.size()) throw std::invalid_argument("Wrong matrices sizes");
    if (!std::accumulate(a.begin(), a.end(), true,
                         [&a](bool result, const std::vector<long long> &v) {
        return result & (v.size() == a[0].size());
    })) {
        throw std::invalid_argument("Wrong first matrix inner sizes");
    }
    if (!std::accumulate(b.begin(), b.end(), true,
                         [&b](bool result, const std::vector<long long> &v) {
        return result & (v.size() == b[0].size());
    })) {
        throw std::invalid_argument("Wrong second matrix inner sizes");
    }

    long long n = a.size(), m = b.size(), k = b[0].size();
    std::vector<std::vector<long long> > result(n, std::vector<long long>(k, 0));

    for (long long i = 0; i < n; ++i) {
        for (long long j = 0; j < k; ++j) {
            for (long long s = 0; s < a[i].size(); ++s) {
                result[i][j] = (result[i][j] + (a[i][s] * b[s][j]));
            }
        }
    }

    return result;
}

std::vector<std::vector<long long> > pow(std::vector<std::vector<long long> > a, long long n) {
    std::vector<std::vector<long long> > ans =
            std::vector<std::vector<long long> >(a.size(),std::vector<long long>(a.size(),0));
    for (long long i = 0; i < ans.size(); ++i) ans[i][i] = 1;

    while (n) {
        if (n & 1) ans = multiplyMatrices(ans, a);
        a = multiplyMatrices(a, a);
        n >>= 1;
    }
    return ans;
}

bool isCorrect(unsigned long long l, unsigned long long r, unsigned long long size) {
    unsigned long long lRepetitions = ~(l ^ (l >> 1u)) & ((1u << (size - 1)) - 1);
    unsigned long long rRepetitions = ~(r ^ (r >> 1u)) & ((1u << (size - 1)) - 1);
    return !(lRepetitions & rRepetitions & ~(l ^ r));
}

long long countPatterns(long long n, long long m) {
    if (n < m) std::swap(n, m);

    std::vector<std::vector<long long> > matrix(1 << m, std::vector<long long>(1 << m, 0));
    for (long long i = 0; i < matrix.size(); ++i) {
        for (long long j = 0; j < matrix.size(); ++j) {
            matrix[i][j] = isCorrect(i, j, m);
        }
    }

    std::vector<std::vector<long long> > ans(1 << m, {1});
    ans = multiplyMatrices(pow(matrix, n - 1), ans);
    return std::accumulate(ans.begin(), ans.end(), 0,
                           [](const long long l, const std::vector<long long> &r) { return l + r[0]; });
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, m;
    std::cin >> n >> m;
    std::cout << countPatterns(n, m) << std::endl;
    return 0;
}