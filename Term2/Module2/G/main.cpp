#include <iostream>
#include <vector>

long long maskCost(long long mask, const std::vector< long long >& costs) {
    long long sum = 0;
    for(long long i = 0; mask; mask >>= 1, ++i) {
        if(mask & 1) sum += costs[i];
    }
    return sum;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, m;
    std::cin >> n >> m;

    std::vector< long long > d(n, 0);
    std::vector< std::vector< long long > > costs(n, std::vector< long long >(m, 0));

    for(long long i = 0; i < n; ++i) {
        std::cin >> d[i];
        for(long long j = 0; j < m; ++j) std::cin >> costs[i][j];
    }

    std::vector< std::vector< long long > > dp(n, std::vector< long long >(1 << m, std::numeric_limits<long long>::max()));

    for(long long mask = 0; mask < (1 << m); ++mask) {
        dp[0][mask] = maskCost(mask, costs[0]) + d[0];
    }
    dp[0][0] = 0;

    for(long long i = 1; i < n; ++i) {
        for(long long mask = 0; mask < (1 << m); ++mask) {
            dp[i][mask] = dp[i - 1][mask] + d[i];
            for(long long j = 0; j < m; ++j) {
                if((mask >> j) & 1) dp[i][mask] = std::min(dp[i][mask ^ (1 << j)] + costs[i][j], dp[i][mask]);
            }
        }
        for(long long mask = 0; mask < (1 << m); ++mask) dp[i][mask] = std::min(dp[i][mask], dp[i - 1][mask]);
    }

    std::cout << dp[n - 1][(1 << m) - 1] << std::endl;

    return 0;
}
