#include <iostream>
#include <vector>

long long countOnes(long long n) {
    long long count = 0;
    for(; n; count += (n & 1), n >>= 1);
    return count;
}


std::vector< std::vector< long long > > multiplyMatrices(const std::vector< std::vector< long long > >& a, const std::vector< std::vector< long long > >& b, unsigned long long mod) {

    long long n = a.size(), m = b.size(), k = b[0].size();
    std::vector< std::vector< long long > > result(n, std::vector< long long >(k, 0));

    for(long long i = 0; i < n; ++i) {
        for(long long j = 0; j < k; ++j) {
            for(long long s = 0; s < a[i].size(); ++s) result[i][j] = (result[i][j] + (a[i][s] * b[s][j]) % mod) % mod;
        }
    }

    return result;
}

std::vector< std::vector< long long > > pow(std::vector< std::vector< long long > > a, long long n, unsigned long long mod) {
    std::vector< std::vector< long long > > ans = std::vector< std::vector< long long > >(a.size(), std::vector< long long >(a.size(), 0));
    for(long long i = 0; i < ans.size(); ++i) ans[i][i] = 1;

    while(n) {
        if(n & 1) ans = multiplyMatrices(ans, a, mod);
        a = multiplyMatrices(a , a, mod);
        n >>= 1;
    }
    return ans;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, k;
    std::cin >> n >> k;

    std::vector< long long > a(n);
    for(long long i = 0; i < n; ++i) {
        std::cin >> a[i];
    }

    long long mod = 1'000'000'007;

    std::vector< std::vector< long long > > matrix(n, std::vector< long long >(n, 0));
    for(long long i = 0; i < n; ++i) {
        for(long long j = 0; j < n; ++j) {
            matrix[i][j] = (countOnes(a[i] ^ a[j]) % 3 == 0);
        }
    }

    matrix = pow(matrix, k - 1, mod);
    long long ans = 0;
    for(long long i = 0; i < n; ++i) {
        for(long long j = 0; j < n; ++j) {
            ans = (ans + matrix[i][j]) % mod;
        }
    }

    std::cout << ans << std::endl;

    return 0;
}
