#include <iostream>
#include <vector>
#include <bitset>

enum E_Cities {
    CITY_HUNGRY,
    CITY_RICH,
    CITY_COMMON
};

bool hasBit(long long mask, long long n) {
    return mask & (1 << n);
}

bool setBit(long long mask, long long n, bool value) {
    return (mask & (~(1 << n))) | (value << n);
}

void printMatrix(const std::vector< std::vector< long long > >& a) {
    std::cout << std::endl;
    for(const auto & arr : a) {
        for (const auto &i : arr)
            std::cout << i << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

long long getHungryCount(long long mask1, long long mask2, long long n) {
    long long count = -1;
    for(long long i = 0; i < n - 1; ++i) {
        long long currentCount = hasBit(mask1, i) + hasBit(mask2, i) + hasBit(mask1, i + 1) + hasBit(mask2, i + 1);
        if(count == -1) count = currentCount;
        if(currentCount != count) return -1;
    }
    return count;
}

bool checkMask(long long mask, const std::vector< std::vector< E_Cities > >& country, long long i) {
    for(long long j = 0; j < country.size(); ++j) {
        if((country[j][i] == CITY_RICH && hasBit(mask, j)) || (country[j][i] == CITY_HUNGRY && !hasBit(mask, j))) {
            return false;
        }
    }
    return true;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long mod = 1'000'000'007;
    long long n, m;
    std::cin >> n >> m;

    std::vector< std::vector< E_Cities > > country(n, std::vector< E_Cities >(m, CITY_COMMON));

    std::vector< E_Cities > cities = {CITY_RICH, CITY_HUNGRY, CITY_HUNGRY, CITY_COMMON};
    char c;
    for(long long i = 0; i < n; ++i) {
        for(long long j = 0; j < m; ++j) {
            std::cin >> c;
            country[i][j] = cities[c - '+'];
        }
    }

    std::vector< std::vector< long long > > dp(m, std::vector< long long >(1 << n, 0));

    std::vector< std::vector< bool > > isValidMask(m, std::vector< bool >(1 << n, false));

    for(long long i = 0; i < m; ++i) {
        for(long long mask = 0; mask < (1 << n); ++mask) {
            isValidMask[i][mask] = checkMask(mask, country, i);
        }
    }

    /*std::vector< std::vector< bool > > hungryCount(1 << n, std::vector< bool >(1 << n, false));
    for(long long mask = 0; mask < (1 << n); ++mask) {
        for(long long prevMask = 0; prevMask < (1 << n); ++prevMask) {
            hungryCount[mask][prevMask] = (getHungryCount(mask, prevMask, n) == 2);
        }
    }*/

    for(long long mask = 0; mask < (1 << n); ++mask) {
        if(!isValidMask[0][mask]) continue;
        dp[0][mask] = 1;
    }

    for(long long i = 1; i < m; ++i) {
        for(unsigned long long mask = 0; mask < (1 << n); ++mask) {
            if(!isValidMask[i][mask]) continue;
            /*for(long long prevMask = 0; prevMask < (1 << n); ++prevMask) {
                //bool count = hungryCount[prevMask][mask];
                //if(!count) continue;
                dp[i][mask] += dp[i - 1][prevMask];
                dp[i][mask] %= mod;
            }*/
            for(auto prevMask: {mask, (~mask) & ((1 << n) - 1)}) {
                if(/*isValidMask[i - 1][prevMask] && */getHungryCount(prevMask, mask, n) == 2) dp[i][mask] += dp[i - 1][prevMask];
            }
            dp[i][mask] %= mod;
            //dp[i][mask] = (dp[i - 1][mask] + dp[i - 1][(~mask) & ((1 << n) - 1)]) % mod;
        }
    }

    long long ans = 0;
    for(long long mask = 0; mask < (1 << n); ++mask) {
        ans = (ans + dp[m - 1][mask]) % mod;
    }

    std::cout << ans << std::endl;

    /*for(long long j = 0; j < m; ++j) {
        for (long long i = 0; i < n; ++i) {
            for (long long mask = 0; mask < (1 << (n + 1)); ++mask) {
                for(long long k = 0; k < 4; ++k) {
                    if(dp[j][i][mask][k] > 0) {
                        std::cout << "dp[" << j << "][" << i << "][" << std::bitset<8>(mask) << "][" << k << "] = " << dp[j][i][mask][k] << "" << std::endl;
                    }
                }
            }
        }
    }*/

    return 0;
}
