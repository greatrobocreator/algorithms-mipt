#include <iostream>

long long pow(long long a, long long n, unsigned long long mod) {
    long long ans = 1;
    while(n) {
        if(n & 1) ans = (ans * a) % mod;
        a = (a * a) % mod;
        n /=  2;
    }
    return ans;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long mod = 1'000'000'007;
    long long k;
    std::cin >> k;

    long long x;
    long long n = 1;
    for(long long i = 0; i < k; ++i) {
        std::cin >> x;
        n = (n * (x % (mod - 1))) % (mod - 1);
    }

    long long q = pow(2ll, (n + mod - 2) % (mod - 1), mod);
    long long p = (((q + mod + (n % 2 == 0 ? 1 : -1)) % mod) * 333'333'336) % mod;

    std::cout << p << "/" << q << std::endl;

    return 0;
}
