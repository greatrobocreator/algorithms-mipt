#include <iostream>
#include <vector>
#include <numeric>

std::vector<std::vector<long long> > multiplyMatrices(
        const std::vector<std::vector<long long> > &a,
        const std::vector<std::vector<long long> > &b,
        long long mod) {
    if (a[0].size() != b.size()) throw std::invalid_argument("Wrong matrices sizes");
    if (!std::accumulate(a.begin(), a.end(), true,
                         [&a](bool result, const std::vector<long long> &v) {
        return result & (v.size() == a[0].size());
    })) {
        throw std::invalid_argument("Wrong first matrix inner sizes");
    }
    if (!std::accumulate(b.begin(), b.end(), true,
                         [&b](bool result, const std::vector<long long> &v) {
        return result & (v.size() == b[0].size());
    })) {
        throw std::invalid_argument("Wrong second matrix inner sizes");
    }

    long long n = a.size(), m = b.size(), k = b[0].size();
    std::vector<std::vector<long long> > result(n, std::vector<long long>(k, 0));

    for (long long i = 0; i < n; ++i) {
        for (long long j = 0; j < k; ++j) {
            for (long long s = 0; s < a[i].size(); ++s) {
                result[i][j] = (result[i][j] + (a[i][s] * b[s][j]) % mod) % mod;
            }
        }
    }

    return result;
}

std::vector<std::vector<long long> > pow(std::vector<std::vector<long long> > a, long long n, long long mod) {
    std::vector<std::vector<long long> > ans =
            std::vector<std::vector<long long> >(a.size(),std::vector<long long>(a.size(), 0));

    for (long long i = 0; i < ans.size(); ++i) ans[i][i] = 1;

    while (n) {
        if (n & 1) ans = multiplyMatrices(ans, a, mod);
        a = multiplyMatrices(a, a, mod);
        n >>= 1;
    }
    return ans;
}

long long countDNA(long long n) {
    static const std::vector<std::vector<long long> > matrix = {
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1},
            {1, 1, 0, 1, 0},
            {1, 1, 0, 1, 0}
    };
    static const long long mod = 999'999'937;
    auto ans = multiplyMatrices(pow(matrix, n - 1, mod),{{1},
                                                          {1},
                                                          {1},
                                                          {1},
                                                          {1}}, mod);
    return std::accumulate(ans.begin(), ans.end(), 0,
                           [](const long long l, const std::vector<long long> &r) {
                                return (l + r[0]) % mod;
                            });
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long x;
    while (std::cin >> x, x != 0) {
        std::cout << countDNA(x) << '\n';
    }
    std::cout << std::endl;

    return 0;
}
