#include <iostream>
#include <vector>

std::vector< std::vector< long long > > multiplyMatrices(const std::vector< std::vector< long long > >& a, const std::vector< std::vector< long long > >& b, unsigned long long mod) {
    long long n = a.size(), m = b.size(), k = b[0].size();
    std::vector< std::vector< long long > > result(n, std::vector< long long >(k, 0));

    for(long long i = 0; i < n; ++i) {
        for(long long j = 0; j < k; ++j) {
            for(long long s = 0; s < a[i].size(); ++s) result[i][j] = (result[i][j] + (a[i][s] * b[s][j]) % mod) % mod;
        }
    }

    return result;
}

std::vector< std::vector< long long > > pow(std::vector< std::vector< long long > > a, long long n, unsigned long long mod) {
    std::vector< std::vector< long long > > ans = std::vector< std::vector< long long > >(a.size(), std::vector< long long >(a.size(), 0));
    for(long long i = 0; i < ans.size(); ++i) ans[i][i] = 1;

    while(n) {
        if(n & 1) ans = multiplyMatrices(ans, a, mod);
        a = multiplyMatrices(a , a, mod);
        n >>= 1;
    }
    return ans;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, l, m;
    long long mod = 1'000'000'007;
    std::cin >> n >> l >> m;

    std::vector< long long > inWeights(n, 0), outWeights(n, 0), innerWeights(n, 0);
    for(long long i = 0; i < n; ++i) std::cin >> inWeights[i], inWeights[i] %= m;
    for(long long i = 0; i < n; ++i) std::cin >> innerWeights[i], innerWeights[i] %= m;
    for(long long i = 0; i < n; ++i) std::cin >> outWeights[i], outWeights[i] %= m;

    std::vector< std::vector< long long > > matrix(m, std::vector< long long >(m, 0));
    for(long long i = 0; i < m; ++i) {
        for(long long j = 0; j < n; ++j) {
            matrix[i][(i + m - innerWeights[j]) % m] += 1;
        }
    }

    std::vector< std::vector< long long > > result(m, std::vector< long long >(1, 0));
    for(long long i = 0; i < n; ++i) result[inWeights[i]][0]++;

    matrix = pow(matrix, l - 2, mod);

    result = multiplyMatrices(matrix, result, mod);

    long long ans = 0;
    for(long long i = 0; i < n; ++i) {
        ans = (ans + result[(2 * m - outWeights[i] - innerWeights[i]) % m][0]) % mod;
    }

    std::cout << ans << std::endl;

    return 0;
}
