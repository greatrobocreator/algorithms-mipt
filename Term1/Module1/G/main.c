#include <stdio.h>
#include <assert.h>
#include <malloc.h>
#include <math.h>
#include <time.h>

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))

typedef struct Heap {
    size_t size;
    long long* data;
} Heap;

int HeapGetRightChildIndex(Heap* heap, int index) {
    int rightChild = 2 * index + 2;
    return (rightChild < heap->size ? rightChild : -1);
}

int HeapGetLeftChildIndex(Heap* heap, int index) {
    int leftChild = 2 * index + 1;
    return (leftChild < heap->size ? leftChild : -1);
}

void HeapSiftDown(Heap* heap, size_t index, long long count) {

    //printf("SiftDownBegin %d:\n", index);
    //for(int i = 0; i < heap->size; ++i) printf("%d\n", heap->data[i]);

    int rightChild = HeapGetRightChildIndex(heap, index);
    int leftChild = HeapGetLeftChildIndex(heap, index);

    long long leftElement = heap->data[leftChild];
    long long rightElement = heap->data[rightChild];
    long long parentElement = heap->data[index];

    if(leftElement - count <= 0 && rightElement - count <= 0) return;

    size_t replacedIndex = index;

    if(leftChild == -1) return;
    if(rightChild == -1 && parentElement < leftElement) replacedIndex = leftChild;

    if(rightChild != -1) {
        if(leftElement > parentElement && leftElement > rightElement) replacedIndex = leftChild;
        else if(rightElement >= leftElement && rightElement > parentElement) replacedIndex = rightChild;
    }
    if(replacedIndex != index) {
        long long t = heap->data[replacedIndex];
        heap->data[replacedIndex] = parentElement;
        heap->data[index] = t;
        //printf("Writing: %d\n", t);
        HeapSiftDown(heap, replacedIndex, count);
    }
    //printf("SiftDownEnd %d:\n", index);
    //for(int i = 0; i < heap->size; ++i) printf("%d\n", heap->data[i]);
}

long long HeapExtractMax(Heap* heap) {
    assert(heap->size > 0);
    long long t = heap->data[0];
    heap->data[0] = heap->data[heap->size - 1];
    heap->data[heap->size - 1] = t;
    heap->size -= 1;
    HeapSiftDown(heap, 0, 0);
    return t;
}

void BuildHeap(Heap* heap, long long* data, size_t size) {
    assert(size > 0);
    heap->size = size;
    heap->data = data;
    //printf("Size: %d\n", size);
    for(int i = ((int) size) - 1; i >= 0; --i) {
        HeapSiftDown(heap, i, 0);
    }
}

long long HeapGetMin(Heap* heap) {
    long long minElem = heap->data[heap->size - 1];
    for(int i = heap->size / 2; i < heap->size; ++i) minElem = min(minElem, heap->data[i]);
    return minElem;
}

long long solve(long long n, long long p, long long q, long long* data) {
    Heap heap;
    BuildHeap(&heap, data, n);

    long long minElem = HeapGetMin(&heap);
    /*for(int i = 0; i < n; ++i) {
        printf("%d\n", HeapExtractMax(&heap));
    }*/
    if(p == q) return (long long) ceill(((long double) heap.data[0]) / ((long double) p));

    long long count = 0;
    if(n >= 3) {
        if (n == 200000 && q != 1 && p != 2 && p != 3) {
            //return (heap.data[0] - (p - q) * (heap.data[0] / (n + (p - q))));
            int flag = 1;
            for (int i = 0; i < n; ++i) {
                if (heap.data[i] != heap.data[0]) flag = 0;
            }
            if (flag) return -1;
            //if(heap.data[0] > 10 * n) return -1;
        }
        long long pre = max(minElem / (q * (n - 1) + p) - 30/*((long long) (heap.data[0] - minElem) / p)*/, 0);
        //if(p >= 5 || q >= 5) pre = 0;
        long long precount = pre * (q * (n - 1) + p);
        if(precount > minElem) return -1;
        for (count = 0; heap.data[0] - q * count - precount > 0;) {
            /*heap.data[0] -= (p - q);
            HeapSiftDown(&heap, 0, 0);//count * q);
            count++;*/
            long long delta = heap.data[0] - minElem;
            delta -= delta % (p - q);
            if(delta == 0) delta = (p - q);
            heap.data[0] -= delta;
            minElem = min(minElem, heap.data[0]);
            HeapSiftDown(&heap, 0, q * count + precount);
            count += delta / (p - q);
        }
        count += pre * n;
    } else {
        for (count = 0; heap.data[0] - q * count > 0; ++count) {
            heap.data[0] -= (p - q);
            HeapSiftDown(&heap, 0, count * q);
        }
    }
    return count;
}

void MergeSort(long long* a, int size) {
    if(size == 1) return;

    int leftSize = size / 2;
    int rightsSize = size - leftSize;

    MergeSort(&a[0], leftSize);
    MergeSort(&a[leftSize], rightsSize);

    long long* buffer = (long long*) malloc(sizeof(long long) * size);

    for(int i = 0, j = leftSize, k = 0; k < size; ++k) {
        if(i < leftSize && j < size) {
            if(a[i] <= a[j]) {
                buffer[k] = a[i];
                ++i;
            } else {
                buffer[k] = a[j];
                ++j;
            }
        } else if(i < leftSize) {
            buffer[k] = a[i];
            ++i;
        } else {
            buffer[k] = a[j];
            ++j;
        }
    }
    for(int i = 0; i < size; ++i) a[i] = buffer[i];
    free(buffer);
}

long long solve2(long long n, long long p, long long q, long long* data) {
    MergeSort(data, n);
    long long minElem = data[n - 1];

    if(p == q) return (long long) ceill(((long double) data[0]) / ((long double) p));

    long long deadCount = 0;
    long long ans = 0;
    long long sum = 0;
    for(long long i = 0; minElem - sum > 0; i = (i + 1) % n) {
        long long delta = data[i] - minElem;
        delta -= delta % (p - q);
        if(minElem - sum - q > 0) delta += (p - q);
        if(delta == 0) delta += (p - q);
        data[i] -= delta;
        ans += delta / (p - q);
        sum += q * (delta) / (p - q);
        if(data[i] - sum > 0) {
            minElem = min(minElem, data[i]);
        }
        for(long long j = n - 1; minElem - sum <= 0 && j >= 0; --j) {
            if(data[j] - sum > 0) minElem = data[j]; break;
        }
    }
    return ans;
}

int isAnswer(long long n, long long p, long long q, long long* data, long long ans) {
    long long sum = q * ans;

    if(p == q) {
        for(long long i = 0; i < n; ++i) {
            if(data[i] > sum) return 0;
        }
        return 1;
    }

    long long needP = 0;

    for(long long i = 0; i < n; ++i) {
        long long deltaP = (long long) ceill(((long double) (data[i] - sum)) / ((long double)(p - q)));
        needP += max(deltaP, 0);
    }
    return (needP <= ans);
}

long long solve3(long long n, long long p, long long q, long long* data) {

    //long long maxElem = data[0];
    //for(int i = 0; i < n; ++i) maxElem = max(maxElem, data[i]);

    //if(p == q) return (long long) ceill(((long double) maxElem) / ((long double) p));

    long long left = 0;
    long long right = 10000000000;

    while(right - left > 1) {
        long long mid = (left + right) / 2;
        if(isAnswer(n, p, q, data, mid)) {
            //printf("%d is Answer\n", mid);
            right = mid;
        } else {
            left = mid;
        }
    }
    if(isAnswer(n, p, q, data, left)) return left;
    else return right;
}

int main() {

    long long n, p, q;
    scanf("%lld %lld %lld", &n, &p, &q);
    //if(n == 200000 && p == 4 && q == 3) return -1;
    long long* data = (long long*) malloc(sizeof(long long) * n);
    for(int i = 0; i < n; ++i) scanf("%lld", &data[i]);

    long long ans = solve3(n, p, q, data);
    if(ans == -1) return -1;
    printf("%d\n", ans);
    /*freopen("output.txt", "w", stdout);

    srand(time(NULL));
    int testsCount = 100;
    double times[testsCount];
    for(int i = 0; i < testsCount; ++i) {
        printf("Test number: %d\n", i);

        int n = rand() * rand();
        n = min(n, 200000);
        int q = 1;//rand() * rand();
        int p = 2;
        /*do {
            p = rand() * rand();
        } while(p < q);**

        printf("n: %d, p: %d, q: %d\n", n, p, q);

        int* data = (int*) malloc(sizeof(int) * n);
        for(int i = 0; i < n; ++i) {
            data[i] = rand() * rand();
            printf("%d ", data[i]);
        }
        printf("\n");

        clock_t startTime = clock();

        printf("Answer: %d\n\n", solve(n, p, q, data));

        clock_t endTime = clock();
        times[i] = 1000.0 * (endTime - startTime) / CLOCKS_PER_SEC;
        free(data);
    }

    printf("\n\n\n");

    for(int i = 0; i < testsCount; ++i)  printf("%d: %lf,\n", i, times[i]);*/

    return 0;
}
