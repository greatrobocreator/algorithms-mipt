#include <stdio.h>
#include <stdlib.h>

#define max(a, b) ((a) > (b) ? (a) : (b))

typedef struct Node Node;
struct Node {
    Node* prev;
    long long data;
};

typedef struct Stack {
    Node* top;
    size_t size;
} Stack;

void stackInit(Stack* stack) {
    stack->top = NULL;
    stack->size = 0;
}

int stackTop(Stack* stack, long long* data) {
    if(stack->size > 0) {
        (*data) = stack->top->data;
        return 0;
    } else {
        return -1;
    }
}

int stackPop(Stack* stack, long long* data) {

    int code = stackTop(stack, data);
    if(code == 0 && stack->size > 0) {
        Node* prevTop = stack->top;
        stack->top = stack->top->prev;
        free(prevTop);
        stack->size--;
        return 0;
    } else return code;
}

int stackPush(Stack* stack, long long data) {
    Node* newNode = (Node*) malloc(sizeof(Node));
    newNode->prev = stack->top;
    stack->top = newNode;
    stack->top->data = data;
    stack->size++;
    return 0;
}

void stackClear(Stack* stack) {
    int code;
    while(stack->size > 0) stackPop(stack, &code);
}

int main() {

    long long n;
    scanf("%lld", &n);

    long long leftF[n];
    long long rightF[n];

    long long heights[n];
    for(int i = 0; i < n; ++i) {
        long long h;
        scanf("%lld", &h);
        heights[i] = h;
    }

    Stack stack;
    stackInit(&stack);

    for(int i = 0; i < n; ++i) {
        long long top = 0;
        stackTop(&stack, &top);
        for(; stack.size > 0 && heights[top] >= heights[i]; stackTop(&stack, &top)) stackPop(&stack, &top);

        if(stack.size <= 0) {
            leftF[i] = -1;
            //ans = max(ans, (i + 1) * h);
        } else {
            leftF[i] = top;
            //ans = max(ans, (i - top) * h);
            //ans = max(ans, f[top] + (i - top) * heights[top]);
        }
        stackPush(&stack, i);
    }

    stackClear(&stack);

    for(int i = n - 1; i >= 0; --i) {
        long long top = 0;
        stackTop(&stack, &top);
        for(; stack.size > 0 && heights[top] >= heights[i]; stackTop(&stack, &top)) stackPop(&stack, &top);

        if(stack.size <= 0) {
            rightF[i] = n;
        } else {
            rightF[i] = top;
        }
        stackPush(&stack, i);
    }

    long long ans = 0;
    for(int i = 0; i < n; ++i) {
        ans = max(ans, (rightF[i] - leftF[i] - 1) * heights[i]);
    }

    printf("%lld", ans);

    return 0;
}
