#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node Node;
struct Node {
    Node *prev;
    int data;
};

typedef struct LinkedList {
    Node *head, *tail, *center;
    size_t size;
} LinkedList;

void linkedListInit(LinkedList *list) {
    list->head = NULL;
    list->tail = NULL;
    list->center = NULL;
    list->size = 0;
}

int linkedListFront(LinkedList *list, int *data) {
    if (list->size > 0 && list->head != NULL) {
        (*data) = list->head->data;
        return 0;
    }
    return -1;
}

int linkedListPopFront(LinkedList *list, int *data) {
    int code;
    code = linkedListFront(list, data);
    if (code == 0 && list->size > 0) {
        Node *lastHead = list->head;
        list->head = lastHead->prev;
        list->size--;
        if (list->size % 2 == 1) list->center = list->center->prev;
        free(lastHead);
        if(list->size == 0) {
            list->head = NULL;
            list->tail = NULL;
            list->center = NULL;
        }
        return 0;
    }
    return -1;
}

int linkedListPushBack(LinkedList *list, int data) {
    Node *newNode = (Node *) malloc(sizeof(Node));
    if (newNode == NULL) return -1;
    newNode->prev = NULL;
    if (list->tail != NULL) list->tail->prev = newNode;
    list->tail = newNode;
    newNode->data = data;
    list->size++;
    if (list->size == 1) {
        list->head = list->tail;
        list->center = list->tail;
    } else if (list->size % 2 == 1) list->center = list->center->prev;
    return 0;
}

int linkedListPushCenter(LinkedList *list, int data) {
    Node *newNode = (Node *) malloc(sizeof(Node));
    if (newNode == NULL) return -1;
    if(list->center != NULL) {
        newNode->prev = list->center->prev;
        list->center->prev = newNode;
    } else newNode->prev = NULL;
    list->size++;
    if (list->size % 2 == 1) list->center = newNode;
    if (list->size == 1) {
        list->head = list->center;
        list->tail = list->center;
    }
    if(newNode->prev == NULL) list->tail = newNode;
    newNode->data = data;
    return 0;
}

int main() {

    LinkedList list;
    linkedListInit(&list);

    int n;
    scanf("%d", &n);

    char cmd[10];
    for (int j = 0; j < n; ++j) {
        scanf("%9s", cmd);
        if (strcmp(cmd, "+") == 0) {
            int i;
            scanf("%d", &i);
            linkedListPushBack(&list, i);
        } else if (strcmp(cmd, "*") == 0) {
            int i;
            scanf("%d", &i);
            linkedListPushCenter(&list, i);
        } else if (strcmp(cmd, "-") == 0) {
            int i;
            int code = linkedListPopFront(&list, &i);
            if (code == 0) printf("%d\n", i);
            else {
                printf("Error: %d", code);
                break;
            }
        } else {
            printf("Something went wrong!\n");
            printf("cmd: %s", cmd);
            break;
        }
    }

    return 0;
}
