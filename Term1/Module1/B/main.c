#include <stdio.h>
#include <stdlib.h>

typedef struct Node Node;
struct Node {
    Node* prev;
    int data;
};

typedef struct Stack {
    Node* top;
    size_t size;
} Stack;

void stackInit(Stack* stack) {
    stack->top = NULL;
    stack->size = 0;
}

int stackTop(Stack* stack, int* data) {
    if(stack->size > 0) {
        (*data) = stack->top->data;
        return 0;
    } else {
        return -1;
    }
}

int stackPop(Stack* stack, int* data) {

    int code = stackTop(stack, data);
    if(code == 0 && stack->size > 0) {
        Node* prevTop = stack->top;
        stack->top = stack->top->prev;
        free(prevTop);
        stack->size--;
        return 0;
    } else return code;
}

int stackPush(Stack* stack, int data) {
    Node* newNode = (Node*) malloc(sizeof(Node));
    newNode->prev = stack->top;
    stack->top = newNode;
    stack->top->data = data;
    stack->size++;
    return 0;
}

void stackClear(Stack* stack) {
    int code;
    while(stack->size > 0) stackPop(stack, &code);
}

int findInArray(char* array, int size, char elem) {
    for(int i = 0; i < size; ++i) {
        if(array[i] == elem)  return i;
    }
    return -1;
}

int solve() {
    Stack stack;
    stackInit(&stack);

    char open[] = {'(', '[', '{'};
    char close[] = {')', ']', '}'};

    char c;
    while((c = (char) getchar()) != '\n') {
        int i = findInArray(open, 3, c);
        if(i != -1) {
            stackPush(&stack, i);
        } else {
            i = findInArray(close, 3, c);
            if(stack.size <= 0) return 0;
            int top;
            stackTop(&stack, &top);
            if(top != i) return 0;
            stackPop(&stack, &top);
        }
    }
    if(stack.size > 0) return 0;
    return 1;
}

int main() {

    printf("%s", (solve() ? "yes" : "no"));

    return 0;
}
