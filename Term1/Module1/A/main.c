#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node Node;
struct Node {
        Node* prev;
        int data;
};

typedef struct Stack {
        Node* top;
        size_t size;
} Stack;

void StackInit(Stack* stack) {
    stack->top = NULL;
    stack->size = 0;
}

int stackTop(Stack* stack, int* data) {
    if(stack->size > 0) {
        (*data) = stack->top->data;
        return 0;
    } else {
        return -1;
    }
}

int stackPop(Stack* stack, int* data) {

    int code = stackTop(stack, data);
    if(code == 0 && stack->size > 0) {
        Node* prevTop = stack->top;
        stack->top = stack->top->prev;
        free(prevTop);
        stack->size--;
        return 0;
    } else return code;
}

int stackPush(Stack* stack, int data) {
    Node* newNode = (Node*) malloc(sizeof(Node));
    newNode->prev = stack->top;
    stack->top = newNode;
    stack->top->data = data;
    stack->size++;
    return 0;
}

void stackClear(Stack* stack) {
    int code;
    while(stack->size > 0) stackPop(stack, &code);
}

int main() {
    Stack stack;
    StackInit(&stack);

    char s[10];

    while(1) {
        scanf("%9s", s);
        if(strcmp(s, (char*) "exit") == 0) {
            printf("bye\n");
            break;
        } else if(strcmp(s, "push") == 0) {
            int n;
            scanf("%d", &n);
            stackPush(&stack, n);
            printf("ok\n");
        } else if(strcmp(s, "back") == 0) {
            int data;
            int code = stackTop(&stack, &data);
            if(code == 0) printf("%d\n", data);
            else printf("error\n");
        } else if(strcmp(s, "pop") == 0) {
            int data;
            int code = stackPop(&stack, &data);
            if(code == 0) printf("%d\n", data);
            else printf("error\n");
        } else if(strcmp(s, "size") == 0) {
            printf("%d\n", stack.size);
        } else if(strcmp(s, "clear") == 0) {
            stackClear(&stack);
            printf("ok\n");
        } else {
            printf("Something went wrong!\n");
            printf("s: %s", s);
            break;
        }
    }

    return 0;
}