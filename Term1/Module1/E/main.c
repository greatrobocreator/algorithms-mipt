#include <stdio.h>
#include <malloc.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))

int solve(int* a, int* b, int l) {
    int left = 0;
    int right = l - 1;

    while(right - left > 1) {
        int m = (left + right) / 2;
        if(a[m] > b[m]) right = m;
        else left = m;
    }

    return (max(a[left], b[left]) > max(a[right], b[right]) ? right : left);
    /*for(int i = 0; right - left > 1 && i < 100; ++i) {
        int m = (right + left) / 2;
        if(b[m] > a[m]) left = m;
        else right = m;
    }
    int min = left;
    for(int i = left; i <= right; ++i) if(max(a[i], b[i]) < max(a[min], b[min])) min = i;
    return min;*/


}

int main() {

    int n, m, l;
    scanf("%d %d %d", &n, &m, &l);

    //int a[n][l];
    //int b[m][l];
    int** a = (int**) malloc(n * sizeof(int *));
    for(int i = 0; i < n; ++i) a[i] = (int* ) malloc(l * sizeof(int));

    int** b = (int**) malloc(m * sizeof(int *));
    for(int i = 0; i < m; ++i) b[i] = (int* ) malloc(l * sizeof(int));

    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < l; ++j) {
            scanf("%d", &a[i][j]);
        }
    }

    for(int i = 0; i < m; ++i) {
        for(int j = 0; j < l; ++j) {
            scanf("%d", &b[i][j]);
        }
    }

    int q;
    scanf("%d", &q);

    int i, j;
    for(int k = 0; k < q; ++k) {
        scanf("%d %d", &i, &j);;
        printf("%d\n", solve(a[i - 1], b[j - 1], l) + 1);
    }


    return 0;
}
