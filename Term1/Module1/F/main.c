#include <stdio.h>
#include <malloc.h>

long long inversionsCount(int* a, int size) {
    if(size == 1) return 0;

    int leftSize = size / 2;
    int rightsSize = size - leftSize;

    long long count = inversionsCount(&a[0], leftSize);
    count += inversionsCount(&a[leftSize], rightsSize);

    int* buffer = (int*) malloc(sizeof(int) * size);

    for(int i = 0, j = leftSize, k = 0; k < size; ++k) {
        if(i < leftSize && j < size) {
            if(a[i] <= a[j]) {
                buffer[k] = a[i];
                ++i;
                count += j - leftSize;
            } else {
                buffer[k] = a[j];
                ++j;
            }
        } else if(i < leftSize) {
            buffer[k] = a[i];
            ++i;
            count += j - leftSize;
        } else {
            buffer[k] = a[j];
            ++j;
        }
    }
    for(int i = 0; i < size; ++i) a[i] = buffer[i];
    free(buffer);
    return count;
}

int main() {

    freopen("inverse.in", "r", stdin);
    freopen("inverse.out", "w", stdout);

    int n;
    scanf("%d", &n);

    int a[n];
    for(int i = 0; i < n; ++i) scanf("%d", &a[i]);

    printf("%lld\n", inversionsCount(a, n));

    return 0;
}
