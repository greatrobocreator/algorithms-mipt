#include <bits/stdc++.h>

long long Partition(std::vector<long long> &a, long long left, long long right, long long x) {

    while(true) {
        for(; a[left] <= x && left < a.size(); ++left);
        for(; a[right] > x && right >= 0; --right);

        if(right <= left) return right;

        std::swap(a[left], a[right]);
    }
    /*std::vector<long long> buffer;
    for(int i = left; i <= right; ++i) {
        if(a[i] <= x) buffer.push_back(a[i]);
    }
    long long result = buffer.size() - 1 + left;
    for(int i = left; i <= right; ++i) {
        if(a[i] > x) buffer.push_back(a[i]);
    }
    for(int i = left; i <= right; ++i) {
        a[i] = buffer[i - left];
    }
    return result;*/
}

bool isConstArray(std::vector<long long> &a, long long left, long long right) {
    for(int i = left; i <= right; ++i) {
        if(a[i] != a[left]) return false;
    }
    return true;
}

long long kth_element(std::vector<long long> &a, long long k) {

    long long left = 0, right = a.size() - 1;

    while(right - left > 0) {
        if(isConstArray(a, left, right)) return a[left];
        long long x = a[rand() % (right - left + 1) + left];
        long long i = Partition(a, left, right, x);
        if (k <= i) right = i;
        else left = i + 1;
    }
    return a[left];
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    srand(time(NULL));
    long long n, k;
    std::cin >> n >> k;

    std::vector<long long> a(n, 0);
    for(long long i = 0; i < n; ++i) std::cin >> a[i];

    std::cout << kth_element(a, k) << std::endl;

    return 0;
}
