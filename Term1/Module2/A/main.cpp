#include <bits/stdc++.h>

#define max(a, b) ((a) > (b) ? (a) : (b))

bool compareStrings(const std::string &a, const std::string &b) {
    //int mult = 1;
    /*if (a.size() < b.size()) {
        swap(a, b);
        mult = -1;
    }*/

    /*int result = a.compare(0, b.size(), b) * mult;
    if(result == 0) {
        if(a.size() == b.size()) return true;
        return compareStrings(a.substr(b.size()), b) ^ (mult < 0);
    }
    else return result > 0;*/
    /*while (true) {
        if (a.size() < b.size()) {
            swap(a, b);
            mult *= -1;
        }
        int result = a.compare(0, b.size(), b) * mult;
        if (result == 0) {
            if (a.size() == b.size()) return mult > 0;
            a = a.substr(b.size());
        } else return result > 0;
    }*/
    /*if(a.size() == 0 || b.size() == 0) return true;
    for(int i = 0; i < max(a.size(), b.size()); ++i) {
        char ai = a[i % a.size()];
        char bi = b[i % b.size()];
        if(ai < bi) return false;
        if(ai > bi) return true;
    }
    return true;*/
    return (a + b > b + a);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::fstream input;
    input.open("number.in", std::fstream::in);

    std::fstream output;
    output.open("number.out", std::fstream::out);

    std::vector<std::string> numbers;
    for (std::string line; std::getline(input, line);) numbers.push_back(line);

    std::sort(numbers.begin(), numbers.end(), compareStrings);

    for (std::string &s : numbers) output << s;
    output << std::endl;

    input.close();
    output.close();

    return 0;
}
