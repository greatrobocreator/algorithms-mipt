#include <bits/stdc++.h>

long long getIthDigit(long long n, long long i) {
    n >>= (8 * i);
    return n % (1 << 8);
}

void lsd_sort(std::vector< long long >& numbers) {
    for(long long i = 0; i < 8; ++i) {
        std::vector<long long> count(256, 0);
        std::vector< long long > buffer(numbers.size(), 0);
        for(long long number : numbers) count[getIthDigit(number, i)]++;
        for(long long j = 1; j < count.size(); ++j) count[j] += count[j - 1];
        for(long long j = numbers.size() - 1; j >= 0; j--) buffer[--count[getIthDigit(numbers[j], i)]] = numbers[j];
        for(long long j = 0; j < numbers.size(); ++j) numbers[j] = buffer[j];
    }
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    std::vector< long long > numbers(n, 0);

    for(long long i = 0; i < n; ++i) std::cin >> numbers[i];

    lsd_sort(numbers);

    for(long long number : numbers) std::cout << number << " ";
    std::cout << std::endl;

    return 0;
}
