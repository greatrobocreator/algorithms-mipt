#include <bits/stdc++.h>

class Heap {
public:
    Heap(std::vector<long long> &_data) : data(_data) {};

    void BuildHeap();
    void Insert(long long value);
    long long GetMax();
    void ExtractMax();

private:
    void SiftUp(size_t index);
    void SiftDown(size_t index);

    std::vector<long long> &data;
};

void Heap::BuildHeap() {
    assert(!data.empty());
    for (size_t i = data.size() - 1; i >= 0; --i) SiftDown(i);
}

void Heap::SiftUp(size_t index) {
    while (index > 0 && data[index] > data[(index - 1) / 2]) {
        std::swap(data[index], data[(index - 1) / 2]);
        index = (index - 1) / 2;
    }
}

void Heap::SiftDown(size_t index) {
    while (2 * index + 1 < data.size()) {
        long long j = -1;
        if (data[2 * index + 1] > data[index]) j = 2 * index + 1;
        if (2 * index + 2 < data.size() && data[2 * index + 2] > data[index] &&
            (j == -1 || data[2 * index + 1] < data[2 * index + 2]))
            j = 2 * index + 2;
        if (j == -1) return;
        std::swap(data[index], data[j]);
        index = j;
    }
}

void Heap::Insert(long long value) {
    data.push_back(value);
    SiftUp(data.size() - 1);
}

long long Heap::GetMax() {
    return data[0];
}

void Heap::ExtractMax() {
    std::swap(data[0], data[data.size() - 1]);
    data.pop_back();
    SiftDown(0);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, k;
    std::cin >> n >> k;

    std::vector<long long> data;
    Heap heap(data);

    for(long long i = 0; i < n; ++i) {
        long long x;
        std::cin >> x;
        heap.Insert(x);
        if(i >= k) heap.ExtractMax();
    }

    std::vector<long long> answer;
    for(long long i = 0; i < k; ++i) {
        answer.push_back(heap.GetMax());
        heap.ExtractMax();
    }
    for(long long i = k - 1; i >= 0; --i) {
        std::cout << answer[i] << " ";
    }
    std::cout << std::endl;

    return 0;
}
