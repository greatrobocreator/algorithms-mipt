#include <bits/stdc++.h>

class Heap {
public:
    Heap(std::vector<std::pair<long long, size_t> > &_data,
         bool (*_compare)(const std::pair<long long, size_t> &, const std::pair<long long, size_t> &)) : data(_data),
                                                                                                         compare(_compare) {};

    void BuildHeap();

    void Insert(long long value);

    long long GetRoot();

    void ExtractRoot();

    void SetSecondHeap(Heap *heap);

    size_t GetSize();

protected:
    void Swap(size_t i, size_t j);

    std::vector<std::pair<long long, size_t> > &GetData();

private:
    void SiftUp(size_t index);

    void SiftDown(size_t index);

    std::vector<std::pair<long long, size_t> > &data;

    bool (*compare)(const std::pair<long long, size_t> &, const std::pair<long long, size_t> &);

    Heap *secondHeap;
};

void Heap::Swap(size_t i, size_t j) {
    std::swap(secondHeap->GetData()[data[i].second].second, secondHeap->GetData()[data[j].second].second);
    std::swap(data[i], data[j]);
}

std::vector<std::pair<long long, size_t> > &Heap::GetData() {
    return data;
}

size_t Heap::GetSize() {
    return data.size();
}

void Heap::SetSecondHeap(Heap *heap) {
    secondHeap = heap;
}

void Heap::BuildHeap() {
    assert(!data.empty());
    for (size_t i = data.size() - 1; i >= 0; --i) SiftDown(i);
}

void Heap::SiftUp(size_t index) {
    while (index > 0 && index < data.size() && (*compare)(data[index], data[(index - 1) / 2])) {
        Swap(index, (index - 1) / 2);
        index = (index - 1) / 2;
    }
}

void Heap::SiftDown(size_t index) {
    while (2 * index + 1 < data.size()) {
        long long j = -1;
        if ((*compare)(data[2 * index + 1], data[index])) j = 2 * index + 1;
        if (2 * index + 2 < data.size() && (*compare)(data[2 * index + 2], data[index]) &&
            (j == -1 || (*compare)(data[2 * index + 2], data[2 * index + 1])))
            j = 2 * index + 2;
        if (j == -1) return;
        Swap(index, j);
        index = j;
    }
}

void Heap::Insert(long long value) {
    data.emplace_back(value, secondHeap->GetSize());
    secondHeap->GetData().emplace_back(value, GetSize() - 1);
    SiftUp(data.size() - 1);
    secondHeap->SiftUp(secondHeap->GetSize() - 1);
}

long long Heap::GetRoot() {
    return data[0].first;
}

void Heap::ExtractRoot() {
    size_t rootIndex = data[0].second;
    Swap(0, data.size() - 1);
    secondHeap->Swap(rootIndex, secondHeap->GetSize() - 1);
    data.pop_back();
    secondHeap->data.pop_back();
    secondHeap->SiftUp(rootIndex);
    SiftDown(0);
}

bool Greater(const std::pair<long long, size_t> &a, const std::pair<long long, size_t> &b) {
    return a.first > b.first;
}

bool Less(const std::pair<long long, size_t> &a, const std::pair<long long, size_t> &b) {
    return a.first < b.first;
}

int main() {

    long long n;
    std::cin >> n;

    std::vector<std::pair<long long, size_t> > maxHeapArray;
    Heap *maxHeap = new Heap(maxHeapArray, Greater);

    std::vector<std::pair<long long, size_t> > minHeapArray;
    Heap *minHeap = new Heap(minHeapArray, Less);

    maxHeap->SetSecondHeap(minHeap);
    minHeap->SetSecondHeap(maxHeap);

    for (int i = 0; i < n; ++i) {
        char cmd_c[100];
        getchar(); // To read '\n'
        scanf("%100[^(\n]s", cmd_c); // Reading command until '('
        std::string cmd = cmd_c;

        if (cmd == "Insert") {
            long long x;
            scanf("(%lld)", &x);
            maxHeap->Insert(x);
        } else if (cmd == "GetMax") {
            std::cout << maxHeap->GetRoot() << std::endl;
            maxHeap->ExtractRoot();
        } else if (cmd == "GetMin") {
            std::cout << minHeap->GetRoot() << std::endl;
            minHeap->ExtractRoot();
        } else {
            std::cout << "Something went wrong!" << std::endl;
        }
    }

    delete maxHeap;
    delete minHeap;

    return 0;
}
