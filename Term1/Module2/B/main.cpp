#include <bits/stdc++.h>

using ll = long long;

int main() {

    ll n;
    std::cin >> n;

    std::vector< std::pair< ll, ll > > points;
    for(ll i = 0; i < n; ++i) {
        ll l, r;
        std::cin >> l >> r;
        points.push_back({l, 1});
        points.push_back({r, -1});
    }
    std::sort(points.begin(), points.end());

    ll ans = 0;
    ll maxBalance = 0;
    ll balance = 0;
    ll lastL = -1;
    for(ll i = 0; i < 2 * n; ++i) {
        if(balance == 0) {
            maxBalance = 0;
        }
        if(balance == 1 || balance + points[i].second == 0) {
            ans += points[i].first - lastL;
            lastL = points[i].first;
        }
        lastL = points[i].first;
        balance += points[i].second;
        maxBalance = std::max(maxBalance, balance);
    }

    std::cout << ans << std::endl;

    return 0;
}
