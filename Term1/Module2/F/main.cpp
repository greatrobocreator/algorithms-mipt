#include <bits/stdc++.h>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long l, n, k;
    std::cin >> l >> n >> k;
    if(n == 0) return -1;
    long long blockSize = l / k;

    std::vector< long long > f(k, 0);

    std::vector< std::pair< long long , long long > > painted(n); // std::pair< indexInBlock, blockIndex >
    for(long long i = 0; i < n; ++i) {
        long long x;
        std::cin >> x;
        x--;
        painted[i] = {x % blockSize, x / blockSize};
        f[painted[i].second]++;
    }
    std::sort(painted.begin(), painted.end());

    long long min = LLONG_MAX;
    long long max = 0;
    long long minDiff = LLONG_MAX;
    long long ansCount = 1;
    long long ansIndex = l - 1;


    for(long long j = 0; j < k; ++j) {
        max = std::max(max, f[j]);
        min = std::min(min, f[j]);
    }
    minDiff = max - min;

    long long offset = 0;
    for(long long i = 0; i < n;) {
        if(max - min == minDiff) ansCount += painted[i].first - offset - 1;
        offset = painted[i].first;
        for(; i < n && painted[i].first == offset; ++i) {
            long long prevBlockIndex = (painted[i].second - 1 + k) % k;
            f[painted[i].second]--;
            f[prevBlockIndex]++;
        }
        max = *std::max_element(f.begin(), f.end());
        min = *std::min_element(f.begin(), f.end());
        long long diff = max - min;
        if(diff < minDiff) {
            minDiff = diff;
            ansCount = 1;
            ansIndex = offset;
        } else if(diff == minDiff){
            ansCount++;
        }
    }
    if(max - min == minDiff) ansCount += blockSize - offset - 1;

    std::cout << minDiff << " " << ansCount * k << std::endl;
    std::cout << ansIndex + 1;

    return 0;
}