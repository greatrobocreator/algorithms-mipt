/**

Реализуйте двоичную кучу.

Обработайте запросы следующих видов:

 + insert x: вставить целое число x в кучу;
 + getMin: вывести значение минимального элемента в куче (гарантируется, что к этому моменту куча не пуста);
 + extractMin: удалить минимальный элемент из кучи (гарантируется, что к этому моменту куча не пуста);
 + decreaseKey i Δ: уменьшить число, вставленное на i-м запросе, на целое число Δ≥0 (гарантируется, что i-й запрос был осуществлён ранее, являлся запросом добавления, а добавленное на этом шаге число всё ещё лежит в куче).
 + Можете считать, что в любой момент времени все числа, хранящиеся в куче, попарно различны, а их количество не превышает 10^5.

**/

#include <bits/stdc++.h>

class Heap {

public:
    Heap(std::vector<std::pair<long long, long long> > &_data);

    void BuildHeap();

    void Insert(long long value, long long currentTime);

    void DecreaseKey(long long time, long long delta);

    long long GetMin();

    void ExtractMin();

private:
    void Swap(int i, int j);

    void SiftUp(int index);

    void SiftDown(int index);

    std::vector<std::pair<long long, long long> > &data;
public:
    std::vector<long long> indexByTime;
};

Heap::Heap(std::vector<std::pair<long long, long long>> &_data) : data(_data) {}

void Heap::Swap(int i, int j) {
    std::swap(indexByTime[data[i].second], indexByTime[data[j].second]);
    std::swap(data[i], data[j]);
}

void Heap::BuildHeap() {
    assert(!data.empty());
    for (int i = data.size() - 1; i >= 0; --i) SiftDown(i);
}

void Heap::SiftUp(int index) {
    while (index > 0 && data[index] < data[(index - 1) / 2]) {
        Swap(index, (index - 1) / 2);
        index = (index - 1) / 2;
    }
}

void Heap::SiftDown(int index) {
    while (2 * index + 1 < data.size()) {
        long long j = -1;
        if (data[2 * index + 1] < data[index]) j = 2 * index + 1;
        if (2 * index + 2 < data.size() && data[2 * index + 2] < data[index] &&
            (j == -1 || data[2 * index + 1] > data[2 * index + 2]))
            j = 2 * index + 2;
        if (j == -1) return;
        Swap(index, j);
        index = j;
    }
}

void Heap::Insert(long long value, long long currentTime) {
    data.emplace_back(value, currentTime);
    indexByTime.push_back(data.size() - 1);
    SiftUp(data.size() - 1);
}

void Heap::DecreaseKey(long long time, long long delta) {
    data[indexByTime[time]].first -= delta;
    SiftUp(indexByTime[time]);
    indexByTime.push_back(-1);
}

long long Heap::GetMin() {
    indexByTime.push_back(-1);
    return data[0].first;
}

void Heap::ExtractMin() {
    assert(!data.empty());
    Swap(0, data.size() - 1);
    data.pop_back();
    SiftDown(0);
    indexByTime.push_back(-1);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long q;
    scanf("%lld", &q);

    std::vector<std::pair<long long, long long> > data;
    Heap *heap = new Heap(data);

    char cmd;

    for (long long i = 0; i < q; ++i) {
        getchar();
        cmd = getchar();
        if (cmd == 'i') {
            for(int j = 0; j < 6; ++j) getchar();
            long long x;
            scanf("%lld", &x);
            heap->Insert(x, i);
        } else if (cmd == 'd') {
            for(int j = 0; j < 11; ++j) getchar();
            long long j, delta;
            scanf("%lld %lld", &j, &delta);
            heap->DecreaseKey(j - 1, delta);
        } else if (cmd == 'g') {
            for(int j = 0; j < 5; ++j) getchar();
            printf("%lld\n", heap->GetMin());
        } else if (cmd == 'e') {
            for(int j = 0; j < 9; ++j) getchar();
            heap->ExtractMin();
        } else {
            std::cout << "Something went wrong!" << std::endl;
        }
    }

    delete heap;
    return 0;
}