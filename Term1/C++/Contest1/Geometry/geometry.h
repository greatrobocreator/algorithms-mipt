#pragma once

#define _USE_MATH_DEFINES
#include <cmath>
#include <type_traits>
#include <limits>
#include <vector>
#include <stdexcept>

// from https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
template<class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
compare(T x, T y, int ulp = 1000000)
{
    // the machine epsilon has to be scaled to the magnitude of the values used
    // and multiplied by the desired precision in ULPs (units in the last place)
    return std::fabs(x-y) <= std::numeric_limits<T>::epsilon() * std::fabs(x+y) * ulp
           // unless the result is subnormal
           || std::fabs(x-y) < std::numeric_limits<T>::min();
}

class Line;

struct Point {
    long double x, y;

    Point(long double x = 0, long double y = 0) : x(x), y(y) {};

    Point operator-() const;
    Point& operator+=(const Point& r);
    Point& operator-=(const Point& r);
    Point& operator*=(long double k);
    Point& operator/=(long double k);

    long double abs() const;
    long double sqAbs() const;
    Point& normalize();
    Point& rotate(long double angle);
    Point& reflex();
    Point& reflex(const Line& axis);
};

long double operator*(const Point& l, const Point& r) {
    return l.x * r.y - r.x * l.y;
}

long double operator^(const Point& l, const Point& r) {
    return l.x * r.x + l.y * r.y;
}

Point& Point::operator*=(long double k) {
    x *= k;
    y *= k;
    return *this;
}

Point& Point::operator/=(long double k) {
    return (*this) *= 1 / k;
}

Point& Point::operator+=(const Point &r) {
    x += r.x;
    y += r.y;
    return *this;
}

Point Point::operator-() const {
    return Point(-x, -y);
}

Point& Point::operator-=(const Point &r) {
    return (*this) += -r;
}

long double Point::abs() const {
    return sqrtl(sqAbs());
}

Point& Point::normalize() {
    return (*this) /= abs();
}

Point& Point::rotate(long double angle) {
    angle = (angle * M_PI) / 180;
    long double newX = x * cosl(angle) - y * sinl(angle);
    long double newY = y * cosl(angle) + x * sinl(angle);
    x = newX;
    y = newY;
    return (*this);
}

long double Point::sqAbs() const {
    return x * x + y * y;
}

Point operator+(const Point& l, const Point& r) {
    Point result = l;
    result += r;
    return result;
}

Point operator-(const Point& l, const Point& r) {
    Point result = l;
    result -= r;
    return result;
}

Point operator/(const Point& l, long double r) {
    Point result = l;
    result /= r;
    return result;
}

Point operator*(const Point& l, long double r) {
    Point result = l;
    result *= r;
    return result;
}

Point& Point::reflex() {
    return (*this *= -1);
}

long double dist(const Point& p1, const Point& p2) {
    return (p2 - p1).abs();
}

bool operator==(const Point &l, const Point &r) {
    return compare(l.x, r.x) && compare(l.y, r.y);
}

bool operator!=(const Point& l, const Point& r) {
    return !(l == r);
}

class Line {
public:
    long double a = 0, b = 0, c = 0; // ax + by + c = 0

    Line(const Point& p1, const Point& p2);
    Line(long double k, long double b);
    Line(const Point& p, long double k);
    Line(long double a, long double b, long double c) : a(a), b(b), c(c) {};

    short Side(const Point& p) const;
    long double distance(const Point& p) const;
    Line perpendicular(const Point& p) const;
    Point normal() const;
};

Line::Line(const Point &p1, const Point &p2) {
    a = p2.y - p1.y;
    b = p1.x - p2.x;
    c = - (p1.y * b + p1.x * a);
    if(a < 0) {
        a *= -1;
        b *= -1;
        c *= -1;
    }
}

Line::Line(long double k, long double b) {
    a = k;
    b = -1;
    c = b;
}

Line::Line(const Point &p, long double k) {
    a = k;
    b = -1;
    c = p.y - k * p.x;
}

short Line::Side(const Point& p) const {
    long double side = a * p.x + b * p.y + c;
    if(compare(side, 0.0l)) return 0;
    return (side > 0.0l ? 1 : -1);
}

Line Line::perpendicular(const Point &p) const {
    return Line(b, -a, a * p.y - b * p.x);
}

long double Line::distance(const Point &p) const {
    return fabsl(a * p.x + b * p.y + c) / Point(a, b).abs();
}

Point Line::normal() const {
    return Point(a, b);
}

Point intersection(const Line& l, const Line& r) {
    long double det = l.a * r.b - l.b * r.a;
    return Point(-l.c * r.b + l.b * r.c, -l.a * r.c + l.c * r.a) / det;
}

bool operator==(const Line& l, const Line& r) {
    return compare(l.c * r.a, r.c * l.a) && compare(l.b * r.a, r.b * l.a);
}

Point& Point::reflex(const Line& axis) {
    return (*this -= 2 * axis.distance(*this) * axis.normal());
}

class Shape {
public:
    virtual long double perimeter() const = 0;
    virtual long double area() const = 0;
    virtual bool operator==(const Shape& another) const = 0;
    virtual bool operator!=(const Shape& another) const { return !(*this == another); }
    virtual bool isCongruentTo(const Shape& another) const = 0;
    virtual bool isSimilarTo(const Shape& another) const = 0;
    virtual bool containsPoint(const Point& another) const = 0;

    virtual void rotate(const Point& center, long double angle) = 0;
    virtual void reflex(const Point& center) = 0;
    virtual void reflex(const Line& axis) = 0;
    virtual void scale(const Point& center, long double coefficient) = 0;

    virtual ~Shape() {};
};

class Polygon : public Shape {
protected:
    std::vector< Point > vertices;

public:
    Polygon(std::vector< Point > _vertices);
    Polygon(std::initializer_list< Point > _vertices);

    long long verticesCount() const;
    const std::vector< Point >& getVertices() const;
    bool isConvex() const;
    std::vector< long double > getAngles() const;
    long double sideLength(long long i) const;

    long double area() const override;
    long double perimeter() const override;
    bool operator==(const Shape& another) const override;
    bool isCongruentTo(const Shape& another) const override ;
    bool isSimilarTo(const Shape& another) const override ;
    bool containsPoint(const Point& p) const override;

    void rotate(const Point& center, long double angle) override;
    void reflex(const Point& center) override;
    void reflex(const Line& axis) override;
    void scale(const Point& center, long double coefficient) override;

};

Polygon::Polygon(std::vector< Point > _vertices) : vertices(_vertices) {
    if(verticesCount() <= 2) {
        throw std::invalid_argument("Invalid polygon: verticesCount() <= 2");
    }
};

Polygon::Polygon(std::initializer_list< Point > _vertices) {
    vertices = _vertices;
    if(verticesCount() <= 2) {
        throw std::invalid_argument("Invalid polygon: verticesCount() <= 2");
    }
};

long long Polygon::verticesCount() const {
    return vertices.size();
}

const std::vector<Point> &Polygon::getVertices() const {
    return vertices;
}

bool Polygon::isConvex() const {
    long long n = verticesCount();
    for(long long i = 0; i < n; ++i) {
        Line edge(vertices[i], vertices[(i + 1) % n]);
        if(edge.Side(vertices[(i - 1 + n) % n]) * edge.Side(vertices[(i + 2) % n]) < 0) return false;
    }
    return true;
}

long double Polygon::area() const {
    long double sum = 0.0l;
    for(long long i = 0; i < verticesCount(); ++i) {
        sum += vertices[i] * vertices[(i + 1) % verticesCount()];
    }
    return fabsl(sum) / 2;
}

long double Polygon::perimeter() const {
    long double sum = 0.0l;
    for(long long i = 0; i < verticesCount(); ++i) {
        sum += dist(vertices[i], vertices[(i + 1) % verticesCount()]);
    }
    return sum;
}

bool Polygon::containsPoint(const Point &p) const {
    long double sum = 0.0l;
    long long n = verticesCount();
    for(long long i = 0; i < n; ++i) {
        Point v1 = (vertices[i] - p).normalize();
        Point v2 = (vertices[(i + 1) % n] - p).normalize();
        sum += atan2l(v1 * v2, v1 ^ v2);
    }
    return compare(static_cast<double>(fabsl(sum)), 2 * M_PI);
}

bool Polygon::operator==(const Shape &another) const {
    try {
        Polygon r = dynamic_cast<const Polygon&>(another);
        if(verticesCount() != r.verticesCount()) return false;
        long long n = verticesCount();

        long long i = 0;
        for(; i < n && r.vertices[i] != vertices[0]; ++i);
        if(i >= n) return  false;

        bool flag = true;
        for(long long j = 0; j < n; ++j) {
            if(vertices[j] != r.vertices[(i + j) % n]) flag = false;
        }
        if(flag) return true;
        for(long long j = 0; j < n; ++j) {
            if(vertices[j] != r.vertices[(i - j + n) % n]) return false;
        }
        return true;
    } catch(const std::bad_cast& e) {
        return false;
    }
}

bool Polygon::isCongruentTo(const Shape &another) const {
    try {
        Polygon r = dynamic_cast<const Polygon&>(another);
        if(r.verticesCount() != verticesCount()) return false;
        std::vector< long double > angles = getAngles();
        std::vector< long double > rAngles = r.getAngles();

        long long n = verticesCount();
        for(long long i = 0; i < n; ++i) {
            if(compare(angles[0], rAngles[i])) {
                bool flag = true;
                for(long long j = 0; j < n && flag; ++j) {
                    if(!compare(angles[j], rAngles[(i + j) % n]) || !compare(sideLength(j), r.sideLength(i + j))) {
                       flag = false;
                    }
                }
                if(flag) {
                    return true;
                }
                flag = true;
                for(long long j = 0; j < n && flag; ++j) {
                    if(!compare(angles[j], rAngles[(i - j + n) % n]) || !compare(sideLength(j), r.sideLength(i - j - 1))) {
                        flag = false;
                    }
                }
                if(flag) {
                    return true;
                }
            }
        }
        return false;
    } catch(const std::bad_cast& e) {
        return false;
    }
}

std::vector<long double> Polygon::getAngles() const {
    long long n = verticesCount();
    std::vector< long double > angles;
    angles.reserve(n);

    for(long long i = 0; i < n; ++i) {
        angles.push_back((vertices[(i + 1) % n] - vertices[i]).normalize() ^ (vertices[(i - 1 + n) % n] - vertices[i]).normalize());
    }

    return angles;
}

long double Polygon::sideLength(long long i) const {
    long long n = verticesCount();
    i = (i % n + n) % n;
    return dist(vertices[i], vertices[(i + 1) % n]);
}

bool Polygon::isSimilarTo(const Shape &another) const {
    try {
        Polygon r(dynamic_cast<const Polygon&>(another));
        r.scale(Point(0, 0), perimeter() / r.perimeter());
        bool ans = isCongruentTo(r);
        return ans;
    } catch(const std::bad_cast& e) {
        return false;
    }
}

void Polygon::rotate(const Point& center, long double angle) {
    for(auto& v : vertices) {
        (v -= center).rotate(angle) += center;
    }
}

void Polygon::reflex(const Point& center) {
    for(auto& v : vertices) {
        (v -= center).reflex() += center;
    }
}

void Polygon::reflex(const Line& axis) {
    for(auto& v : vertices) {
        v.reflex(axis);
    }
}

void Polygon::scale(const Point& center, long double coefficient) {
    for(auto& v : vertices) {
        ((v -= center) *= coefficient) += center;
    }
}

class Ellipse : public Shape {
protected:
    std::pair< Point, Point > _focuses;
    long double a;

    long double c() const;
    long double b() const;

public:

    Ellipse(const Point& f1, const Point& f2, long double d) : _focuses({ f1, f2 }), a(d / 2) {};

    const std::pair< Point, Point >& focuses() const;
    std::pair< Line, Line > directrices() const;
    long double eccentricity() const;
    Point center() const;

    long double area() const override;
    long double perimeter() const override;
    bool operator==(const Shape& another) const override;
    bool isCongruentTo(const Shape& another) const override ;
    bool isSimilarTo(const Shape& another) const override ;
    bool containsPoint(const Point& p) const override;

    void rotate(const Point& center, long double angle) override;
    void reflex(const Point& center) override;
    void reflex(const Line& axis) override;
    void scale(const Point& center, long double coefficient) override;
};

const std::pair<Point, Point> &Ellipse::focuses() const {
    return _focuses;
}

long double Ellipse::eccentricity() const {
    return c() / a;
}

std::pair<Line, Line> Ellipse::directrices() const {
    const Point offset = (focuses().first - center()) / (eccentricity() * eccentricity());
    const Line axis(focuses().first, focuses().second);
    return { axis.perpendicular(center() + offset), axis.perpendicular(center() - offset) };
}

long double Ellipse::area() const {
    return M_PI * a * b();
}

long double Ellipse::c() const {
    return dist(_focuses.first, _focuses.second) / 2;
}

long double Ellipse::b() const {
    return sqrtl(a * a - c() * c());
}

long double Ellipse::perimeter() const {
    return M_PI * (3 * (a + b()) - sqrtl((3 * a + b()) * (a + 3 * b())));
}

bool Ellipse::containsPoint(const Point &p) const {
    return (dist(p, focuses().first) + dist(p, focuses().second)) <= 2 * a;
}

Point Ellipse::center() const {
    return (_focuses.first + _focuses.second) / 2;
}

bool Ellipse::isSimilarTo(const Shape &another) const {
    try {
        const Ellipse& r = dynamic_cast<const Ellipse&>(another);
        return eccentricity() == r.eccentricity();
    } catch(const std::bad_cast& e) {
        return false;
    }
}

bool Ellipse::operator==(const Shape &another) const {
    try {
        const Ellipse& r = dynamic_cast<const Ellipse&>(another);
        return ((_focuses.first == r.focuses().first && _focuses.second == r.focuses().second) ||
                (_focuses.first == r.focuses().second && _focuses.second == r.focuses().first)) && compare(a, r.a);
    } catch(const std::bad_cast& e) {
        return false;
    }
}

bool Ellipse::isCongruentTo(const Shape &another) const {
    try {
        const Ellipse& r = dynamic_cast<const Ellipse&>(another);
        return eccentricity() == r.eccentricity() && c() == r.c();
    } catch(const std::bad_cast& e) {
        return false;
    }
}

void Ellipse::reflex(const Line &axis) {
    _focuses.first.reflex(axis);
    _focuses.second.reflex(axis);
}

void Ellipse::reflex(const Point &center) {
    (_focuses.first -= center).reflex() += center;
    (_focuses.second -= center).reflex() += center;
}

void Ellipse::rotate(const Point &center, long double angle) {
    (_focuses.first -= center).rotate(angle) += center;
    (_focuses.second -= center).rotate(angle) += center;
}

void Ellipse::scale(const Point &center, long double coefficient) {
    ((_focuses.first -= center) *= coefficient) += center;
    ((_focuses.second -= center) *= coefficient) += center;
}

class Circle : public Ellipse {
public:
    Circle(const Point& center, long double radius) : Ellipse(center, center, 2 * radius) {};
    Circle(const Point& center, const Point& p) : Circle(center, dist(center, p)) {};

    long double radius() const;
};

long double Circle::radius() const {
    return a;
}

class Rectangle : public Polygon {
public:
    Rectangle(const Point& a, const Point& c, long double k);

    Point center() const;
    std::pair< Line, Line > diagonals() const;
};

Rectangle::Rectangle(const Point &a, const Point& c, long double k) : Polygon({Point(), Point(), Point(), Point()}) {
    if (k < 1) k = 1 / k;
    Point diag = c - a;
    long double ab = diag.abs() / sqrtl(1 + k * k);
    diag.normalize() *= ab;
    diag.rotate(atanl(k) * 180 / M_PI);
    vertices = {a, a + diag, c, c - diag};
}

Point Rectangle::center() const {
    return (vertices[0] + vertices[2]) / 2;
}

std::pair<Line, Line> Rectangle::diagonals() const {
    return { Line(vertices[0], vertices[2]), Line(vertices[1], vertices[3]) };
}

class Square : public Rectangle {
public:
    Square(const Point& a, const Point& c) : Rectangle(a, c, 1) {};

    Circle circumscribedCircle() const;
    Circle inscribedCircle() const;
};

Circle Square::circumscribedCircle() const {
    return Circle(center(), vertices[0]);
}

Circle Square::inscribedCircle() const {
    return Circle(center(), Line(vertices[0], vertices[1]).distance(center()));
}

class Triangle : public Polygon {
public:

    Triangle(const Point& a, const Point& b, const Point& c) : Polygon({ a, b, c }) {};

    Circle circumscribedCircle() const;
    Circle inscribedCircle() const;
    Point centroid() const;
    Point orthocenter() const;
    Line EulerLine() const;
    Circle ninePointsCircle() const;
};

Circle Triangle::inscribedCircle() const {
    long double a, b, c;
    a = dist(vertices[1], vertices[2]);
    b = dist(vertices[0], vertices[2]);
    c = dist(vertices[0], vertices[1]);
    Point center = (vertices[0] * a + vertices[1] * b + vertices[2] * c) / (a + b + c);
    return Circle(center, Line(vertices[0], vertices[1]).distance(center));
}

Circle Triangle::circumscribedCircle() const {
    Point b2 = vertices[1] - vertices[0];
    Point c2 = vertices[2] - vertices[0];
    long double d = 2 * (b2.x * c2.y - b2.y * c2.x);
    Point center = (Point(c2.y * b2.sqAbs() - b2.y * c2.sqAbs(), b2.x * c2.sqAbs() - c2.x * b2.sqAbs()) / d) + vertices[0];
    return Circle(center, dist(center, vertices[0]));
}

Point Triangle::centroid() const {
    return (vertices[0] + vertices[1] + vertices[2]) / 3;
}

Point Triangle::orthocenter() const {
    return Point(intersection(Line(vertices[0], vertices[1]).perpendicular(vertices[2]), Line(vertices[0], vertices[2]).perpendicular(vertices[1])));
}

Line Triangle::EulerLine() const {
    return Line(circumscribedCircle().center(), orthocenter());
}

Circle Triangle::ninePointsCircle() const {
    return Circle((circumscribedCircle().center() + orthocenter()) / 2, circumscribedCircle().radius() / 2);
}

