#include <string>
#include <vector>
#include <iostream>

class BigInteger {

private:
    static const long long base = 1'000'000'000;

    std::vector< long long > digits;
    bool negative = false;

    size_t Size() const;

    static std::pair< BigInteger, BigInteger > divide(const BigInteger& dividend, const BigInteger& divisor);

public:

    BigInteger(long long n = 0);
    BigInteger(std::string s);

    void Swap(BigInteger &r);

    BigInteger operator-() const;
    friend bool operator<(const BigInteger& l, const BigInteger& r);

    BigInteger& operator+=(BigInteger r);
    BigInteger& operator-=(const BigInteger& r);

    BigInteger& operator*=(long long r);
    BigInteger& operator*=(const BigInteger& r);

    BigInteger& operator/=(const BigInteger& r);
    friend BigInteger operator/(const BigInteger& l, const BigInteger& r);

    BigInteger& operator%=(const BigInteger& r);
    friend BigInteger operator%(const BigInteger& l, const BigInteger& r);

    static BigInteger gcd(BigInteger l, BigInteger r);

    BigInteger& operator++();
    BigInteger& operator--();

    BigInteger operator++(int);
    BigInteger operator--(int);

    explicit operator bool() const;

    std::string toString() const;
    friend std::istream& operator>>(std::istream& in, BigInteger& r);

    bool isNegative() const;
};

BigInteger::BigInteger(long long n) {
    negative = (n < 0);
    if(negative) n *= -1;
    if(n == 0) digits.push_back(0);
    for(; n > 0; n /= base) digits.push_back(n % base);
}

bool operator<(const BigInteger& l, const BigInteger& r) {
    if(l.negative != r.negative) return l.negative;
    if(l.Size() != r.Size()) return l.negative ^ (l.Size() < r.Size());
    for(long long i = static_cast<long long>(l.Size()) - 1; i >= 0; --i) {
        if(l.digits[i] != r.digits[i]) return l.negative ^ (l.digits[i] < r.digits[i]);
    }
    return false;
}

bool operator>(const BigInteger& l, const BigInteger& r) {
    return (r < l);
}

bool operator<=(const BigInteger& l, const BigInteger& r) {
    return !(l > r);
}

bool operator>=(const BigInteger& l, const BigInteger& r) {
    return !(l < r);
}

bool operator==(const BigInteger& l, const BigInteger& r) {
    return (l >= r) && (l <= r);
}

bool operator!=(const BigInteger& l, const BigInteger& r) {
    return !(l == r);
}

BigInteger& BigInteger::operator+=(BigInteger r) {
    bool subtract = false;
    if(r.negative ^ negative) {
        if(negative) { // -a + b
            negative = false;
            if(r > (*this)) { // b - a
                Swap(r);
            } else {
                negative = true;
            }
        } else { // a + (-b)
            r.negative = false;
            if((*this) < r) { // -(b - a)
                Swap(r);
                negative = true;
            }
        }
        subtract = true;
    }
    long long carry = 0;
    size_t i = 0;
    for(i = 0; i < r.Size() && i < Size(); ++i) {
        long long newValue = digits[i] + (r.digits[i] * (subtract ? -1 : 1)) + carry;
        digits[i] = newValue % base;
        carry = newValue / base;
        if(digits[i] < 0) {
            digits[i] += base;
            carry -= 1;
        }
    }
    for(; i < Size() && carry != 0; ++i) {
        long long newValue = digits[i] + carry;
        digits[i] = newValue % base;
        carry = newValue / base;
        if (newValue < 0) {
            digits[i] += base;
            carry -= 1;
        }
    }
    for(; i < r.Size(); ++i) {
        long long newValue = r.digits[i] + carry;
        carry = newValue / base;
        digits.push_back(newValue % base);
    }
    if(carry != 0) digits.push_back(carry);
    while(Size() > 1 && digits.back() == 0) digits.pop_back();
    if(Size() == 1 && digits.back() == 0) negative = false;

    return *this;
}

BigInteger operator+(const BigInteger& l, const BigInteger& r) {
    BigInteger copy(l);
    copy += r;
    return copy;
}

BigInteger& BigInteger::operator-=(const BigInteger &r) {
    return (*this) += (-r);
}

BigInteger operator-(const BigInteger& l, const BigInteger& r) {
    BigInteger copy(l);
    copy -= r;
    return copy;
}

size_t BigInteger::Size() const {
    return digits.size();
}

BigInteger BigInteger::operator-() const {
    BigInteger result = *this;
    if(result != 0) result.negative ^= true;
    return result;
}

void BigInteger::Swap(BigInteger &r) {
    digits.swap(r.digits);
    std::swap(negative, r.negative);
}

BigInteger& BigInteger::operator*=(long long r) {
    if((*this) == 0) return (*this);
    if(r == 0) {
        negative = false;
        digits.clear();
        digits.push_back(0);
        return (*this);
    }
    if(r < 0) {
        negative ^= true;
        r *= -1;
    }
    long long carry = 0;
    for(size_t i = 0; i < Size(); ++i) {
        long long newValue = digits[i] * r + carry;
        digits[i] = newValue % base;
        carry = newValue / base;
    }
    while(carry != 0) {
        digits.push_back(carry % base);
        carry /= base;
    }
    return (*this);
}

BigInteger operator*(const BigInteger& l, long long r) {
    BigInteger copy(l);
    copy *= r;
    return copy;
}

BigInteger& BigInteger::operator*=(const BigInteger& r) {
    if(r.Size() == 0 || r == 0 || (*this) == 0) {
        return *this *= 0;
    }
    negative ^= r.negative;
    BigInteger copy(*this);

    (*this) *= r.digits[0];
    for(size_t i = 1; i < r.Size(); ++i) {
        copy.digits.insert(copy.digits.begin(), 0); // *= base
        (*this) += (copy * r.digits[i]);
    }
    return (*this);
}

BigInteger operator*(const BigInteger& l, const BigInteger& r) {
    BigInteger copy(l);
    copy *= r;
    return copy;
}

std::pair<BigInteger, BigInteger> BigInteger::divide(const BigInteger &dividend, const BigInteger &divisor) {
    BigInteger q, r;
    q.digits.resize(dividend.Size());

    for(long long i = static_cast<long long>(dividend.Size()) - 1; i >= 0; --i) {
        r.digits.insert(r.digits.begin(), dividend.digits[i]);
        long long s1 = r.Size() <= divisor.Size() ? 0 : r.digits[divisor.Size()];
        long long s2 = r.Size() < divisor.Size() ? 0 : r.digits[divisor.Size() - 1];
        long long d = (base * s1 + s2) / divisor.digits.back();
        r -= divisor * (divisor.negative ? -d : d);
        while(r < 0) {
            if(!divisor.negative)
                r += divisor;
            else r -= divisor;
            --d;
        }
        q.digits[i] = d;
    }

    q.negative = dividend.negative ^ divisor.negative;
    r.negative = dividend.negative;

    while(q.Size() > 1 && q.digits.back() == 0) q.digits.pop_back();
    while(r.Size() > 1 && r.digits.back() == 0) r.digits.pop_back();
    if(q.Size() == 1 && q.digits[0] == 0) q.negative = false;
    if(r.Size() == 1 && r.digits[0] == 0) r.negative = false;
    return {q, r};
}

BigInteger& BigInteger::operator/=(const BigInteger &r) {
    return (*this = divide(*this, r).first);
}

BigInteger operator/(const BigInteger& l, const BigInteger& r) {
    BigInteger result = BigInteger::divide(l, r).first;
    return result;
}

BigInteger& BigInteger::operator%=(const BigInteger &r) {
    return (*this = divide(*this, r).second);
}

BigInteger operator%(const BigInteger& l, const BigInteger& r) {
    BigInteger result = BigInteger::divide(l, r).second;
    return result;
}

BigInteger& BigInteger::operator++() {
    return (*this += 1);
}

BigInteger& BigInteger::operator--() {
    return (*this -= 1);
}

BigInteger BigInteger::operator++(int) {
    BigInteger copy = (*this);
    ++(*this);
    return copy;
}

BigInteger BigInteger::operator--(int) {
    BigInteger copy = (*this);
    --(*this);
    return copy;
}

BigInteger::operator bool() const {
    return (*this != 0);
}

std::string BigInteger::toString() const {
    std::string result = (negative ? "-" : "");
    size_t baseLength = std::to_string(base).size() - 1;

    result += std::to_string(digits.back());
    for(long long i = static_cast<long long>(Size()) - 2; i >= 0; --i) {
        std::string digit = std::to_string(digits[i]);
        result += std::string(baseLength - digit.size(), '0') + digit;
    }

    return result;
}

std::istream &operator>>(std::istream &in, BigInteger& r) {
    std::string input;
    in >> input;
    r = BigInteger(input);
    return in;
}

BigInteger::BigInteger(std::string s) {
    if (s.empty()) {
        (*this) = BigInteger();
        return;
    }
    negative = (s[0] == '-');
    if (s[0] == '-' || s[0] == '+') s.erase(0, 1);

    size_t baseLength = std::to_string(base).size() - 1;
    while (s.size() > baseLength) {
        digits.push_back(stoll(s.substr(s.size() - baseLength)));
        s.erase(s.size() - baseLength, s.size() - 1);
    }
    digits.push_back(stoll(s));
}

BigInteger BigInteger::gcd(BigInteger a, BigInteger b) {
    //if(a == 0 && b == 0) return BigInteger(1);
    a.negative = false;
    b.negative = false;
    if(a < b) std::swap(a, b);
    while(b) {
        a %= b;
        a.Swap(b);
    }
    return a;
}

bool BigInteger::isNegative() const {
    return negative;
}

std::ostream& operator<<(std::ostream& out, const BigInteger& r) {
    return (out << r.toString());
}

class Rational {
private:

    BigInteger numerator, denominator;

    void Fix();

public:

    Rational(const BigInteger& numerator = 0, const BigInteger& denominator = 1);
    Rational(long long n) : Rational(BigInteger(n)) {};

    Rational operator-() const;

    Rational& operator+=(const Rational& r);
    Rational& operator-=(const Rational& r);
    Rational& operator*=(const Rational& r);
    Rational& operator/=(const Rational& r);

    friend bool operator<(const Rational& l, const Rational& r);

    std::string toString() const;

    std::string asDecimal(size_t precision = 0) const;

    explicit operator double() const;
};

Rational::Rational(const BigInteger& numerator, const BigInteger& denominator) : numerator(numerator), denominator(denominator) {
    Fix();
}

void Rational::Fix() {
    if(denominator.isNegative()) {
        denominator = -denominator;
        numerator = -numerator;
    }
    BigInteger gcd = BigInteger::gcd(numerator, denominator);
    numerator /= gcd;
    denominator /= gcd;
}

Rational& Rational::operator+=(const Rational& r) {
    numerator *= r.denominator;
    numerator += r.numerator * denominator;
    denominator *= r.denominator;
    Fix();
    return (*this);
}

Rational operator+(const Rational& l, const Rational& r) {
    Rational result = l;
    result += r;
    return result;
}
Rational Rational::operator-() const {
    return Rational(-numerator, denominator);
}

Rational& Rational::operator-=(const Rational &r) {
    return (*this) += (-r);
}
Rational operator-(const Rational& l, const Rational& r) {
    Rational result = l;
    result -= r;
    return result;
}

Rational& Rational::operator*=(const Rational &r) {
    numerator *= r.numerator;
    denominator *= r.denominator;
    Fix();
    return (*this);
}

Rational operator*(const Rational& l, const Rational& r) {
    Rational result = l;
    result *= r;
    return result;
}

Rational& Rational::operator/=(const Rational &r) {
    numerator *= r.denominator;
    denominator *= r.numerator;
    Fix();
    return (*this);
}
Rational operator/(const Rational& l, const Rational& r) {
    Rational result = l;
    result /= r;
    return result;
}

bool operator<(const Rational &l, const Rational &r) {
    return (l.numerator * r.denominator < r.numerator * l.denominator);
}

bool operator>(const Rational& l, const Rational& r) {
    return r < l;
}

bool operator>=(const Rational& l, const Rational& r) {
    return !(l < r);
}

bool operator<=(const Rational& l, const Rational& r) {
    return !(l > r);
}

bool operator==(const Rational& l, const Rational& r) {
    return (l <= r) && (r <= l);
}

bool operator!=(const Rational& l, const Rational& r) {
    return !(l == r);
}

std::string Rational::toString() const {
    return (numerator.toString()) + (denominator == 1 ? "" : "/" + denominator.toString());
}

std::string Rational::asDecimal(size_t precision) const {
    BigInteger numeratorCopy = numerator;
    BigInteger intPart = numeratorCopy / denominator;
    std::string result = ((intPart == 0 && numeratorCopy.isNegative()) ? "-" : "") + intPart.toString();
    if(numeratorCopy.isNegative()) numeratorCopy *= -1;
    numeratorCopy %= denominator;

    if(precision > 0) {
        result += ".";
        for(size_t i = 0; i < precision; ++i) {
            numeratorCopy *= 10;
            result += (numeratorCopy / denominator).toString();
            numeratorCopy %= denominator;
        }
    }
    return result;
}

Rational::operator double() const {
    return stod(asDecimal(32));
}

