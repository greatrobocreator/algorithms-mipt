#pragma once

#include <string>
#include <vector>
#include <iostream>

class BigInteger {

private:
    static const long long base = 1'000'000'000;

    std::vector< long long > digits;
    bool negative = false;

    size_t Size() const;

    static std::pair< BigInteger, BigInteger > divide(const BigInteger& dividend, const BigInteger& divisor);

public:

    BigInteger(long long n = 0);
    BigInteger(std::string s);

    void Swap(BigInteger &r);

    BigInteger operator-() const;
    friend bool operator<(const BigInteger& l, const BigInteger& r);

    BigInteger& operator+=(BigInteger r);
    BigInteger& operator-=(const BigInteger& r);

    BigInteger& operator*=(long long r);
    BigInteger& operator*=(const BigInteger& r);

    BigInteger& shiftRight();
    BigInteger& operator/=(const BigInteger& r);
    friend BigInteger operator/(const BigInteger& l, const BigInteger& r);

    BigInteger& operator%=(const BigInteger& r);
    friend BigInteger operator%(const BigInteger& l, const BigInteger& r);

    static BigInteger gcd(BigInteger l, BigInteger r);

    BigInteger& operator++();
    BigInteger& operator--();

    BigInteger operator++(int);
    BigInteger operator--(int);

    explicit operator bool() const;

    std::string toString() const;
    friend std::istream& operator>>(std::istream& in, BigInteger& r);

    bool isNegative() const;
    void removeLeadingZeros();
};

BigInteger::BigInteger(long long n) {
    negative = (n < 0);
    if(negative) n *= -1;
    if(n == 0) digits.push_back(0);
    for(; n > 0; n /= base) digits.push_back(n % base);
}

bool operator<(const BigInteger& l, const BigInteger& r) {
    if(l.negative != r.negative) return l.negative;
    if(l.Size() != r.Size()) return l.negative ^ (l.Size() < r.Size());
    for(long long i = static_cast<long long>(l.Size()) - 1; i >= 0; --i) {
        if(l.digits[i] != r.digits[i]) return l.negative ^ (l.digits[i] < r.digits[i]);
    }
    return false;
}

bool operator>(const BigInteger& l, const BigInteger& r) {
    return (r < l);
}

bool operator<=(const BigInteger& l, const BigInteger& r) {
    return !(l > r);
}

bool operator>=(const BigInteger& l, const BigInteger& r) {
    return !(l < r);
}

bool operator==(const BigInteger& l, const BigInteger& r) {
    return (l >= r) && (l <= r);
}

bool operator!=(const BigInteger& l, const BigInteger& r) {
    return !(l == r);
}

BigInteger& BigInteger::operator+=(BigInteger r) {
    bool subtract = false;
    if(r.negative ^ negative) {
        if(negative) { // -a + b
            negative = false;
            if(r > (*this)) { // b - a
                Swap(r);
            } else {
                negative = true;
            }
        } else { // a + (-b)
            r.negative = false;
            if((*this) < r) { // -(b - a)
                Swap(r);
                negative = true;
            }
        }
        subtract = true;
    }
    long long carry = 0;
    size_t i;
    for (i = 0; i < std::max(digits.size(), r.digits.size()); ++i) {
        long long digit = carry;
        if (i < digits.size()) digit += digits.at(i);
        if (i < r.digits.size()) digit += r.digits.at(i) * (subtract ? -1 : 1);
        if (digit >= base) {
            digit -= base;
            carry = 1;
        } else if (digit < 0) {
            digit += base;
            carry = -1;
        } else carry = 0;
        if(i < digits.size()) digits[i] = digit;
        else digits.push_back(digit);
    }

    if (carry != 0) digits.push_back(1);
    else removeLeadingZeros();

    return *this;
}

BigInteger operator+(const BigInteger& l, const BigInteger& r) {
    BigInteger copy(l);
    copy += r;
    return copy;
}

BigInteger& BigInteger::operator-=(const BigInteger &r) {
    return (*this) += (-r);
}

BigInteger operator-(const BigInteger& l, const BigInteger& r) {
    BigInteger copy(l);
    copy -= r;
    return copy;
}

size_t BigInteger::Size() const {
    return digits.size();
}

BigInteger BigInteger::operator-() const {
    BigInteger result = *this;
    if(result != 0) result.negative ^= true;
    return result;
}

void BigInteger::Swap(BigInteger &r) {
    digits.swap(r.digits);
    std::swap(negative, r.negative);
}

BigInteger& BigInteger::operator*=(long long r) {
    if((*this) == 0) return (*this);
    if(r == 0) {
        negative = false;
        digits.clear();
        digits.push_back(0);
        return (*this);
    }
    if(r < 0) {
        negative ^= true;
        r *= -1;
    }
    long long carry = 0;
    for(size_t i = 0; i < Size(); ++i) {
        long long newValue = digits[i] * r + carry;
        digits[i] = newValue % base;
        carry = newValue / base;
    }
    while(carry != 0) {
        digits.push_back(carry % base);
        carry /= base;
    }
    return (*this);
}

BigInteger operator*(const BigInteger& l, long long r) {
    BigInteger copy(l);
    copy *= r;
    return copy;
}

BigInteger& BigInteger::operator*=(const BigInteger& r) {
    if(r.Size() == 0 || r == 0 || (*this) == 0) {
        return *this *= 0;
    }
    negative ^= r.negative;
    BigInteger copy(*this);

    (*this) *= r.digits[0];
    for(size_t i = 1; i < r.Size(); ++i) {
        copy.digits.insert(copy.digits.begin(), 0); // *= base
        (*this) += (copy * r.digits[i]);
    }
    return (*this);
}

BigInteger operator*(const BigInteger& l, const BigInteger& r) {
    BigInteger copy(l);
    copy *= r;
    return copy;
}

BigInteger& BigInteger::shiftRight() {
    if(Size() <= 0) return (*this);
    digits.insert(digits.begin(), 0);
    return (*this);
}

std::pair<BigInteger, BigInteger> BigInteger::divide(const BigInteger &dividend, const BigInteger &divisor) {
    BigInteger q, r;
    q.digits.resize(dividend.digits.size(), 0);
    q.negative = dividend.negative ^ divisor.negative;
    BigInteger divisorCopy = divisor;
    divisorCopy.negative = false;

    for (int i = dividend.digits.size() - 1; i >= 0; --i) {
        if(r == 0) r.digits.clear();
        r.digits.insert(r.digits.begin(), dividend.digits.at(i));
        long long s1 = r.digits.size() <= divisorCopy.digits.size() ? 0 : r.digits.at(divisorCopy.digits.size());
        long long s2 = r.digits.size() <= divisorCopy.digits.size() - 1 ? 0 : r.digits.at(divisorCopy.digits.size() - 1);
        long long left = (base * s1 + s2) / (divisorCopy.digits.back() + 1);
        long long right = (base * s1 + s2 + 1) / divisorCopy.digits.back();
        long long d = 0;
        while (left <= right) {
            long long m = (left + right) / 2;

            if (r < divisorCopy * m)
                right = m - 1;
            else {
                left = m + 1;
                d = m;
            }
        }
        q.digits.at(i) = d;
        r -= divisorCopy * d;
    }

    q.removeLeadingZeros();
    r.removeLeadingZeros();
    r.negative = dividend.negative;
    return {q, r};
}

BigInteger& BigInteger::operator/=(const BigInteger &r) {
    return (*this = divide(*this, r).first);
}

BigInteger operator/(const BigInteger& l, const BigInteger& r) {
    BigInteger result = BigInteger::divide(l, r).first;
    return result;
}

BigInteger& BigInteger::operator%=(const BigInteger &r) {
    return (*this = divide(*this, r).second);
}

BigInteger operator%(const BigInteger& l, const BigInteger& r) {
    BigInteger result = BigInteger::divide(l, r).second;
    return result;
}

BigInteger& BigInteger::operator++() {
    return (*this += 1);
}

BigInteger& BigInteger::operator--() {
    return (*this -= 1);
}

BigInteger BigInteger::operator++(int) {
    BigInteger copy = (*this);
    ++(*this);
    return copy;
}

BigInteger BigInteger::operator--(int) {
    BigInteger copy = (*this);
    --(*this);
    return copy;
}

BigInteger::operator bool() const {
    return (*this != 0);
}

std::string BigInteger::toString() const {
    std::string result = (negative ? "-" : "");
    size_t baseLength = std::to_string(base).size() - 1;

    result += std::to_string(digits.back());
    for(long long i = static_cast<long long>(Size()) - 2; i >= 0; --i) {
        std::string digit = std::to_string(digits[i]);
        result += std::string(baseLength - digit.size(), '0') + digit;
    }

    return result;
}

std::istream &operator>>(std::istream &in, BigInteger& r) {
    std::string input;
    in >> input;
    r = BigInteger(input);
    return in;
}

BigInteger::BigInteger(std::string s) {
    if (s.empty()) {
        (*this) = BigInteger();
        return;
    }
    negative = (s[0] == '-');
    if (s[0] == '-' || s[0] == '+') s.erase(0, 1);

    size_t baseLength = std::to_string(base).size() - 1;
    while (s.size() > baseLength) {
        digits.push_back(stoll(s.substr(s.size() - baseLength)));
        s.erase(s.size() - baseLength, s.size() - 1);
    }
    digits.push_back(stoll(s));
}

BigInteger BigInteger::gcd(BigInteger a, BigInteger b) {
    a.negative = false;
    b.negative = false;
    if(a < b) std::swap(a, b);
    while(b) {
        a %= b;
        a.Swap(b);
    }
    return a;
}

bool BigInteger::isNegative() const {
    return negative;
}

void BigInteger::removeLeadingZeros() {
    while(Size() > 1 && digits.back() == 0) digits.pop_back();
    if(Size() == 1 && digits[0] == 0) negative = false;
}

std::ostream& operator<<(std::ostream& out, const BigInteger& r) {
    return (out << r.toString());
}

class Rational {
private:

    BigInteger numerator, denominator;

    void Fix();

public:

    Rational(const BigInteger& numerator = 0, const BigInteger& denominator = 1);
    Rational(long long n) : Rational(BigInteger(n)) {};

    Rational operator-() const;

    Rational& operator+=(const Rational& r);
    Rational& operator-=(const Rational& r);
    Rational& operator*=(const Rational& r);
    Rational& operator/=(const Rational& r);

    friend bool operator<(const Rational& l, const Rational& r);

    std::string toString() const;

    std::string asDecimal(size_t precision = 0) const;

    explicit operator double() const;
};

Rational::Rational(const BigInteger& numerator, const BigInteger& denominator) : numerator(numerator), denominator(denominator) {
    Fix();
}

void Rational::Fix() {
    if(denominator.isNegative()) {
        denominator = -denominator;
        numerator = -numerator;
    }
    BigInteger gcd = BigInteger::gcd(numerator, denominator);
    numerator /= gcd;
    denominator /= gcd;
}

Rational& Rational::operator+=(const Rational& r) {
    numerator *= r.denominator;
    numerator += r.numerator * denominator;
    denominator *= r.denominator;
    Fix();
    return (*this);
}

Rational operator+(const Rational& l, const Rational& r) {
    Rational result = l;
    result += r;
    return result;
}
Rational Rational::operator-() const {
    return Rational(-numerator, denominator);
}

Rational& Rational::operator-=(const Rational &r) {
    return (*this) += (-r);
}
Rational operator-(const Rational& l, const Rational& r) {
    Rational result = l;
    result -= r;
    return result;
}

Rational& Rational::operator*=(const Rational &r) {
    numerator *= r.numerator;
    denominator *= r.denominator;
    Fix();
    return (*this);
}

Rational operator*(const Rational& l, const Rational& r) {
    Rational result = l;
    result *= r;
    return result;
}

Rational& Rational::operator/=(const Rational &r) {
    numerator *= r.denominator;
    denominator *= r.numerator;
    Fix();
    return (*this);
}
Rational operator/(const Rational& l, const Rational& r) {
    Rational result = l;
    result /= r;
    return result;
}

bool operator<(const Rational &l, const Rational &r) {
    return (l.numerator * r.denominator < r.numerator * l.denominator);
}

bool operator>(const Rational& l, const Rational& r) {
    return r < l;
}

bool operator>=(const Rational& l, const Rational& r) {
    return !(l < r);
}

bool operator<=(const Rational& l, const Rational& r) {
    return !(l > r);
}

bool operator==(const Rational& l, const Rational& r) {
    return (l <= r) && (r <= l);
}

bool operator!=(const Rational& l, const Rational& r) {
    return !(l == r);
}

std::string Rational::toString() const {
    return (numerator.toString()) + (denominator == 1 ? "" : "/" + denominator.toString());
}

std::string Rational::asDecimal(size_t precision) const {
    BigInteger numeratorCopy = numerator;
    BigInteger intPart = numeratorCopy / denominator;
    std::string result = ((intPart == 0 && numeratorCopy.isNegative()) ? "-" : "") + intPart.toString();
    if(numeratorCopy.isNegative()) numeratorCopy *= -1;
    numeratorCopy %= denominator;

    if(precision > 0) {
        result += ".";
        for(size_t i = 0; i < precision; ++i) {
            numeratorCopy *= 10;
            result += (numeratorCopy / denominator).toString();
            numeratorCopy %= denominator;
        }
    }
    return result;
}

Rational::operator double() const {
    return stod(asDecimal(32));
}

std::istream &operator>>(std::istream &in, Rational &r) {
    long long n;
    in >> n;
    r = Rational(n);
    return in;
}

#include <type_traits>
#include <array>

constexpr bool IsPrime(unsigned long long n) {
    for (unsigned long long i = 2; i * i <= n; ++i)
        if (n % i == 0)
            return false;
    return true;
}

template<unsigned long long N>
struct Inverse {
    std::array<unsigned long long, N> inv;

    constexpr Inverse() : inv() {
        inv[1] = 1;
        for (unsigned long long i = 2; i < N; ++i)
            inv[i] = N - (N / i) * inv[N % i] % N;
    }
};

template<unsigned long long N>
class Finite {
private:

    unsigned long long n;
public:

    Finite(long long n = 0) : n((n % static_cast<long long>(N) + static_cast<long long>(N)) % N) {};

    Finite& operator+=(const Finite& r);

    Finite& operator-=(const Finite& r);

    Finite& operator*=(const Finite& r);

    Finite& operator++();
    Finite operator++(int);
    Finite& operator--();
    Finite operator--(int);

    template<unsigned long long M>
    friend bool operator==(const Finite<M>& l, const Finite<M>& r);

    friend int main();

    template<unsigned long long M = N>
    typename std::enable_if<IsPrime(M), Finite& >::type
    operator/=(const Finite &r);
};

template<unsigned long long N>
Finite<N> &Finite<N>::operator+=(const Finite<N> &r) {
    n = (n + r.n) % N;
    return (*this);
}

template<unsigned long long N>
Finite<N> &Finite<N>::operator-=(const Finite<N> &r) {
    n = (n - r.n + N) % N;
    return (*this);
}

template<unsigned long long N>
Finite<N> &Finite<N>::operator*=(const Finite<N> &r) {
    n = (n * r.n) % N;
    return (*this);
}

unsigned long long pow(unsigned long long base, unsigned long long exp, unsigned long long mod) {
    base %= mod;
    unsigned long long result = 1;
    while( exp > 0) {
        if(exp & 1) result = (result * base) % mod;
        base = (base * base) % mod;
        exp >>= 1;
    }
    return result;
}

template<unsigned long long N>
template<unsigned long long M>
typename std::enable_if<IsPrime(M), Finite<N>& >::type
Finite<N>::operator/=(const Finite<N> &r) {
    //n = n * inv[r.n] % N;
    n = (n * pow(r.n, N - 2, N)) % N;
    return (*this);
}

template<unsigned long long N>
Finite<N> operator+(const Finite<N>& l, const Finite<N>& r) {
    Finite<N> result(l);
    result += r;
    return result;
}

template<unsigned long long N>
Finite<N> operator-(const Finite<N>& l, const Finite<N>& r) {
    Finite<N> result(l);
    result -= r;
    return result;
}

template<unsigned long long N>
Finite<N> operator*(const Finite<N>& l, const Finite<N>& r) {
    Finite<N> result(l);
    result *= r;
    return result;
}

template<unsigned long long N, typename = std::enable_if_t<IsPrime(N)> >
Finite<N> operator/(const Finite<N>& l, const Finite<N> & r) {
    Finite<N> result(l);
    result /= r;
    return result;
}

template <unsigned long long N>
bool operator==(const Finite<N>& l, const Finite<N>& r) {
    return l.n == r.n;
}

template<unsigned long long int N>
Finite<N>& Finite<N>::operator++() {
    return (*this += 1);
}

template<unsigned long long int N>
Finite<N> Finite<N>::operator++(int) {
    Finite<N> copy(*this);
    *this += 1;
    return copy;
}

template<unsigned long long int N>
Finite<N>& Finite<N>::operator--() {
    return (*this -= 1);
}

template<unsigned long long int N>
Finite<N> Finite<N>::operator--(int) {
    Finite<N> copy(*this);
    *this -= 1;
    return copy;
}

template <unsigned long long N>
bool operator!=(const Finite<N>& l, const Finite<N>& r) {
    return !(l == r);
}

template<unsigned long long M, unsigned long long N, typename Field = Rational>
class Matrix {
public:
    std::array< std::array< Field, N>, M> a;

    // Converts to triangular matrix
    // Returns determinant multiplier
    long long GaussianElimination();

    template<unsigned long long T = N, unsigned long long U = M>
    std::enable_if_t<T == 2 * U> GaussianJordanElimination();

public:

    Matrix(std::vector< std::vector< Field > >& _a);
    Matrix(std::vector< std::vector< int > >& _a);

    Matrix();

    const std::array<Field, N>& operator[](unsigned long long i) const;
    std::array<Field, N>& operator[](unsigned long long i);

    Matrix& operator+=(const Matrix& r);
    Matrix& operator-=(const Matrix& r);
    Matrix& operator*=(const Field& r);

    template<unsigned long long K>
    Matrix<M, K>& operator*=(const Matrix<N, K>& r);

    template<unsigned long long T = N, unsigned long long U = M>
    std::enable_if_t<T == U, Field> det() const;

    Matrix<N, M, Field> transposed() const;
    unsigned long long rank() const;
    Field trace() const;

    template<unsigned long long T = N, unsigned long long U = M>
    std::enable_if_t<T == U> invert();

    template<unsigned long long T = N, unsigned long long U = M>
    std::enable_if_t<T == U, Matrix> inverted() const;

    std::vector< Field > getRow(unsigned long long i) const;
    std::vector< Field > getColumn(unsigned long long i) const;
};

template<unsigned long long N, typename Field = Rational>
using SquareMatrix = Matrix<N, N, Field>;

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field>::Matrix(std::vector<std::vector<Field>>& _a) {
    for(unsigned long long i = 0; i < M; ++i)
        for(unsigned long long j = 0; j < N; ++j)
            a[i][j] = _a[i][j];
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field>::Matrix(std::vector<std::vector<int>>& _a) {
    for(unsigned long long i = 0; i < M; ++i)
        for(unsigned long long j = 0; j < N; ++j)
            a[i][j] = Field(_a[i][j]);
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field>::Matrix() {
    //a.resize(M, std::vector< Field >(N));
    for (unsigned long long i = 0; i < M; ++i)
        for (unsigned long long j = 0; j < N; ++j)
            a[i][j] = Field((N == M) && (i == j));
}

template<unsigned long long int M, unsigned long long int N, typename Field>
bool operator==(const Matrix<M, N, Field>& l, const Matrix<M, N, Field>& r) {
    for (unsigned long long i = 0; i < M; ++i)
        for (unsigned long long j = 0; j < N; ++j)
            if(l[i][j] != r[i][j]) return false;
    return true;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
bool operator!=(const Matrix<M, N, Field>& l, const Matrix<M, N, Field>& r) {
    return !(l == r);
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field>& Matrix<M, N, Field>::operator+=(const Matrix<M, N, Field> &r) {
    for(unsigned long long i = 0; i < M; ++i)
        for(unsigned long long j = 0; j < N; ++j)
            a[i][j] += r[i][j];
    return (*this);
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field>& Matrix<M, N, Field>::operator-=(const Matrix<M, N, Field> &r) {
    for(unsigned long long i = 0; i < M; ++i)
        for(unsigned long long j = 0; j < N; ++j)
            a[i][j] -= r[i][j];
    return (*this);
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field>& Matrix<M, N, Field>::operator*=(const Field& r) {
    for(unsigned long long i = 0; i < M; ++i)
        for(unsigned long long j = 0; j < N; ++j)
            a[i][j] *= r;
    return (*this);
}

template<unsigned long long int M, unsigned long long int N, typename Field>
const std::array<Field, N>& Matrix<M, N, Field>::operator[](unsigned long long int i) const {
    return a[i];
}

template<unsigned long long int M, unsigned long long int N, typename Field>
std::array<Field, N>& Matrix<M, N, Field>::operator[](unsigned long long int i) {
    return a[i];
}

template<unsigned long long int M, unsigned long long int N, typename Field>
template<unsigned long long int T, unsigned long long int U>
std::enable_if_t<T == U, Field> Matrix<M, N, Field>::det() const {
    Matrix<M, N, Field> copy(*this);
    Field result = copy.GaussianElimination();
    for(long long i = 0; i < static_cast<long long>(N); ++i) result *= copy[i][i];
    return result;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
long long Matrix<M, N, Field>::GaussianElimination() {
    long long multiplier = 1;
    for (long long i = 0; i < static_cast<long long>(M) && i < static_cast<long long>(N); ++i) {
        long long j;
        for (j = i; j < static_cast<long long>(M) && a[j][i] == Field(0); ++j);
        if (j >= static_cast<long long>(M)) continue;
        if (j != i) {
            std::swap(a[i], a[j]);
            multiplier *= -1;
        }
        for (j = i + 1; j < static_cast<long long>(M); ++j) {
            if (a[j][i] != Field(0)) {
                Field m = a[j][i] / a[i][i];
                for (long long k = i; k < static_cast<long long>(N); ++k) {
                    a[j][k] -= a[i][k] * m;
                }
            }
        }

    }
    return multiplier;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<N, M, Field> Matrix<M, N, Field>::transposed() const {
    std::vector< std::vector< Field > > resultA;
    resultA.resize(N, std::vector< Field >(M));
    for(unsigned long long i = 0; i < M; ++i)
        for(unsigned long long j = 0; j < static_cast<long long>(N); ++j)
            resultA[j][i] = a[i][j];
    return Matrix<N, M, Field>(resultA);
}

template<unsigned long long int M, unsigned long long int N, typename Field>
unsigned long long Matrix<M, N, Field>::rank() const {
    Matrix<M, N, Field> copy(*this);
    copy.GaussianElimination();
    unsigned long long rank = 0;
    unsigned long long i = 0;
    for(rank = 0; rank < static_cast<long long>(M) && i < static_cast<long long>(N); ++i) {
        for(; i < static_cast<long long>(N) && copy[rank][i] == Field(0); ) {
            ++i;
        }
        if(i < static_cast<long long>(N)) rank++;
    }
    return rank;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Field Matrix<M, N, Field>::trace() const {
    Field sum(0);
    for(unsigned long long i = 0; i < M && i < static_cast<long long>(N); ++i) sum += a[i][i];
    return sum;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
template<unsigned long long T, unsigned long long U>
std::enable_if_t<T == 2 * U> Matrix<M, N, Field>::GaussianJordanElimination() {
    GaussianElimination();
    for(long long i = M - 1; i >= 0; --i) {
        Field m = a[i][i];
        if(m == Field(0)) throw std::invalid_argument("Invalid matrix! (rank < N)");
        m = Field(1) / m;
        for(long long j = i; j < static_cast<long long>(N); ++j) a[i][j] *= m;
    }
    for(long long i = M - 1; i >= 0; --i) {
        for(long long j = i - 1; j >= 0; --j) {
            Field m = a[j][i];
            for(long long k = i; k < static_cast<long long>(N); ++k) a[j][k] -= a[i][k] * m;
        }
    }
}

template<unsigned long long int M, unsigned long long int N, typename Field>
template<unsigned long long int T, unsigned long long int U>
std::enable_if_t<T == U> Matrix<M, N, Field>::invert() {
    std::vector< std::vector< Field > > b(M, std::vector< Field >(M));
    for(long long i = 0; i < static_cast<long long>(M); ++i)
        for(long long j = 0; j < static_cast<long long>(M); ++j)
            b[i][j] = a[i][j];
    for(long long i = 0; i < static_cast<long long>(M); ++i)
        for(long long j = 0; j < static_cast<long long>(M); ++j)
            b[j].push_back(j == i);
    Matrix<M, 2 * M, Field> copy(b);
    copy.GaussianJordanElimination();
    for(long long i = 0; i < static_cast<long long>(M); ++i)
        for(long long j = 0; j < static_cast<long long>(M); ++j)
            a[i][j] = copy[i][j + M];
}

template<unsigned long long int M, unsigned long long int N, typename Field>
template<unsigned long long int T, unsigned long long int U>
std::enable_if_t<T == U, Matrix<M, N, Field> > Matrix<M, N, Field>::inverted() const {
    Matrix copy(*this);
    copy.invert();
    return copy;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
std::vector<Field> Matrix<M, N, Field>::getRow(unsigned long long int i) const {
    return std::vector< Field >((*this)[i].begin(), (*this)[i].end());
}

template<unsigned long long int M, unsigned long long int N, typename Field>
std::vector<Field> Matrix<M, N, Field>::getColumn(unsigned long long int i) const {
    std::vector< Field > result(M);
    for(long long j = 0; j < static_cast<long long>(M); ++j)
        result[j] = a[j][i];
    return result;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
template<unsigned long long int K>
Matrix<M, K>& Matrix<M, N, Field>::operator*=(const Matrix<N, K>& r) {
    (*this) = (*this) * r;
    return *this;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field> operator+(const Matrix<M, N, Field> &l, const Matrix<M, N, Field> &r) {
    Matrix<M, N, Field> result(l);
    result += r;
    return result;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field> operator-(const Matrix<M, N, Field> &l, const Matrix<M, N, Field> &r) {
    Matrix<M, N, Field> result(l);
    result -= r;
    return result;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field> operator*(const Matrix<M, N, Field> &l, const Field& r) {
    Matrix<M, N, Field> result(l);
    result *= r;
    return result;
}

template<unsigned long long int M, unsigned long long int N, typename Field>
Matrix<M, N, Field> operator*(const Field& l, const Matrix<M, N, Field> &r) {
    return r * l;
}

template<unsigned long long M, unsigned long long N, unsigned long long K, typename Field>
Matrix<M, K, Field> operator*(const Matrix<M, N, Field>& l, const Matrix<N, K, Field>& r) {
    Matrix<M, K, Field> result;
    for(unsigned long long i = 0; i < M; ++i)
        for(unsigned long long j = 0; j < K; ++j) {
            result[i][j] = 0;
            for (unsigned long long k = 0; k < static_cast<long long>(N); ++k)
                result[i][j] += l[i][k] * r[k][j];
        }
    return result;
}