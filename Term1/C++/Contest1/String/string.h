#pragma once

#include <iostream>
#include <cstring>

class String {
private:
    size_t size = 0;
    size_t capacity = 0;
    char* str = nullptr;

public:

    String() = default;
    String(const char* _str, size_t count);
    String(const char* _str) : String(_str, strlen(_str)) {};
    String(size_t n, const char c);
    String(const char c) : String(1, c) {};
    String(const String& s);
    ~String();

    String& operator= (String s);
    void swap(String& s);

    size_t length() const;

    friend bool operator== (const String& l, const String& r);
    const char& operator[] (size_t index) const;
    char& operator[] (size_t index);

    void push_back(const char& c);
    void pop_back();

    const char& front() const;
    char& front();
    const char& back() const;
    char& back();

    String& operator+= (const String& s);

    size_t find(const String& substring) const;
    size_t rfind(const String& substring) const;

    String substr(size_t start, size_t count) const;

    bool empty() const;
    void clear();
};

String::String(const char* _str, size_t count) : size(count), capacity(size * 2), str(new char[capacity]){
    std::copy(_str, _str + count, str);
}

String::String(size_t n, const char c) : size(n), capacity(size * 2), str(new char[size]) {
    memset(str, c, n);
}

String::String(const String& s) : size(s.size), capacity(s.capacity), str(new char[capacity]) {
    std::copy(s.str, s.str + size, str);
}

String::~String() {
    delete[] str;
}

void String::swap(String& s) {
    std::swap(size, s.size);
    std::swap(capacity, s.capacity);
    std::swap(str, s.str);
}

String& String::operator=(String s) {
    swap(s);
    return *this;
}

size_t String::length() const {
    return size;
}

bool operator== (const String& l, const String& r) {
    if(l.length() != r.length()) return false;
    return strncmp(l.str, r.str, l.length()) == 0;
}

const char& String::operator[](size_t index) const {
    return str[index];
}

char& String::operator[](size_t index) {
    return str[index];
}

void String::push_back(const char& c) {
    if(capacity <= 0) capacity = 1;
    if(size >= capacity - 1) {
        capacity *= 2;
        char* newStr = new char[capacity];
        std::copy(str, str + size, newStr);
        delete[] str;
        str = newStr;
    }
    str[size] = c;
    size++;
}

void String::pop_back() {
    if(size <= 0) return;

    size--;
    if(4 * size <= capacity) {
        capacity /= 2;
        char* newStr = new char[capacity];
        std::copy(str, str + size, newStr);
        delete[] str;
        str = newStr;
    }
}

const char& String::front() const {
    return str[0];
}

char& String::front() {
    return str[0];
}

const char& String::back() const {
    return str[size - 1];
}

char& String::back() {
    return str[size - 1];
}

String& String::operator+=(const String &s) {
    if(capacity <= 0) capacity = 1;
    size += s.size;
    if(size >= capacity - 1) {
        capacity = size * 2;
        char* newStr = new char[capacity];
        std::copy(str, str + size - s.size, newStr);
        delete[] str;
        str = newStr;
    }
    std::copy(s.str, s.str + s.size, str + (size - s.size));
    return *this;
}

String operator+ (const String& l, const String& r) {
    String result = l;
    result += r;
    return result;
}

size_t String::find(const String& substring) const {
    if(substring.size > size) return length();
    for(size_t i = 0; i < size - substring.size + 1; ++i) {
        if(strncmp(str + i, substring.str, substring.size) == 0) return i;
    }
    return length();
}

size_t String::rfind(const String& substring) const {
    if(substring.size > size) return length();
    for(size_t i = size - substring.size + 1; i > 0; --i) {
        if(strncmp(str + i - 1, substring.str, substring.size) == 0) return i - 1;
    }
    return length();
}

String String::substr(size_t start, size_t count) const {
    return String(str + start, count);
}

bool String::empty() const {
    return (length() <= 0);
}

void String::clear() {
    delete [] str;
    str = new char[0];
    size = 0;
    capacity = 0;
}

std::ostream& operator<< (std::ostream& out, const String& s) {
    for(size_t i = 0; i < s.length(); ++i) out << s[i];
    return out;
}

std::istream& operator>> (std::istream& in, String& s) {
    while (isspace(in.peek())) in.get();
    for(char c; !isspace(in.peek()) && in >> c;) {
        s.push_back(c);
    }
    return in;
}



