#include <bits/stdc++.h>

#pragma GCC optimize("Ofast")
//#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")

int main() {


    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long blockSize = 600;
    long long n, m;
    std::cin >> n >> m;
    std::vector< long long > numbers;
    numbers.reserve(n);
    std::vector< std::vector< long long > > blocks(n / blockSize + 1);
    for(long long i = 0; i < n; ++i) {
        long long x;
        std::cin >> x;
        numbers[i] = x;
        blocks[i / blockSize].push_back(x);
    }

    for(auto & block : blocks) std::sort(block.begin(), block.end());

    std::string cmd;
    for(long long i = 0; i < m; ++i) {
        std::cin >> cmd;
        if(cmd == "SET") {
            long long a, b;
            std::cin >> a >> b; a--;
            long long block = a / blockSize;
            auto it = std::lower_bound(blocks[block].begin(), blocks[block].end(), numbers[a]);
            *it = b;
            for(; std::next(it) != blocks[block].end() && *std::next(it) < *it; std::iter_swap(it, std::next(it)), ++it);
            for(; it != blocks[block].begin() && *std::prev(it) > *it; std::iter_swap(it, std::prev(it)), --it);
            /*blocks[block].erase(it);
            it = std::lower_bound(blocks[block].begin(), blocks[block].end(), b);
            blocks[block].insert(it, b);*/
            numbers[a] = b;
        } else if(cmd == "GET") {
            long long x, y, k, l;
            std::cin >> x >> y >> k >> l; x--; y--;

            long long count = 0;
            for(long long j = x; j <= y; ++j) {
                if(j % blockSize == 0 && j + blockSize - 1 <= y) {
                    long long block = j / blockSize;
                    auto kIt = std::lower_bound(blocks[block].begin(), blocks[block].end(), k);
                    auto lIt = std::upper_bound(blocks[block].begin(), blocks[block].end(), l);
                    count += std::distance(kIt, lIt);
                    j += blockSize - 1;
                } else if(k <= numbers[j] && numbers[j] <= l) {
                    count++;
                }
            }
            std::cout << count << "\n";
        }
    }

    return 0;
}
