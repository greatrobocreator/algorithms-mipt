#include <bits/stdc++.h>

class Node {
public:
    long long x, y;
    std::unique_ptr<Node> left, right;

    long long subTreeSize;

    Node(long long x, long long y, std::unique_ptr<Node> left = nullptr, std::unique_ptr<Node> right = nullptr);

    static long long getSubtreeSize(Node* node);
    void UpdateSubtreeSize();
};

Node::Node(long long int x, long long int y, std::unique_ptr<Node> left, std::unique_ptr<Node> right) : x(x), y(y), left(std::move(left)), right(std::move(right)) {
    UpdateSubtreeSize();
}

void Node::UpdateSubtreeSize() {
    subTreeSize = 1 + getSubtreeSize(left.get()) + getSubtreeSize(right.get());
}

long long Node::getSubtreeSize(Node* node) {
    return (node ? node->subTreeSize : 0);
}

class TTreap {
private:

    std::unique_ptr<Node> root;

public:

    TTreap(long long x) : root(std::make_unique<Node>(x, rand())) {};
    TTreap(std::unique_ptr<Node> root = nullptr) : root(std::move(root)) {};

    void Merge(TTreap& right);
    TTreap Split(long long pos);
    void Insert(long long pos, long long x);
    void Erase(long long pos);
    long long At(long long pos);
    void AddAt(long long pos, long long x);
    long long Size();

};

void TTreap::Merge(TTreap &right) {
    if(!right.root) return;
    if(!root) {
        root = std::move(right.root);
        return;
    }

    if(root->y < right.root->y) {
        TTreap rightChild(std::move(root->right));
        rightChild.Merge(right);
        root->right = std::move(rightChild.root);
    } else {
        TTreap leftChild(std::move(right.root->left));
        Merge(leftChild);
        right.root->left = std::move(root);
        root = std::move(right.root);
    }
    root->UpdateSubtreeSize();
}

TTreap TTreap::Split(long long pos) {
    if(!root) return TTreap();
    long long leftSubtreeSize = Node::getSubtreeSize(root->left.get()) + 1;
    if(pos >= leftSubtreeSize) {
        if(!root->right) return TTreap();
        TTreap rightChild(std::move(root->right));
        TTreap right = rightChild.Split(pos - leftSubtreeSize);
        root->right = std::move(rightChild.root);
        root->UpdateSubtreeSize();
        return right;
    } else {
        if(!root->left) return TTreap(std::move(root));
        TTreap leftChild(std::move(root->left));
        TTreap right(std::move(root));
        right.root->left = std::move(leftChild.Split(pos).root);
        root = std::move(leftChild.root);
        right.root->UpdateSubtreeSize();
        return right;
    }
}

void TTreap::Insert(long long pos, long long x) {
    TTreap right = Split(pos);
    TTreap center(x);
    Merge(center);
    Merge(right);
}

void TTreap::Erase(long long pos) {
    TTreap right = Split(pos + 1);
    Split(pos);
    Merge(right);
}

long long TTreap::At(long long pos) {
    TTreap right = Split(pos + 1);
    TTreap center = Split(pos);
    long long x = center.root->x;
    Merge(center);
    Merge(right);
    return x;
}

void TTreap::AddAt(long long pos, long long x) {
    TTreap right = Split(pos + 1);
    TTreap center = Split(pos);
    center.root->x += x;
    Merge(center);
    Merge(right);
}

long long TTreap::Size() {
    return (root ? root->subTreeSize : 0);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    srand(time(nullptr));

    std::freopen("river.in", "r", stdin);
    std::freopen("river.out", "w", stdout);

    TTreap tree;

    long long n, p;
    std::cin >> n >> p;

    long long length = 0;
    long long x;
    for(long long i = 0; i < n; ++i) {
        std::cin >> x;
        length += x * x;
        tree.Insert(i, x);
    }
    std::cout << length << std::endl;

    long long k;
    std::cin >> k;

    long long event, pos;
    for(long long i = 0; i < k; ++i) {
        std::cin >> event >> pos;
        if(event == 1) {
            long long len = tree.At(pos - 1);
            tree.Erase(pos - 1);
            if(pos == 1) {
                long long rightLength = tree.At(0);
                tree.AddAt(0, len);
                long long newRightLength = tree.At(0);
                length += (newRightLength * newRightLength - len * len - rightLength * rightLength);
            } else if(pos == tree.Size() + 1) {
                long long leftLength = tree.At(pos - 2); // -2 because we deleted (pos - 1)
                tree.AddAt(pos - 2, len);
                long long newLeftLength = leftLength + len;
                length += (newLeftLength * newLeftLength - len * len - leftLength * leftLength);
            } else {
                long long leftLength = tree.At(pos - 2);
                long long rightLength = tree.At(pos - 1);
                long long leftDelta = len / 2;
                long long rightDelta = (len + 1) / 2;
                tree.AddAt(pos - 2, leftDelta);
                tree.AddAt(pos - 1, rightDelta);
                long long newLeftLength = leftLength + leftDelta;
                long long newRightLength = rightLength + rightDelta;
                length += (newRightLength * newRightLength + newLeftLength * newLeftLength - len * len - leftLength * leftLength - rightLength * rightLength);
            }
        } else if(event == 2) {
            long long len = tree.At(pos - 1);
            long long leftPart = len / 2;
            long long rightPart = (len + 1) / 2;
            tree.Erase(pos - 1);
            tree.Insert(pos - 1, leftPart);
            tree.Insert(pos, rightPart);
            length += (leftPart * leftPart + rightPart * rightPart - len * len);
        }
        std::cout << length << std::endl;
    }

    return 0;
}
