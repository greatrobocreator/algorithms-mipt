#include <bits/stdc++.h>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, k, p;
    std::cin >> n >> k >> p;
    std::vector< long long > order(p, 0);
    for(long long i = 0; i < p; ++i) {
        std::cin >> order[i];
        --order[i];
    }

    std::vector< std::pair< long long, std::vector< long long > > > occurrences(n, {0, {}});
    for(long long i = 0; i < p; ++i) occurrences[order[i]].second.push_back(i);
    for(long long i = 0; i < n; ++i) occurrences[i].second.push_back(LLONG_MAX);

    std::set< long long > currentCars;
    std::set< std::pair< long long, long long > > nextCurrentCarsOccurrences;
    /*currentCars.insert(order[0]);
    long long firstCarsNextOccur = occurrences[order[0]].size() > 1
    nextCurrentCarsOccurrences.insert( { occurrences[order[0]], order[0] } );*/

    long long ans = 0;
    for(long long i = 0; i < p; ++i) {
        auto it = currentCars.find(order[i]);
        if(it != currentCars.end()) {
            std::pair< long long, std::vector< long long > >& pair = occurrences[order[i]];
            auto nextOccurIt = nextCurrentCarsOccurrences.find( { pair.second[pair.first], order[i] } );
            for(; pair.second[pair.first] <= i; ++pair.first);
            nextCurrentCarsOccurrences.erase(nextOccurIt);
            nextCurrentCarsOccurrences.insert( { pair.second[pair.first], order[i] } );
            continue;
        };
        if(currentCars.size() >= k) {
            /*auto last = currentCars.begin();
            for (auto it = currentCars.begin(); it != currentCars.end(); ++it) {
                std::pair< long long, std::vector< long long > >& pair = occurrences[*it];
                for(; pair.first < pair.second.size() && pair.second[pair.first] <= i; ++pair.first);
                if (pair.first == pair.second.size() || pair.second[pair.first] > occurrences[*last].second[occurrences[*last].first]) last = it;
                if (pair.first == pair.second.size()) it = --currentCars.end();
            }*/
            //std::cout << "Push " << *last + 1 << " Get " << order[i] + 1 << std::endl;
            long long toErase = (--nextCurrentCarsOccurrences.end())->second;
            currentCars.erase(toErase);
            nextCurrentCarsOccurrences.erase(--nextCurrentCarsOccurrences.end());
        }
        currentCars.insert(order[i]);

        std::pair< long long, std::vector< long long > >& pair = occurrences[order[i]];
        for(; pair.second[pair.first] <= i; ++pair.first);
        nextCurrentCarsOccurrences.insert({ pair.second[pair.first], order[i] });

        ++ans;
    }

    std::cout << ans << std::endl;

    return 0;
}