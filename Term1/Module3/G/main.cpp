#include <bits/stdc++.h>

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n, x, y;
    std::cin >> n >> y >> x;

    std::vector< std::tuple< long long, bool, long long > > segments; // std::tuple< y, close?, x >
    long long x_i, y1, y2;
    for(long long i = 0; i < n; ++i) {
        std::cin >> x_i >> y1 >> y2;
        segments.emplace_back(y1, false, x_i);
        segments.emplace_back(y2, true, x_i);
    }
    std::sort(segments.begin(), segments.end());


    std::multiset< long long > race;
    race.insert(x);

    std::multiset< long long > stations;
    stations.insert(0);
    stations.insert(x);

    for(long long currentY = 0, i = 0; currentY <= y; ++currentY) {
        //std::cout << std::get<0>(segments[i]) << " " << std::get<2>(segments[i]) << " "
                //<< std::get<2>(segments[i]) << std::endl;
        for(; std::get<0>(segments[i]) == currentY && !std::get<1>(segments[i]); ++i) {
            auto it = stations.insert(std::get<2>(segments[i]));
            race.erase(race.find(*std::next(it) - *std::prev(it)));
            race.insert(*std::next(it) - *it);
            race.insert(*it - *std::prev(it));
        }

        std::cout << *(--race.end()) << std::endl;

        for(; std::get<0>(segments[i]) == currentY && std::get<1>(segments[i]); ++i) {
            auto it = stations.find(std::get<2>(segments[i]));
            race.erase(race.find(*std::next(it) - *it));
            race.erase(race.find(*it - *std::prev(it)));
            race.insert(*std::next(it) - *std::prev(it));
            it = stations.erase(it);
        }
    }

    return 0;
}
