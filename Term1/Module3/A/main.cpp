#include <bits/stdc++.h>

class Node {
public:
    long long key;
    long long height;
    long long delta;
    std::unique_ptr<Node> left;
    std::unique_ptr<Node> right;

    Node(long long key) : key(key), delta(0), height(1), left(nullptr), right(nullptr) {};

    static long long getHeight(Node* node);
    void UpdateHeight();
};

void Node::UpdateHeight() {
    height = std::max(getHeight(left.get()), getHeight(right.get())) + 1;
    delta = getHeight(left.get()) - getHeight(right.get());
}

long long Node::getHeight(Node* node) {
    return node ? node->height : 0;
}

class TAVLTree {
private:

    std::unique_ptr<Node> root;

    static std::unique_ptr<Node> rotateLeft(std::unique_ptr<Node>& q);
    static std::unique_ptr<Node> rotateRight(std::unique_ptr<Node>& p);
    static std::unique_ptr<Node> balance(std::unique_ptr<Node> node);

    std::unique_ptr<Node> insert(std::unique_ptr<Node> node, long long key);

    static std::unique_ptr<Node> eraseMin(std::unique_ptr<Node> node, std::unique_ptr<Node>& min);
    static std::unique_ptr<Node> erase(std::unique_ptr<Node> node, long long key);

    static bool next(Node* node, long long key, long long& result);
    static bool prev(Node* node, long long key, long long& result);

public:

    void Insert(long long key);
    void Erase(long long key);
    bool Exists(long long key);
    bool Next(long long key, long long& result);
    bool Prev(long long key, long long& result);

    TAVLTree() : root(nullptr) {};
};

std::unique_ptr<Node> TAVLTree::rotateRight(std::unique_ptr<Node>& p) {
    std::unique_ptr<Node> q = std::move(p->left);
    p->left = std::move(q->right);
    q->right = std::move(p);
    q->right->UpdateHeight();
    q->UpdateHeight();
    return q;
}

std::unique_ptr<Node> TAVLTree::rotateLeft(std::unique_ptr<Node>& q) {
    std::unique_ptr<Node> p = std::move(q->right);
    q->right = std::move(p->left);
    p->left = std::move(q);
    p->left->UpdateHeight();
    p->UpdateHeight();
    return p;
}

std::unique_ptr<Node> TAVLTree::balance(std::unique_ptr<Node> node) {
    node->UpdateHeight();
    if(node->delta == 2) {
        if(node->left->delta < 0) {
            node->left = rotateLeft(node->left);
        }
        return rotateRight(node);
    } else if(node->delta == -2) {
        if(node->right->delta > 0) {
            node->right = rotateRight(node->right);
        }
        return rotateLeft(node);
    }
    return node;
}

std::unique_ptr<Node> TAVLTree::insert(std::unique_ptr<Node> node, long long int key) {
    if(!node) return std::make_unique<Node>(key);
    if(key < node->key) {
        node->left = insert(std::move(node->left), key);
    } else if(key > node->key){
        node->right = insert(std::move(node->right), key);
    }
    return balance(std::move(node));
}

// finding min and extracting them. Then rebalancing tree
std::unique_ptr<Node> TAVLTree::eraseMin(std::unique_ptr<Node> node, std::unique_ptr<Node>& min) {
    if(!node->left) {
        min = std::move(node);
        return std::move(min->right);
    }

    node->left = eraseMin(std::move(node->left), min);
    return balance(std::move(node));
}

std::unique_ptr<Node> TAVLTree::erase(std::unique_ptr<Node> node, long long key) {
    if(!node) return nullptr;

    if(key < node->key) {
        node->left = erase(std::move(node->left), key);
    } else if(key > node->key) {
        node->right = erase(std::move(node->right), key);
    } else {
        if(!node->right) return std::move(node->left);

        std::unique_ptr<Node> min;
        std::unique_ptr<Node> right = eraseMin(std::move(node->right), min);
        min->right = std::move(right);
        min->left = std::move(node->left);
        return balance(std::move(min));
    }

    return balance(std::move(node));
}

void TAVLTree::Insert(long long int key) {
    root = insert(std::move(root), key);
}

void TAVLTree::Erase(long long int key) {
    root = erase(std::move(root), key);
}

bool TAVLTree::Exists(long long int key) {
    Node* node = root.get();
    if(!node) return false;
    while(node->key != key) {
        if(key > node->key) {
            if(!node->right) return false;
            node = node->right.get();
        } else if(key < node->key) {
            if(!node->left) return false;
            node = node->left.get();
        }
    }
    return true;
}

bool TAVLTree::next(Node* node, long long key, long long& result) {
    if(!node) return false;

    if(key >= node->key) {
        return next(node->right.get(), key, result);
    } else {
        if(!next(node->left.get(), key, result)) {
            result = node->key;
        }
        return true;
    }
}

bool TAVLTree::Next(long long key, long long& result) {
    return next(root.get(), key, result);
}

bool TAVLTree::prev(Node* node, long long key, long long& result) {
    if(!node) return false;

    if(key <= node->key) {
        return prev(node->left.get(), key, result);
    } else {
        if(!prev(node->right.get(), key, result)) {
            result = node->key;
        }
        return true;
    }
}

bool TAVLTree::Prev(long long key, long long& result) {
    return prev(root.get(), key, result);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    TAVLTree tree;

    std::string cmd;
    while(std::cin >> cmd) {
        if(cmd == "insert") {
            long long x;
            std::cin >> x;
            tree.Insert(x);
        } else if(cmd == "delete") {
            long long x;
            std::cin >> x;
            tree.Erase(x);
        } else if(cmd == "exists") {
            long long x;
            std::cin >> x;
            std::cout << (tree.Exists(x) ? "true" : "false") << std::endl;
        } else if(cmd == "next") {
            long long x, result;
            std::cin >> x;
            std::cout << (tree.Next(x, result) ? std::to_string(result) : "none") << std::endl;
        } else if(cmd == "prev") {
            long long x, result;
            std::cin >> x;
            std::cout << (tree.Prev(x, result) ? std::to_string(result) : "none") << std::endl;
        } else {
            std::cout << "Something went wrong" << std::endl;
        }
    }

    return 0;
}
