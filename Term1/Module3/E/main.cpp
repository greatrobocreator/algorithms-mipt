#include <bits/stdc++.h>

using ll = long long;
using ull = unsigned long long;

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    unsigned long long n, m;
    long long k;

    std::cin >> n >> m >> k;

    std::map<unsigned long long, std::set<long long> > setsWithElement;
    std::vector<std::set<long long> > sets(m + 1);

    std::string cmd;
    for (long long i = 0; i < k; ++i) {
        std::cin >> cmd;
        if (cmd == "ADD") {
            long long s;
            unsigned long long e;
            std::cin >> e >> s;
            sets[s].insert(e);
            setsWithElement[e].insert(s);
        } else if (cmd == "DELETE") {
            long long s;
            unsigned long long e;
            std::cin >> e >> s;
            sets[s].erase(e);
            setsWithElement[e].erase(s);
        } else if (cmd == "CLEAR") {
            long long s;
            std::cin >> s;
            for (auto it = sets[s].begin(); it != sets[s].end(); ++it) {
                setsWithElement[*it].erase(s);
            }
            sets[s].clear();
        } else if (cmd == "LISTSET") {
            long long s;
            std::cin >> s;
            if (sets[s].empty()) {
                std::cout << "-1" << std::endl;
            } else {
                std::copy(sets[s].begin(), sets[s].end(), std::ostream_iterator<long long>(std::cout, " "));
                std::cout << std::endl;
            }
        } else if (cmd == "LISTSETSOF") {
            unsigned long long e;
            std::cin >> e;
            if (setsWithElement[e].empty()) {
                std::cout << "-1" << std::endl;
            } else {
                std::copy(setsWithElement[e].begin(), setsWithElement[e].end(),
                          std::ostream_iterator<long long>(std::cout, " "));
                std::cout << std::endl;
            }
        }
    }


    return 0;
}
