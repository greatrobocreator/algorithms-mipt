#include <iostream>
#include <vector>
#include <bits/unique_ptr.h>
#include <cassert>

class Node {
public:
    std::vector<long long> keys;
    std::vector< std::unique_ptr< Node > > children;

    Node(long long key, std::unique_ptr< Node > left = nullptr, std::unique_ptr< Node > right = nullptr);
    Node(std::vector<long long>& _keys, std::vector< std::unique_ptr< Node > >& _children, long long from, long long to);

    void Append(long long key, Node* node);
    long long getSubtreeSize();
};

long long Node::getSubtreeSize() {
    long long subtreeSize = keys.size();
    if(!children.empty() && children[0]) { // not leaf
        for (auto &child : children) subtreeSize += child->getSubtreeSize();
    }
    return subtreeSize;
}

Node::Node(long long int key, std::unique_ptr< Node > left, std::unique_ptr< Node > right) {
    keys.push_back(key);
    children.push_back(std::move(left));
    children.push_back(std::move(right));
}

Node::Node(std::vector<long long>& _keys, std::vector< std::unique_ptr< Node > >& _children, long long from, long long to) {
    for(long long i = from; i <= to; ++i) {
        keys.push_back(_keys[i]);
    }
    for(long long i = from; i <= to + 1; ++i) {
        children.push_back(std::move(_children[i]));
    }
}

void Node::Append(long long key, Node* node) {
    keys.push_back(key);
    keys.insert(keys.end(), node->keys.begin(), node->keys.end());
    children.insert(children.end(), std::make_move_iterator(node->children.begin()), std::make_move_iterator(node->children.end()));
}

class TBTree {

private:

    long long t;
    std::unique_ptr< Node > root;

    Node* expandChild(Node* child, Node* parent);
    long long eraseMin(Node* node);
    long long eraseMax(Node* node);

public:

    TBTree(long long t, std::unique_ptr< Node > root = nullptr) : t(t), root(std::move(root)) {};

    void Insert(long long key);
    void Erase(long long key);
    long long kthMax(long long k);
};

void TBTree::Insert(long long key) {
    if(!root) {
        root = std::make_unique< Node >(key);
        return;
    }
    Node* parent = nullptr;
    Node* node = root.get();
    Node *leftPart = nullptr, *rightPart = nullptr;
    while(true) {
        long long childIndex = std::distance(node->keys.begin(), std::lower_bound(node->keys.begin(), node->keys.end(), key));
        if(node->keys[childIndex] == key) return; // ignoring repeating keys
        Node* child = node->children[childIndex].get();
        if(node->keys.size() == 2 * t - 1) {
            if(node != root.get()) {
                long long indexInParent = std::distance(parent->keys.begin(), std::lower_bound(parent->keys.begin(), parent->keys.end(), node->keys[0]));
                std::vector< long long > keys = node->keys;
                std::vector< std::unique_ptr< Node > > children = std::move(node->children);
                parent->keys.insert(parent->keys.begin() + indexInParent, keys[t - 1]);
                parent->children[indexInParent] = std::make_unique< Node >(keys, children, 0, t - 2);
                leftPart = parent->children[indexInParent].get();
                parent->children.insert(parent->children.begin() + indexInParent + 1, std::make_unique< Node >(keys, children, t, 2 * t - 2));
                rightPart = parent->children[indexInParent + 1].get();
            } else {
                std::unique_ptr< Node > newRoot = std::make_unique< Node >(root->keys[t - 1],
                                         std::make_unique< Node >(root->keys, root->children, 0, t - 2),
                                                 std::make_unique< Node >(root->keys, root->children, t, 2 * t - 2));
                leftPart = newRoot->children[0].get();
                rightPart = newRoot->children[1].get();

                root = std::move(newRoot);
            }
        }
        if(!child) { // leaf
            if(leftPart) {
                node = (childIndex < t ? leftPart : rightPart);
                childIndex = (childIndex < t ? childIndex : childIndex - t);
            }
            node->keys.insert(node->keys.begin() + (childIndex), key);
            node->children.push_back(nullptr);
            return;
        }
        if(leftPart) {
            parent = (childIndex < t ? leftPart : rightPart);
        } else parent = node;
        node = child;
        leftPart = rightPart = nullptr;
    }
}

Node* TBTree::expandChild(Node *child, Node *parent) {
    if(!child || !parent || child->keys.size() >= t) return nullptr;

    long long indexInParent = std::distance(parent->keys.begin(), std::lower_bound(parent->keys.begin(), parent->keys.end(), child->keys[0]));
    if(indexInParent > 0 && parent->children[indexInParent - 1]->keys.size() >= t) { // from left brother
        Node* leftBrother = parent->children[indexInParent - 1].get();

        child->keys.insert(child->keys.begin(), parent->keys[indexInParent - 1]);
        parent->keys[indexInParent - 1] = leftBrother->keys.back();
        leftBrother->keys.pop_back();

        child->children.insert(child->children.begin(), std::move(leftBrother->children.back()));
        leftBrother->children.pop_back();
        return child;
    } else if(indexInParent < parent->children.size() - 1  && parent->children[indexInParent + 1]->keys.size() >= t) { // from right brother
        Node* rightBrother = parent->children[indexInParent + 1].get();

        child->keys.push_back(parent->keys[indexInParent]);
        parent->keys[indexInParent] = rightBrother->keys.front();
        rightBrother->keys.erase(rightBrother->keys.begin());

        child->children.push_back(std::move(rightBrother->children.front()));
        rightBrother->children.erase(rightBrother->children.begin());
        return child;
    } else { // merging with brother
        Node* leftPart;
        Node* rightPart;
        if(indexInParent > 0) { // with left
            leftPart = parent->children[indexInParent - 1].get();
            rightPart = child;
            indexInParent--; // now it points between parts
        } else { // with right
            leftPart = child;
            rightPart = parent->children[indexInParent + 1].get();
        }
        leftPart->Append(parent->keys[indexInParent], rightPart);

        parent->keys.erase(parent->keys.begin() + indexInParent);
        parent->children.erase(parent->children.begin() + indexInParent + 1);
        if(parent->keys.empty()) { // parent == root
            root = std::move(root->children[indexInParent]);
        }
        return leftPart;
    }
}

void TBTree::Erase(long long key) {
    if(!root) return;

    Node* node = root.get();
    Node* parent = nullptr;
    while(node != nullptr) {
        if(node != root.get() && node->keys.size() < t) node = expandChild(node, parent);
        parent = node;

        auto it = std::lower_bound(node->keys.begin(), node->keys.end(), key);
        if(*it == key) {
            assert(!node->children.empty());
            if(!node->children[0]) { // leaf
                node->keys.erase(it);
                node->children.pop_back();
                return;
            } else {
                long long index = std::distance(node->keys.begin(), it);
                if(node->children[index]->keys.size() >= t) {
                    (*it) = eraseMax(node->children[index].get());
                    return;
                } else if(node->children[index + 1]->keys.size() >= t) {
                    (*it) = eraseMin(node->children[index + 1].get());
                    return;
                } else { // merging two children
                    Node* leftChild = node->children[index].get();
                    Node* rightChild = node->children[index + 1].get();
                    leftChild->Append(key, rightChild);

                    node->keys.erase(it);
                    node->children.erase(node->children.begin() + index + 1);
                    if(node->keys.empty()) { // node == root
                        root = std::move(node->children[index]);
                    }
                    node = leftChild;
                }
            }
        } else {
            node = node->children[std::distance(node->keys.begin(), it)].get();
        }
    }
}

long long TBTree::eraseMax(Node *node) {
    if(!node) return std::numeric_limits<long long>::max();

    Node* parent = nullptr;
    while(node) {
        if(node != root.get() && node->keys.size() < t) {
            if(!parent) return std::numeric_limits<long long>::max();
            node = expandChild(node, parent);
        }
        parent = node;
        node = node->children.back().get();
    }
    long long key = parent->keys.back();
    parent->keys.pop_back();
    parent->children.pop_back();
    return key;
}

long long TBTree::eraseMin(Node *node) {
    if(!node) return std::numeric_limits<long long>::min();

    Node* parent = nullptr;
    while(node) {
        if(node != root.get() && node->keys.size() < t) {
            if(!parent) return std::numeric_limits<long long>::min();
            node = expandChild(node, parent);
        }
        parent = node;
        node = node->children.front().get();
    }
    long long key = parent->keys.front();
    parent->keys.erase(parent->keys.begin());
    parent->children.pop_back();
    return key;
}

long long TBTree::kthMax(long long k) {
    if(!root) return std::numeric_limits<long long>::min();
    if (k > root->getSubtreeSize()) return std::numeric_limits<long long>::min();

    Node* node = root.get();
    while(!node->children.empty() &&  node->children[0]) {
        if (k > node->getSubtreeSize()) return std::numeric_limits<long long>::min();
        long long i = node->children.size() - 1;
        for (; i >= 0 && k > node->children[i]->getSubtreeSize(); k -= node->children[i]->getSubtreeSize() + 1, --i);
        if(k == 0) return node->keys[i];
        node = node->children[i].get();
    }
    return node->keys[node->keys.size() - k];
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    TBTree tree(10000);

    long long n;
    std::cin >> n;

    long long cmd, k;
    for(long long i = 0; i < n; ++i) {
        std::cin >> cmd >> k;
        if(cmd == 1) {
            tree.Insert(k);
        } else if(cmd == -1) {
            tree.Erase(k);
        } else if(cmd == 0) {
            std::cout << tree.kthMax(k) << std::endl;
        } else {
            std::cout << "Something went wrong!" << std::endl;
        }
    }

    return 0;
}
