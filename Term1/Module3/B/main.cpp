#include <bits/stdc++.h>

struct Node {

    long long key, sum;
    Node *left, *right, *parent;

    Node(long long key, Node* left = nullptr, Node* right = nullptr, Node* parent = nullptr);
    ~Node();

    static long long getSum(Node* node);
    void UpdateSum();
};

Node::~Node() {
    delete left;
    delete right;
}

Node::Node(long long int key, Node *left, Node *right, Node *parent)  : key(key), sum(key), left(left), right(right), parent(parent) {
    UpdateSum();
}

long long Node::getSum(Node* node) {
    return (node ? node->sum : 0);
}

void Node::UpdateSum() {
    sum = key + getSum(left) + getSum(right);
}

class TSplayTree {
private:

    Node* root;

    static void rotateLeft(Node* node);
    static void rotateRight(Node* node);
    static void rotate(Node* node, bool right);

    static Node* splay(Node* node);
    static Node* find(Node* node, long long key);

public:
    TSplayTree() : root(nullptr) {}
    TSplayTree(Node* root) : root(root) {}
    ~TSplayTree();

    TSplayTree Split(long long key);
    void Merge(TSplayTree& tree);
    void Add(long long key);
    long long Sum(long long l, long long r);
};

void TSplayTree::rotateLeft(Node* node) {
    if (!node->right) return;

    Node* parent = node->parent;
    Node* right = node->right;

    bool rightChild;
    if (parent) rightChild = parent->right == node;

    node->right = right->left;
    if(node->right) node->right->parent = node;

    right->left = node;
    node->parent = right;

    right->parent = parent;
    if(parent) (rightChild ? parent->right : parent->left) = right;

    node->UpdateSum();
    right->UpdateSum();
    if(parent) parent->UpdateSum();
}

void TSplayTree::rotateRight(Node* node) {
    if (!node->left) return;

    Node* parent = node->parent;
    Node* left = node->left;

    bool rightChild;
    if (parent) rightChild = parent->right == node;

    node->left = left->right;
    if(node->left) node->left->parent = node;

    left->right = node;
    node->parent = left;

    left->parent = parent;
    if(parent) (rightChild ? parent->right : parent->left) = left;

    node->UpdateSum();
    left->UpdateSum();
    if(parent) parent->UpdateSum();
}

void TSplayTree::rotate(Node* node, bool right) {
    if(right) rotateRight(node);
    else rotateLeft(node);
}

Node* TSplayTree::splay(Node* node) {
    for(Node* parent = node->parent; parent; parent = node->parent) {
        bool rightChild = (parent->right == node);
        Node* grandParent = parent->parent;
        if(!grandParent) { // zig
            rotate(parent, !rightChild);
        } else if((grandParent->right == parent) == rightChild){ // zig-zig
            rotate(grandParent, !rightChild);
            rotate(parent, !rightChild);
        } else { // zig-zag
            rotate(parent, !rightChild);
            rotate(grandParent, rightChild);
        }
    }
    return node;
}

Node* TSplayTree::find(Node* node, long long int key) {
    if(!node) return nullptr;
    if(node->key == key) return splay(node);
    if(key < node->key && node->left) return find(node->left, key);
    if(key > node->key && node->right) return find(node->right, key);
    return splay(node);
}

TSplayTree TSplayTree::Split(long long int key) {
    if(!root) return TSplayTree();
    root = find(root, key);
    if(root->key <= key) {
        Node* right = root->right;
        root->right = nullptr;
        root->UpdateSum();
        if(right) {
            right->parent = nullptr;
            right->UpdateSum();
        }
        return TSplayTree(right);
    } else {
        Node* left = root->left;
        if(left) left->parent = nullptr;
        root->left = nullptr;
        Node* secondRoot = root;
        root = left;

        if(root) root->UpdateSum();
        secondRoot->UpdateSum();

        return TSplayTree(secondRoot);
    }
}

void TSplayTree::Merge(TSplayTree& tree) {
    if(!tree.root) return;

    root = find(root, tree.root->key);

    if(root) {
        root->right = tree.root;
        tree.root->parent = root;
    } else root = tree.root;
    tree.root = nullptr; // in order not to delete root->right subtree
}

void TSplayTree::Add(long long int key) {
    root = find(root, key);
    if(root && root->key == key) return;

    TSplayTree right = Split(key);
    Node* newRoot = new Node(key, root, right.root);
    if(root) root->parent = newRoot;
    if(right.root) right.root->parent = newRoot;

    root = newRoot;
    right.root = nullptr;

}

TSplayTree::~TSplayTree() {
    delete root;
}

long long TSplayTree::Sum(long long l, long long r) {
    if(r < l) return 0;

    TSplayTree rightPart = Split(r);
    TSplayTree centerPart = Split(l - 1);
    long long sum = Node::getSum(centerPart.root);
    Merge(centerPart);
    Merge(rightPart);
    return sum;
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    TSplayTree tree;

    long long n;
    std::cin >> n;

    std::string cmd;
    long long carry = 0;
    for(long long i = 0; i < n; ++i) {
        std::cin >> cmd;
        if(cmd == "+") {
            long long x;
            std::cin >> x;
            tree.Add((x + carry) % 1000000000);
            carry = 0;
        } else if(cmd == "?") {
            long long l, r;
            std::cin >> l >> r;
            carry = tree.Sum(l, r);
            std::cout << carry << std::endl;
        } else {
            std::cout << "Something went wrong!" << std::endl;
        }
    }

    return 0;
}
