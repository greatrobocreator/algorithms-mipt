#include <bits/stdc++.h>

class Node {
public:
    long long x, y;
    std::unique_ptr<Node> left, right;

    long long subTreeSize, sum;
    long long add;
    bool needToSetZero;
    //long long min, max;
    bool isSorted, isReverseSorted;
    bool toReverse;
    long long rightKey, leftKey;

    Node(long long x, long long y, std::unique_ptr<Node> left = nullptr, std::unique_ptr<Node> right = nullptr);

    static long long getSubtreeSize(Node* node);
    static long long getSum(Node* node);
    static bool getIsSorted(Node* node);
    static bool getIsReverseSorted(Node* node);
    void Update();
    void Push();

    void Print(std::vector< long long >& result);
};

Node::Node(long long x, long long y, std::unique_ptr<Node> left, std::unique_ptr<Node> right) : x(x), y(y), left(std::move(left)), right(std::move(right)), add(0), needToSetZero(false), toReverse(false) {
    Update();
}

void Node::Update() {
    Push();
    if(left) left->Push();
    if(right) right->Push();

    subTreeSize = 1 + getSubtreeSize(left.get()) + getSubtreeSize(right.get());
    sum = x + getSum(left.get()) + getSum(right.get());
    leftKey = rightKey = x;
    if(left) leftKey = left->leftKey;
    if(right) rightKey = right->rightKey;
    isSorted = (getIsSorted(left.get()) && getIsSorted(right.get()) && (left ? left->rightKey <= x : true) && (right ? right->leftKey >= x : true));
    isReverseSorted = (getIsReverseSorted(left.get()) && getIsReverseSorted(right.get()) && (left ? left->rightKey >= x : true) && (right ? right->leftKey <= x : true));
    add = 0;
    needToSetZero = false;
    toReverse = false;
}

long long Node::getSubtreeSize(Node* node) {
    return (node ? node->subTreeSize : 0);
}

long long Node::getSum(Node* node) {
    return (node ? node->sum : 0);
}
bool Node::getIsSorted(Node *node) {
    return (node ? node->isSorted : true);
}

bool Node::getIsReverseSorted(Node *node) {
    return (node ? node->isReverseSorted : true);
}

void Node::Push() {
    if(toReverse) {
        std::swap(isSorted, isReverseSorted);
        std::swap(leftKey, rightKey);
        std::swap(left, right);
        if(left) left->toReverse ^= true;
        if(right) right->toReverse ^= true;
        toReverse = false;
    }
    if(needToSetZero) {
        leftKey = rightKey = x = sum = 0;
        isReverseSorted = isSorted = true;
        toReverse = false;
        needToSetZero = false;
        if(left) {
            left->needToSetZero = true;
            left->add = 0;
        }
        if(right) {
            right->needToSetZero = true;
            right->add = 0;
        }
    }
    if(add != 0) {
        x += add;
        sum += add * subTreeSize;
        leftKey += add;
        rightKey += add;
        if(left) left->add += add;
        if(right) right->add += add;
        add = 0;
    }
}

void Node::Print(std::vector< long long >& result) {
    Push();
    if(left) left->Print(result);
    result.push_back(x);
    if(right) right->Print(result);
}

class TTreap {
private:

    std::unique_ptr<Node> root;
    long long _sortedSize(Node* node);

public:

    TTreap(long long x) : root(std::make_unique<Node>(x, rand())) {};
    TTreap(std::unique_ptr<Node> root = nullptr) : root(std::move(root)) { if(root) root->Update(); };

    void Merge(TTreap& right);
    TTreap Split(long long pos);
    void Insert(long long pos, long long x);
    void Erase(long long pos);
    void Swap(long long l, long long r);

    long long At(long long pos);
    long long Size();
    long long SortedSize();
    long long ReverseSortedSize();
    long long Next(long long x); // only if reverse sorted!
    long long Prev(long long x); // only if sorted!

    void AddAt(long long l, long long r, long long x);
    void SetAt(long long l, long long r, long long x);
    long long SumAt(long long l, long long r);

    void Reverse(long long l, long long r);
    void NextPermutation(long long l, long long r);
    void PrevPermutation(long long l, long long r);

    void Print();
    void PrintInVector(std::vector< long long >& result);

};

void TTreap::Merge(TTreap &right) {
    if(!right.root) return;
    if(!root) {
        root = std::move(right.root);
        return;
    }

    root->Push();
    right.root->Push();

    if(root->y < right.root->y) {
        TTreap rightChild(std::move(root->right));
        rightChild.Merge(right);
        root->right = std::move(rightChild.root);
    } else {
        TTreap leftChild(std::move(right.root->left));
        Merge(leftChild);
        right.root->left = std::move(root);
        root = std::move(right.root);
    }
    root->Update();
}

TTreap TTreap::Split(long long pos) {
    if(!root) return TTreap();

    root->Push();

    long long leftSubtreeSize = Node::getSubtreeSize(root->left.get()) + 1;
    if(pos >= leftSubtreeSize) {
        if(!root->right) return TTreap();
        TTreap rightChild(std::move(root->right));
        TTreap right = rightChild.Split(pos - leftSubtreeSize);
        root->right = std::move(rightChild.root);
        if(right.root) right.root->Update();
        if(root) root->Update();
        return right;
    } else {
        if(!root->left) return TTreap(std::move(root));
        TTreap leftChild(std::move(root->left));
        TTreap right(std::move(root));
        right.root->left = std::move(leftChild.Split(pos).root);
        root = std::move(leftChild.root);
        if(right.root) right.root->Update();
        if(root) root->Update();
        return right;
    }
}

void TTreap::Insert(long long pos, long long x) {
    TTreap right = Split(pos);
    TTreap center(x);
    Merge(center);
    Merge(right);
}

void TTreap::Erase(long long pos) {
    TTreap right = Split(pos + 1);
    Split(pos);
    Merge(right);
}

long long TTreap::At(long long pos) {
    TTreap right = Split(pos + 1);
    TTreap center = Split(pos);
    long long x = center.root->x;
    Merge(center);
    Merge(right);
    return x;
}

void TTreap::AddAt(long long l, long long r, long long x) {
    TTreap right = Split(r + 1);
    TTreap center = Split(l);
    center.root->add += x;
    Merge(center);
    Merge(right);
}

void TTreap::SetAt(long long int l, long long int r, long long int x) {
    TTreap right = Split(r + 1);
    TTreap center = Split(l);
    center.root->needToSetZero = true;
    center.root->add = x;
    Merge(center);
    Merge(right);
}

long long TTreap::Size() {
    return (root ? root->subTreeSize : 0);
}

long long TTreap::SumAt(long long l, long long r) {
    TTreap right = Split(r + 1);
    TTreap center = Split(l);
    long long result = center.root->sum;
    Merge(center);
    Merge(right);
    return result;
}

void TTreap::Print() {
    std::vector< Node* > current;
    std::vector< Node* > next;

    long long currentOffset = (long long) pow(2, ceil(log2(Size())) + 3);
    current.push_back(root.get());
    bool hasNotNull = true;
    for(; hasNotNull; currentOffset /= 2) {
        hasNotNull = false;
        std::cout << std::string(currentOffset, '-');
        for(long long i = 0; i < current.size(); ++i) {
            if(current[i]) {
                //current[i]->Push();
                current[i]->Update();
                std::cout << current[i]->x << ":" << current[i]->isReverseSorted << ":" << current[i]->leftKey << ":" << current[i]->rightKey;
                next.push_back(current[i]->left.get());
                next.push_back(current[i]->right.get());
                hasNotNull = true;
            } else {
                next.push_back(nullptr);
                next.push_back(nullptr);
            }
            std::cout << std::string(currentOffset * 2, ' ');
        }
        std::cout << std::endl;
        current = next;
        next.clear();
    }
}

/*long long TTreap::SortedSize() {
    if(!root) return 0;
    root->Push();
    if(root->isSorted) return root->subTreeSize;

    long long size = 0;
    //long long border = LLONG_MAX;
    Node* node = root.get();
    while(node) {
        if(node->left) {
            node->left->Push();
            if(node->left->left) node->left->left->Push();
            if(node->left->right) node->left->right->Push();
        }
        if(node->right) {
            node->right->Push();
            if(node->right->left) node->right->left->Push();
            if(node->right->right) node->right->right->Push();
        }

        /*if(node->max > border) {
            node = node->right.get();
            continue;
        }
        if(!Node::getIsSorted(node->right.get())) {
            node = node->right.get();
            continue;
        }
        size += Node::getSubtreeSize(node->right.get());
        if(node->x > Node::getMin(node->right.get())) break;
        size++;
        border = node->x;
        node = node->left.get();***
        if(!Node::getIsSorted(node->right.get())) {
            node = node->right.get();
            continue;
        }
        size += Node::getSubtreeSize(node->right.get());
        if(node->right && node->x > node->right->leftKey) break;
        size++;
        if(node->left && node->left->rightKey > node->x) break;
        node = node->left.get();
    }
    return size;
}*/

long long TTreap::SortedSize() {
    return Size() - _sortedSize(root.get());
}

long long TTreap::_sortedSize(Node *node) {
    if(!node) return 0;
    node->Push();
    if(node->left) {
        node->left->Push();
        if(node->left->left) node->left->left->Push();
        if(node->left->right) node->left->right->Push();
    }
    if(node->right) {
        node->right->Push();
        if(node->right->left) node->right->left->Push();
        if(node->right->right) node->right->right->Push();
    }
    if (node->right && !node->right->isSorted) {
        return 1 + Node::getSubtreeSize(node->left.get()) + _sortedSize(node->right.get());
    }
    if (node->right && node->x > node->right->leftKey) {
        return 1 + Node::getSubtreeSize(node->left.get());
    }
    if (node->left && node->x < node->left->rightKey) {
        return Node::getSubtreeSize(node->left.get());
    }
    return _sortedSize(node->left.get());
}

long long TTreap::ReverseSortedSize() {
    if(!root) return 0;
    root->Push();
    if(root->isReverseSorted) return root->subTreeSize;

    long long size = 0;
    Node* node = root.get();
    while(node) {
        if(node->left) {
            node->left->Push();
            if(node->left->left) node->left->left->Push();
            if(node->left->right) node->left->right->Push();
        }
        if(node->right) {
            node->right->Push();
            if(node->right->left) node->right->left->Push();
            if(node->right->right) node->right->right->Push();
        }

        if(!Node::getIsReverseSorted(node->right.get())) {
            node = node->right.get();
            continue;
        }
        size += Node::getSubtreeSize(node->right.get());
        if(node->right && node->x < node->right->leftKey) break;
        size++;
        if(node->left && node->left->rightKey < node->x) break;
        node = node->left.get();
    }
    return size;
}

void TTreap::Reverse(long long l, long long r) {
    TTreap right = Split(r + 1);
    TTreap center = Split(l);
    center.root->toReverse ^= true;
    Merge(center);
    Merge(right);
}

void TTreap::Swap(long long int l, long long int r) {
    TTreap right = Split(r + 1);
    TTreap rightElem = Split(r);
    TTreap center = Split(l + 1);
    TTreap leftElem = Split(l);
    Merge(rightElem);
    Merge(center);
    Merge(leftElem);
    Merge(right);
}

void TTreap::NextPermutation(long long int l, long long int r) {
    TTreap right = Split(r + 1);
    TTreap center = Split(l);

    if(center.root->isReverseSorted) center.Reverse(0, center.Size() - 1);
    else {
        long long reverseSorted = center.ReverseSortedSize();
        //std::cout << "reverseSorted " << reverseSorted << std::endl;
        //center.Print();
        TTreap rightCenter = center.Split(center.Size() - reverseSorted);

        //std::cout << "RIGHT CENTER: " << std::endl;
        //rightCenter.Print();

        TTreap toSwap = center.Split(center.Size() - 1);

        //std::cout << "To swap: "; toSwap.Print(); std::cout << std::endl;

        long long nextIndex = rightCenter.Next(toSwap.root->x);
        //std::cout << "NextIndex: " << nextIndex << std::endl;
        TTreap rightPart = rightCenter.Split(nextIndex + 1);
        TTreap toSwap2 = rightCenter.Split(nextIndex);
        rightCenter.Merge(toSwap);
        rightCenter.Merge(rightPart);
        rightCenter.Reverse(0, rightCenter.Size() - 1);
        toSwap2.Merge(rightCenter);
        center.Merge(toSwap2);
    }

    Merge(center);
    Merge(right);
}

long long TTreap::Next(long long x) {
    if(!root || !root->isReverseSorted) return -1;

    Node* node = root.get();
    Node* lastRight = nullptr;
    long long index = 0;
    while(node) {
        if (x >= node->x) {
            node = node->left.get();
        } else {
            index += Node::getSubtreeSize(node->left.get()) + 1;
            lastRight = node;
            node = node->right.get();
        }
    }
    return index - 1;
}

void TTreap::PrevPermutation(long long int l, long long int r) {
    TTreap right = Split(r + 1);
    TTreap center = Split(l);

    if(center.root->isSorted) center.Reverse(0, center.Size() - 1);
    else {
        long long sorted = center.SortedSize();
        TTreap rightCenter = center.Split(center.Size() - sorted);
        TTreap toSwap = center.Split(center.Size() - 1);
        long long prevIndex = rightCenter.Prev(toSwap.root->x);
        //std::cout << "NextIndex: " << nextIndex << std::endl;
        TTreap rightPart = rightCenter.Split(prevIndex + 1);
        TTreap toSwap2 = rightCenter.Split(prevIndex);
        rightCenter.Merge(toSwap);
        rightCenter.Merge(rightPart);
        rightCenter.Reverse(0, rightCenter.Size() - 1);
        toSwap2.Merge(rightCenter);
        center.Merge(toSwap2);
    }

    Merge(center);
    Merge(right);
}

long long TTreap::Prev(long long x) {
    if(!root || !root->isSorted) return -1;

    Node* node = root.get();
    Node* lastRight = nullptr;
    long long index = 0;
    while(node) {
        if (x <= node->x) {
            node = node->left.get();
        } else {
            index += Node::getSubtreeSize(node->left.get()) + 1;
            lastRight = node;
            node = node->right.get();
        }
    }
    return index - 1;
}

void TTreap::PrintInVector(std::vector<long long int> &result) {
    if(root) root->Print(result);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    srand(time(nullptr));
    TTreap tree;

    /*long long n, a;
    std::cin >> n;
    for(long long i = 0; i < n; ++i) {
        std::cin >> a;
        tree.Insert(i, a);
    }

    long long q, cmd;
    std::cin >> q;
    for(long long i = 0; i < q; ++i) {
        std::cin >> cmd;
        if(cmd == 1) {
            long long l, r;
            std::cin >> l >> r;
            std::cout << tree.SumAt(l ,r) << std::endl;
        } else if(cmd == 2) {
            long long x, pos;
            std::cin >> x >> pos;
            tree.Insert(pos, x);
        } else if(cmd == 3) {
            long long pos;
            std::cin >> pos;
            tree.Erase(pos);
        } else if(cmd == 4) {
            long long x, l, r;
            std::cin >> x >> l >> r;
            tree.SetAt(l, r, x);
        } else if(cmd == 5) {
            long long x, l, r;
            std::cin >> x >> l >> r;
            tree.AddAt(l, r, x);
        } else if(cmd == 6) {
            long long l, r;
            std::cin >> l >> r;
            tree.NextPermutation(l, r);
        } else if(cmd == 7) {
            long long l, r;
            std::cin >> l >> r;
            tree.PrevPermutation(l, r);
        }
        //tree.PrintInLine(false);
    }

    std::vector< long long > numbers;
    tree.PrintInVector(numbers);
    std::copy(numbers.begin(), numbers.end(), std::ostream_iterator< long long >(std::cout, " ")); std::cout << std::endl;*/

    std::vector< long long > numbers;
    long long n, a;
    n = 100;
    for(long long i = 0; i < n; ++i) {
        a = rand() % 10000;
        tree.Insert(i, a);
        numbers.push_back(a);
    }

    long long q, cmd;
    q = 1000000;
    for(long long i = 0; i < q; ++i) {
        if(i % 100000 == 0) {
            std::vector<long long> numbers2;
            tree.PrintInVector(numbers2);

            /*for(long long j = 0; j < numbers2.size(); ++j) {
                if(j % 10 == 0) std::cout << j << ": ";
                std::cout << numbers[j] << " ";
            } std::cout << std::endl;*/

            if (!(numbers == numbers2)) {
                std::cout << "Error!" << std::endl;
            }
        }
        /*if(i % 100 == 0 && i > 0) {
            for(auto it = numbers.begin() + 1; it != numbers.begin() + 9 + 1; ++it) (*it) += 101;
            tree.AddAt(1, 9, 101);
        }*/
        cmd = rand() % (7 + 1 + 50);
        if(cmd == 1) {
            long long l, r;
            l = rand() % numbers.size();
            r = rand() % numbers.size();
            if(r < l) std::swap(l, r);
            long long sum = std::accumulate(numbers.begin() + l, numbers.begin() + r + 1, 0ll);
            long long sum2 = tree.SumAt(l, r);
            if(sum != sum2) {
                tree.Print();
                std::vector<long long> numbers2;
                tree.PrintInVector(numbers2);
                if (!(numbers == numbers2)) {
                    auto its = std::mismatch(numbers2.begin(), numbers2.end(), numbers.begin());
                    long long index = std::distance(numbers2.begin(), its.first);
                    std::cout << "Error!" << std::endl;
                }
                long long sum3 = tree.SumAt(l, r);
                std::cout << "Error!" << std::endl;
            }
        } else if(cmd == 2 || numbers.size() <= 10) {
            long long x, pos;
            pos = rand() % (numbers.size() + 1);
            x = rand() % 10000;
            numbers.insert(numbers.begin() + pos, x);
            tree.Insert(pos, x);
            //std::cout << "Insert " << x << " in " << pos << std::endl;
        } else if(cmd == 3) {
            long long pos;
            pos = rand() % numbers.size();
            numbers.erase(numbers.begin() + pos);
            tree.Erase(pos);
            //std::cout << "Erase at " << pos << std::endl;
        }/* else if(cmd == 4) {
            long long x, l, r;
            l = rand() % numbers.size();
            r = rand() % numbers.size();
            if(r < l) std::swap(l, r);
            x = rand() % 10000;
            std::fill(numbers.begin() + l, numbers.begin() + r + 1, x);
            tree.SetAt(l, r, x);
            //std::cout << "Set " << l << " " << r << " to " << x << std::endl;
        }*/ else if(cmd == 5) {
            long long x, l, r;
            l = rand() % numbers.size();
            r = rand() % numbers.size();
            if(r < l) std::swap(l, r);
            x = rand() % 10000;
            for(auto it = numbers.begin() + l; it != numbers.begin() + r + 1; ++it) (*it) += x;
            tree.AddAt(l, r, x);
            //std::cout << "Increase " << l << " " << r << " by " << x << std::endl;
        } else if(cmd >= 6 && cmd <= 35) {
            long long l, r;
            l = rand() % numbers.size();
            r = rand() % numbers.size();
            if(r < l) std::swap(l, r);
            //std::cout << "Next_permutation " << l << " " << r << std::endl;
            //tree.Print();
            std::next_permutation(numbers.begin() + l, numbers.begin() + r + 1);
            tree.NextPermutation(l, r);
        }/* else if(cmd >= 36) {
            long long l, r;
            l = rand() % numbers.size();
            r = rand() % numbers.size();
            if(r < l) std::swap(l, r);
            //std::cout << "Prev_permutation " << l << " " << r << std::endl;
            std::prev_permutation(numbers.begin() + l, numbers.begin() + r + 1);
            tree.PrevPermutation(l, r);
        }*/
        //tree.PrintInLine(false);
    }

    //tree.PrintInLine(true);

    /*std::vector< long long > numbers = {82, 49, 59, 78, 93, 8, 46, 25, 25, 68, 25, 18, 13, 96, 70, 24, 6, 51, 96, 58};
    for(long long i = 0; i < numbers.size(); ++i) tree.Insert(i, numbers[i]);
    
    bool print = true;
    
    if(print) tree.Print();
    std::fill(numbers.begin() + 6, numbers.begin() + 9 + 1, 19);
    tree.SetAt(6, 9, 19);
    std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<long long>(std::cout, " ")); std::cout << std::endl;

    if(print) tree.Print();
    std::next_permutation(numbers.begin() + 5, numbers.begin() + 18 + 1);
    tree.NextPermutation(5, 18);

    if(print) tree.Print();
    std::fill(numbers.begin() + 14, numbers.begin() + 19 + 1, 48);
    tree.SetAt(14, 19, 48);

    if(print) tree.Print();
    for(auto it = numbers.begin() + 8; it != numbers.begin() + 13 + 1; ++it) (*it) += 26;
    tree.AddAt(8, 13, 26);

    if(print) tree.Print();
    std::fill(numbers.begin() + 14, numbers.begin() + 16 + 1, 70);
    tree.SetAt(14, 16, 70);

    if(print) tree.Print();
    for(auto it = numbers.begin() + 8; it != numbers.begin() + 14 + 1; ++it) (*it) += 35;
    tree.AddAt(8, 14, 35);

    if(print) tree.Print();
    numbers.insert(numbers.begin() + 5, 33);
    tree.Insert(5, 33);

    if(print) tree.Print();
    numbers.erase(numbers.begin() + 5);
    tree.Erase(5);

    if(print) tree.Print();
    numbers.insert(numbers.begin() + 2, 89);
    tree.Insert(2, 89);

    std::next_permutation(numbers.begin() + 20, numbers.begin() + 20 + 1);
    tree.NextPermutation(20, 20);

    if(print) tree.Print();
    std::fill(numbers.begin() + 9, numbers.begin() + 14 + 1, 88);
    tree.SetAt(9, 14, 88);

    if(print) tree.Print();
    numbers.insert(numbers.begin() + 9, 27);
    tree.Insert(9, 27);

    std::next_permutation(numbers.begin() + 5, numbers.begin() + 15 + 1);
    tree.NextPermutation(5, 15);

    std::vector< long long > numbers2;
    tree.PrintInVector(numbers2);
    if(!(numbers == numbers2)) {
        std::cout << "Error!" << std::endl;
    }*/

    return 0;
}
