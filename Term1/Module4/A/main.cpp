#pragma GCC optimize("Ofast")

#include <iostream>
#include <vector>

class SegmentTree {
private:

    long long size;
    std::vector<long long> tree;

    long long defaultValue;
    long long (*combine)(long long l, long long r);

    void build(std::vector<long long> &a, long long v, long long tl, long long tr);

    //long long combine(long long l, long long r);

    long long query(long long v, long long tl, long long tr, long long l, long long r);

    void update(long long v, long long tl, long long tr, long long pos, long long x);

public:

    SegmentTree(std::vector<long long> &a, long long (*combine)(long long l, long long r), long long defaultValue);

    long long Query(long long l, long long r);

    void Update(long long pos, long long x);

};

SegmentTree::SegmentTree(std::vector<long long> &a,
                         long long (*combine)(long long l, long long r) = [](long long l, long long r) {
                             return l + r; // sum
                         }, long long defaultValue = 0) : size(a.size()), defaultValue(defaultValue), combine(combine) {
    tree.resize(size * 4);
    build(a, 0, 0, size - 1);
}

void SegmentTree::build(std::vector<long long int> &a, long long int v, long long int tl, long long int tr) {
    if (tl == tr) {
        tree[v] = a[tl];
        return;
    }
    long long tm = (tl + tr) / 2;
    build(a, 2 * v + 1, tl, tm);
    build(a, 2 * v + 2, tm + 1, tr);
    tree[v] = combine(tree[2 * v + 1], tree[2 * v + 2]);
}

/*long long SegmentTree::combine(long long int l, long long int r) {
    return l + r;
}*/

long long SegmentTree::query(long long int v, long long int tl, long long int tr, long long int l, long long int r) {
    if (r < l) return defaultValue;
    if (tl == l && tr == r) return tree[v];

    long long tm = (tl + tr) / 2;
    return combine(query(2 * v + 1, tl, tm, l, std::min(tm, r)),
                   query(2 * v + 2, tm + 1, tr, std::max(tm + 1, l), r));
}

long long SegmentTree::Query(long long l, long long r) {
    return query(0, 0, size - 1, l, r);
}

void SegmentTree::update(long long int v, long long int tl, long long int tr, long long int pos, long long int x) {
    if (tl == tr) {
        tree[v] = x;
        return;
    }
    long long tm = (tl + tr) / 2;
    if (pos <= tm) update(2 * v + 1, tl, tm, pos, x);
    else update(2 * v + 2, tm + 1, tr, pos, x);

    tree[v] = combine(tree[2 * v + 1], tree[2 * v + 2]);
}

void SegmentTree::Update(long long int pos, long long int x) {
    update(0, 0, size - 1, pos, x);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::vector< long long > a(100000);
    for(long long i = 1; i <= a.size(); ++i) {
        a[i - 1] = (i * i) % 12345 + (i * i * i) % 23456;
    }

    SegmentTree treeMax(a, [](long long l, long long r){ return std::max(l, r); }, std::numeric_limits<long long>::min());
    SegmentTree treeMin(a, [](long long l, long long r){ return std::min(l, r); }, std::numeric_limits<long long>::max());

    long long k;
    std::cin >> k;

    long long x, y;
    for(long long i = 0; i < k; ++i) {
        std::cin >> x >> y;
        if(x > 0) {
            std::cout << treeMax.Query(x - 1, y - 1) - treeMin.Query(x - 1, y - 1) << '\n';
        } else if(x < 0) {
            treeMax.Update(std::abs(x) - 1, y);
            treeMin.Update(std::abs(x) - 1, y);
        }
    }

    return 0;
}
