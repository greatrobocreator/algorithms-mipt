#include <iostream>
#include <bits/unique_ptr.h>
#include <vector>

struct Node {
    long long data;
    std::unique_ptr< Node > left, right;

    Node(long long data, std::unique_ptr<Node> left = nullptr, std::unique_ptr<Node> right = nullptr) : data(data),
                                                                                                        left(std::move(left)),
                                                                                                        right(std::move(right)) {};

};

class TDynamicSegmentTree {
private:

    long long size;
    long long defaultValue;
    long long (*combine)(long long l, long long r);
    std::unique_ptr< Node > root;

    long long getNodeData(Node* node);
    std::unique_ptr<Node> add(std::unique_ptr<Node> node, long long tl, long long tr, long long pos, long long delta);
    long long query(Node* node, long long tl, long long tr, long long l, long long r);

public:
    TDynamicSegmentTree(long long size, long long (*combine)(long long l, long long r), long long defaultValue);

    void Add(long long pos, long long delta);
    long long Query(long long l, long long r);
};

TDynamicSegmentTree::TDynamicSegmentTree(long long size,
                                         long long (*combine)(long long l, long long r) = [](long long l, long long r) {
                                             return l + r; // sum
                                         }, long long defaultValue = 0) : size(size), defaultValue(defaultValue), combine(combine) {
    root = std::make_unique< Node >(defaultValue);
}

std::unique_ptr<Node> TDynamicSegmentTree::add(std::unique_ptr<Node> node, long long tl, long long tr, long long pos, long long delta) {
    if(!node) node = std::make_unique<Node>(defaultValue);
    if(tl == tr) {
        node->data += delta;
        return node;
    }
    long long tm = (tl + tr) / 2;
    if(pos <= tm) node->left = add(std::move(node->left), tl, tm, pos, delta);
    else node->right = add(std::move(node->right), tm + 1, tr, pos, delta);
    node->data = combine(getNodeData(node->left.get()), getNodeData(node->right.get()));
    return node;
}

long long TDynamicSegmentTree::getNodeData(Node* node) {
    return (node ? node->data : defaultValue);
}

long long TDynamicSegmentTree::query(Node* node, long long tl, long long tr, long long l, long long r) {
    if(!node || r < l) return defaultValue;
    if(tl == l && tr == r) return node->data;

    long long tm = (tl + tr) / 2;
    return combine(query(node->left.get(), tl, tm, l, std::min(tm, r)),
                   query(node->right.get(), tm + 1, tr, std::max(l, tm + 1), r));
}

void TDynamicSegmentTree::Add(long long pos, long long delta) {
    root = add(std::move(root), 0, size - 1, pos, delta);
}

long long TDynamicSegmentTree::Query(long long l, long long r) {
    return query(root.get(), 0, size - 1, l, r);
}


int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    TDynamicSegmentTree tree(1'000'000'000);

    long long q, x;
    std::cin >> q;

    for(long long i = 0; i < q; ++i) {
        char cmd;
        std::cin >> cmd >> x;
        if(cmd == '+') {
            tree.Add(x, x);
        } else if(cmd == '?') {
            std::cout << tree.Query(0, x) << std::endl;
        }
    }

    return 0;
}
