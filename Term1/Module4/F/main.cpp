#include <iostream>
#include <vector>

class TFenwikTree {
private:

    long long size;
    std::vector<std::vector<std::vector<long long> > > &a;
    std::vector<std::vector<std::vector<long long> > > t;

    void update(long long x, long long y, long long z, long long delta);

    long long sum(long long x, long long y, long long z);

public:

    TFenwikTree(std::vector<std::vector<std::vector<long long> > > &a);

    void Update(long long x, long long y, long long z, long long delta);

    long long Sum(long long x1, long long y1, long long z1, long long x2, long long y2, long long z2);
};

TFenwikTree::TFenwikTree(std::vector<std::vector<std::vector<long long> > > &a) : size(a.size()), a(a) {
    t.resize(size);
    for(long long i = 0; i < size; ++i) {
        t[i].resize(size);
        for (long long j = 0; j < size; ++j)
            t[i][j].resize(size);
    }

    for (size_t i = 0; i < size; ++i)
        for (size_t j = 0; j < size; ++j)
            for (size_t k = 0; k < size; ++k)
                update(i, j, k, a[i][j][k]);
}

void TFenwikTree::update(long long x, long long y, long long z, long long delta) {
    for (long long i = x; i < size; i |= (i + 1))
        for (long long j = y; j < size; j |= (j + 1))
            for (long long k = z; k < size; k |= (k + 1))
                t[i][j][k] += delta;
}

long long TFenwikTree::sum(long long x, long long y, long long z) {
    long long ans = 0;
    for (long long i = x; i >= 0; i = (i & (i + 1)) - 1)
        for (long long j = y; j >= 0; j = (j & (j + 1)) - 1)
            for (long long k = z; k >= 0; k = (k & (k + 1)) - 1)
                ans += t[i][j][k];

    return ans;
}

void TFenwikTree::Update(long long x, long long y, long long z, long long delta) {
    update(x, y, z, delta);
}

long long TFenwikTree::Sum(long long x1, long long y1, long long z1, long long x2, long long y2, long long z2) {
    --x1; --y1; --z1;
    return sum(x2, y2, z2)
           - sum(x1, y2, z2) - sum(x2, y1, z2) - sum(x2, y2, z1)
           + sum(x1, y1, z2) + sum(x2, y1, z1) + sum(x1, y2, z1)
           - sum(x1, y1, z1);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    std::vector<std::vector<std::vector<long long> > > a(n, std::vector< std::vector< long long > >(n, std::vector< long long>(n, 0)));
    TFenwikTree tree(a);

    long long m;
    std::cin >> m;
    for (; m != 3; std::cin >> m) {
        if (m == 1) {
            long long x, y, z, k;
            std::cin >> x >> y >> z >> k;
            tree.Update(x, y, z, k);
        } else if (m == 2) {
            long long x1, y1, z1, x2, y2, z2;
            std::cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;
            std::cout << tree.Sum(x1, y1, z1, x2, y2, z2) << std::endl;//'\n';
        }
    }

    return 0;
}
