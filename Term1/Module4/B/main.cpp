#include <iostream>
#include <vector>
#include <cmath>

class TFenwikTree {
private:

    long long size;
    std::vector< long long >& a;
    std::vector< long long > t;

    void update(unsigned long long pos, long long delta);
    long long sum(long long pos);

public:

    TFenwikTree(std::vector< long long >& a);
    void Update(unsigned long long pos, long long delta);
    void Set(unsigned long long pos, long long x);
    long long Sum(long long l, long long r);
};

TFenwikTree::TFenwikTree(std::vector<long long int> &a) : size(a.size()), a(a) {
    t.resize(size);
    for(size_t i = 0; i < size; ++i) update(i, a[i]);
}

void TFenwikTree::update(unsigned long long pos, long long delta) {
    for(; pos < size; pos |= (pos + 1)) t[pos] += delta;
}

long long TFenwikTree::sum(long long pos) {
    long long ans = 0;
    for(; pos >= 0; pos = (pos & (pos + 1)) - 1) ans += t[pos];
    return ans;
}

void TFenwikTree::Update(unsigned long long int pos, long long int delta) {
    update(pos, delta);
}

void TFenwikTree::Set(unsigned long long int pos, long long int x) {
    update(pos, x - a[pos]);
    a[pos] = x;
}

long long TFenwikTree::Sum(long long l, long long r) {
    return sum(r) - sum(l - 1);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    long long n;
    std::cin >> n;

    std::vector< long long > even((n + 1) / 2); // четные
    std::vector< long long > odd(n / 2); // нечетные

    for(long long i = 0; i < n; ++i) {
        std::cin >> (i % 2 == 0 ? even[i / 2] : odd[i / 2]);
    }

    TFenwikTree treeEven(even);
    TFenwikTree treeOdd(odd);

    long long m, cmd, x, y;
    std::cin >> m;
    for(long long i = 0; i < m; ++i) {
        std::cin >> cmd >> x >> y;
        if(cmd == 0) {
            --x;
            (x % 2 == 0 ? treeEven : treeOdd).Set(x / 2, y);
        } else if(cmd == 1) {
            --x; --y;
            std::cout << (treeEven.Sum((x + 1) / 2, y / 2) - treeOdd.Sum(x / 2, floor((y - 1) / 2.0))) * (x % 2 == 0 ? 1 : -1) << std::endl;//'\n';
        }
    }

    return 0;
}
